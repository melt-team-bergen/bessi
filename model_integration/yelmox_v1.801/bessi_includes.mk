# Basic libraries

# The rule is always called `filename.o` and first dependency is always `filename.f90`. The other dependencies must be compiled before the the file, since it has a `use` statement inside. Then the file is compiled and the $@ is the name of the rule, i.e. `filename.o` while `$<` points to the first dependency, i.e. `filename.f90`.

$(objdir)/m_npy.o: $(srcdir)/m_npy.f90
	$(FC) $(FFLAGS) -c -o $@ $< $(VARFLAGS) $(IOPATH)

$(objdir)/bessi_defs.o: $(srcdir)/bessi_defs.f90
	$(FC) $(FFLAGS) -c -o $@ $< $(VARFLAGS) $(IOPATH)

$(objdir)/bessi_io.o: $(srcdir)/bessi_io.f90
	$(FC) $(FFLAGS) $(LIB_NC) -c -o $@ $< $(IOVARFLAGS)

$(objdir)/bessi_data.o: $(srcdir)/bessi_data.f90 $(objdir)/bessi_defs.o \
						 $(objdir)/bessi_io.o \
						 $(objdir)/conservation.o
	$(FC) $(FFLAGS) $(LIB_NC) -c -o $@ $<

$(objdir)/bessi_climate.o: $(srcdir)/bessi_climate.f90 \
						   $(objdir)/bessi_defs.o \
						   $(objdir)/bessi_io.o
	$(FC) $(FFLAGS) -c -o $@ $<

$(objdir)/bessi_boundary.o: $(srcdir)/bessi_boundary.f90 \
							$(objdir)/bessi_defs.o \
							$(objdir)/bessi_io.o
	$(FC) $(FFLAGS) -c -o $@ $<

$(objdir)/bessi_tools.o: $(srcdir)/bessi_tools.f90 \
						 $(objdir)/bessi_defs.o \
						 $(objdir)/bessi_climate.o \
						 $(objdir)/bessi_io.o
	$(FC) $(FFLAGS) -c -o $@ $<  


# Physics libraries
$(objdir)/conservation.o: $(srcdir)/physics/conservation.f90 $(objdir)/bessi_defs.o
	$(FC) $(FFLAGS) -c -o $@ $<  

$(objdir)/regridding.o: $(srcdir)/physics/regridding.f90 $(objdir)/bessi_defs.o
	$(FC) $(FFLAGS) -c -o $@ $<  

$(objdir)/precipitation_accumulate.o: $(srcdir)/physics/precipitation_accumulate.f90 $(objdir)/bessi_defs.o
	$(FC) $(FFLAGS) -c -o $@ $<  

$(objdir)/densification.o: $(srcdir)/physics/densification.f90 $(objdir)/bessi_defs.o
	$(FC) $(FFLAGS) -c -o $@ $<  

$(objdir)/radiation.o: $(srcdir)/physics/radiation.f90 $(objdir)/bessi_defs.o
	$(FC) $(FFLAGS) -c -o $@ $<  

$(objdir)/energy_flux.o: $(srcdir)/physics/energy_flux.f90 $(objdir)/bessi_defs.o
	$(FC) $(FFLAGS) -c -o $@ $<  

$(objdir)/melting.o: $(srcdir)/physics/melting.f90 $(objdir)/bessi_defs.o
	$(FC) $(FFLAGS) -c -o $@ $<  

$(objdir)/percolation_refreezing.o: $(srcdir)/physics/percolation_refreezing.f90 $(objdir)/bessi_defs.o
	$(FC) $(FFLAGS) -c -o $@ $<  

$(objdir)/smb_emb.o: $(srcdir)/physics/smb_emb.f90 $(objdir)/bessi_defs.o \
				   	 $(objdir)/bessi_data.o \
				   	 $(objdir)/bessi_io.o \
				   	 $(objdir)/conservation.o \
				   	 $(objdir)/regridding.o \
				   	 $(objdir)/precipitation_accumulate.o \
				   	 $(objdir)/densification.o \
				   	 $(objdir)/radiation.o \
				   	 $(objdir)/energy_flux.o \
				   	 $(objdir)/melting.o \
				   	 $(objdir)/percolation_refreezing.o
	$(FC) $(FFLAGS) $(PFLAGS) -c -o $@ $<


# Compile macros:
bessi_base :=   $(objdir)/m_npy.o \
		$(objdir)/bessi_defs.o \
		$(objdir)/bessi_data.o \
		$(objdir)/bessi_io.o \
		$(objdir)/bessi_climate.o \
		$(objdir)/bessi_tools.o
#		$(objdir)/bessi_boundary.o \
# 		$(objdir)/bessi.o

bessi_physics := $(objdir)/conservation.o \
		$(objdir)/regridding.o \
		$(objdir)/precipitation_accumulate.o \
		$(objdir)/densification.o \
		$(objdir)/radiation.o \
		$(objdir)/energy_flux.o \
		$(objdir)/melting.o \
		$(objdir)/percolation_refreezing.o \
		$(objdir)/smb_emb.o
