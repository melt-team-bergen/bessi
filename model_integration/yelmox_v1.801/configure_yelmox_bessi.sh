# configuration script for adding/editing necessary scripts from BESSI to yelmox
# Therese Rieckh
# University of Bergen, 2023



### EDIT PATHS ###
BESSIROOT='/work2/trieckh/git-repos/bessi-yelmo-coupled/bessi/'
YELMOROOT='/work2/trieckh/git-repos/bessi-yelmo-coupled/yelmo'
YELMOXROOT='/work2/trieckh/git-repos/bessi-yelmo-coupled/yelmox'
LISROOT='/work2/trieckh/libs/lis'
FORTRANROOT='/usr'
### EDIT PATHS ###


echo "  "
echo "  "
echo "  "
echo ">>> Make sure you have adjusted the BESSIROOT, YELMOROOT, YELMOXROOT, LISROOT, and FORTRANROOT above in the this file (configure_yelmox_elsa.sh) <<<"
echo "  "
echo "  "
echo "  "

echo "Starting confguration of bessi-yelmox"


BESSIY=${BESSIROOT%%/}/model_integration/yelmox_v1.801

echo "Editing paths in bessi_gfortran"
echo "  "
sed -i "s%MYFORTRAN%${FORTRANROOT}%g" $BESSIY/bessi_gfortran
sed -i "s%MYLIS%${LISROOT}%g" $BESSIY/bessi_gfortran
sed -i "s%MYYELMO%${YELMOROOT}%g" $BESSIY/bessi_gfortran
sed -i "s%MYBESSI%${BESSIROOT}%g" $BESSIY/bessi_gfortran


echo "Editing paths in bessi.sh"
echo "  "
sed -i "s%MYBESSI%${BESSIROOT}%g" $BESSIY/bessi.sh


# copy bessi_yelmox.f90 script to bessi/src folder
echo "Copying bessi_yelmox.f90 to bessi/src/ dir"
echo "Copying bessi_defs.f90 to bessi/src/ dir"
echo "  "
cp $BESSIY/bessi_yelmox.f90 $BESSIROOT/src/bessi_yelmox.f90
cp $BESSIY/bessi_defs.f90 $BESSIROOT/src/bessi_defs.f90


# copy bessi_includes.mk script to bessi/ folder
echo "bessi_includes.mk to bessi/ dir"
echo "  "
cp $BESSIY/bessi_includes.mk $BESSIROOT/bessi_includes.mk


# copy over compiler settings
echo "Copying bessi_gfortran compiler settings to yelmox dir"
echo "  "
cp $BESSIY/bessi_gfortran $YELMOXROOT/config/bessi_gfortran


# add executable alias yelmox_bessi
echo "Adding bessi executable alias to runylmox.js file"
echo "  "
cp $YELMOXROOT/config/runylmox.js $YELMOXROOT/runylmox.js # ensure that runylmox.js is in main folder
sed -i  '/"iso"/a"bessi"   : "libyelmox/bin/yelmox_bessi.x",' $YELMOXROOT/runylmox.js


# copy over yelmox Makefile with yelmox_bessi compiling and library linking
echo "Copying Makefile to yelmox dir"
echo "  "
cp $BESSIY/Makefile $YELMOXROOT/config/Makefile


# copy over example namelist file for bessi test run with steady state climate
cp $BESSIY/yelmo_Greenland_bessi_16km.nml $YELMOXROOT/par/yelmo_Greenland_bessi_16km.nml


# copy over yelmox code with bessi adjustments
echo "Copying yelmox_bessi.f90 to yelmox dir"
echo "  "
cp $BESSIY/yelmox_bessi.f90 $YELMOXROOT/yelmox_bessi.f90


# copy over bash script which 1) configures the yelmox Makefile, 2) compiles BESSI 3) compiles yelmox, 4) starts yelmox testrun
echo "Copying bessi.sh to yelmox dir"
echo "  "
cp $BESSIY/bessi.sh $YELMOXROOT/bessi.sh


echo "Confguration done"
