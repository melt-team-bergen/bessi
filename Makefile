.SUFFIXES: .f .F .F90 .f90 .o .mod
.SHELL: /bin/sh

bindir := bin
objdir := include
srcdir := src

FC := gfortran

NCROOT := /usr
LIB_NC := -I${NCROOT}/include -L${NCROOT}/lib -lnetcdff -Wl,-rpath -Wl,${NCROOT}/lib

FFLAGS := -O3 -mcmodel=medium -ffree-line-length-none -J$(objdir) #-Wall -Wextra 

include bessi_includes.mk


$(objdir)/bessi_yelmox.o: $(srcdir)/bessi_yelmox.f90 $(bessi_base) $(bessi_physics)
	$(FC) $(FFLAGS) $(PFLAGS) -c -o $@ $<

bessi_yelmox := $(objdir)/bessi_yelmox.o


# Added for the coupling with yelmox
bessi-library: $(bessi_base) $(bessi_physics) $(bessi_yelmox) # this compiles the files
	ar rc $(objdir)/libbessi_yelmox.a $(bessi_base) $(bessi_physics) $(bessi_yelmox)
	ranlib $(objdir)/libbessi_yelmox.a
	@echo " "
	@echo "    $(objdir)/libbessi_yelmox.a is ready."
	@echo " "

# Static library compilation
bessi-static: $(bessi_base) $(bessi_physics)
	ar rc $(objdir)/libbessi.a $(bessi_base) $(bessi_physics)
	ranlib $(objdir)/libbessi.a
	@echo " "
	@echo "    $(objdir)/libbessi.a is ready."
	@echo " "

bessi-single: bessi-static
	$(FC) $(FFLAGS) -o $(bindir)/bessi.x $(srcdir)/bessi.f90 \
		-L${CURDIR}/include -lbessi $(LFLAGS) $(LIB_NC)
	@echo " "
	@echo "    bessi.x is ready."
	@echo " "

bessi: bessi-static
	$(FC) $(FFLAGS) -o $(bindir)/bessi.x $(srcdir)/bessi.f90 \
		-L${CURDIR}/include -lbessi $(LFLAGS) $(LIB_NC) \
		-fopenmp -DOMPRUN=true
	@echo " "
	@echo "    openMP bessi.x is ready."
	@echo " "


.PHONY: clean
clean:
	-rm -f include/*.o include/*.a include/*.mod

