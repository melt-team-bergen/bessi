module bessi_climate
  ! Load climate data and perform corrections

  use bessi_defs
  use bessi_io

  implicit none
  integer, dimension(15000) :: reorder_sequence ! reads years to use for climate data from file

  type :: climinp_data_type
    integer                              :: year 
    integer                              :: current_clim_year ! what is the difference between year and current_clim_year? This needs to be a comment...
    real(kind=8), dimension(nx,ny,ndays) :: temp ! Temperature, at which height?
    real(kind=8), dimension(nx,ny,ndays) :: precip ! Precipitation
    real(kind=8), dimension(nx,ny,ndays) :: dewpt ! Dew Point Temperature
    ! real(kind=8), dimension(nx,ny,ndays) :: windsp ! Wind Speet
    real(kind=8), dimension(nx,ny,ndays) :: lwrd ! Long Wave Radiation
    real(kind=8), dimension(nx,ny,ndays) :: swrd ! Short Wave Radiation
    contains
    procedure :: init   => initialize_climate ! This means, that the routine initialize_climate can be called as init from outside.
    procedure :: update => update_climate
    procedure :: reset  => reset_climate_data
  end type climinp_data_type


  ! netCDF Climate, only needed when the corrected input climate is kept for debugging (ABo: needs a clean-up):
  character(128) :: netcdf_output_climate_filename
  integer        :: precip_varid
  integer        :: temp_varid
  integer        :: dev_precip_varid
  integer        :: dev_temp_varid
  integer        :: swrad_varid
  integer        :: dev_swrad_varid

  public :: initialize_climate
  public :: update_climate
! public :: store_climate
! public :: load_climate_input
! public :: calculate_climate_elevation_feedback
! public :: calculate_corrections

contains

! ################################################  
  subroutine reset_climate_data(clim_data)
    ! Set all variables except current_clim_year to zero.

    implicit none
    class(climinp_data_type), intent(inout) :: clim_data

    clim_data%year   = 0
    clim_data%temp   = 0d0
    clim_data%precip = 0d0
    clim_data%dewpt  = 0d0
    ! clim_data%windsp = 0d0
    clim_data%lwrd   = 0d0
    clim_data%swrd   = 0d0

  end subroutine reset_climate_data

  
! ################################################  
  subroutine initialize_climate(clim_data)
    ! Initialize variables needed by the climate.
    ! Load years into vector for reorder_climate spinup.

    implicit none
    integer                                 :: kk ! What do these variables do?
    integer                                 :: cr
    integer                                 :: cm
    integer                                 :: ios
    integer                                 :: n
    class(climinp_data_type), intent(inout) :: clim_data

    if (debug > 0) then
      print *, "Initialize Climate variables from external files"
      print *, "------------------------------------------------"
    end if

    ! Load the climate reference elevation:
    climinp_ref_topo = read_variable(netcdf_climate_input_ref_topo, nx, ny, TRIM(adjustl(climate_input_ref_topo_varname)))

    select case (climate_type)
      case ('single_year')
        ! nothing to do here

      case ('backandforth_climate')
        clim_data%current_clim_year = clim_year_start - 1

      case ('reorder_climate')
        print*,"reading order vector"
        OPEN(UNIT=11, FILE=reorder_climate_file)

        ! Read all lines in file
        n = 1
        do
          read(11, *, iostat=ios) reorder_sequence(n)
          !print *, "Read in year ", reorder_sequence(n)
          if (ios /= 0) exit
          n = n + 1
        end do

        if (n-1 .ne. maxyears) then
          write(*,'(A,A,A,I4,A,I5,A)') 'File ', trim(adjustl(reorder_climate_file)), ' includes ', n-1,' entries, which does not match the ', maxyears, ' simulation years.'
          stop
        end if

     end select

  end subroutine initialize_climate


! ################################################
  subroutine update_climate(clim_data, myear)
    ! Find the correct year of climate focing, load and correct it

    implicit none
    integer, intent(in)                     :: myear
    character(128)                          :: new_input_file
    character(len=256)                      :: spec_format
    class(climinp_data_type), intent(inout) :: clim_data

    select case (climate_type)
      case ('single_year')
        if (debug > 1) write(*,*) "*** Calculating mass balance using a single year of climate forcing"
        ! wipe all data from previous year:
        call clim_data%reset! ABo: not necessary for single-year forcing. Could save time by not re-loading the same year (see below).

        clim_data%current_clim_year = clim_year

      case ('backandforth_climate')
        if (debug > 1) print*,"*** Calculating mass balance with backandforth climate"
        ! wipe all data from previous year:
        call clim_data%reset

        ! follow this sequence: 1979 1980 ... 2015 2016 2016(again) 2015 ... 1980 1979 1979(again) 1980 ...
        if (climate_backwards) then
          clim_data%current_clim_year = clim_data%current_clim_year - 1
        else
          clim_data%current_clim_year = clim_data%current_clim_year + 1
        end if

        if (clim_data%current_clim_year == clim_year_start -1) then
          climate_backwards = .false.
          clim_data%current_clim_year = clim_year_start
        else if (clim_data%current_clim_year == clim_year_turn +1) then
          climate_backwards = .true.
          clim_data%current_clim_year = clim_year_turn
        end if

      case ('reorder_climate')
        if (debug > 1) print*,"*** Re-order climate forcing using year order vector"
        ! wipe all data from previous year:
        call clim_data%reset

        clim_data%current_clim_year = reorder_sequence(myear)

      case default
        write(*,*) 'Parameter "climate_type" is undefined.'
        stop

    end select

    if (debug > 1) write(*,*) '[update_climate] Current climate year', clim_data%current_clim_year

    ! now that we know which year to pick, construct the correct file name:
    write (spec_format, '("(A", I0,",I",I0,".", I0,",A", I0,")")') &
      len_trim(netcdf_input_name_leading_string), netcdf_input_digit_specification, &
      netcdf_input_digit_specification , len_trim(netcdf_input_name_end_string) 

    write (new_input_file, spec_format) netcdf_input_name_leading_string, clim_data%current_clim_year, netcdf_input_name_end_string

    new_input_file = TRIM(adjustl(climate_input_dir)) // TRIM(adjustl(new_input_file))

    call read_climate_variables(clim_data, new_input_file, myear)
    ! Check climate variables
    call check_climate_variables(clim_data)
    ! Calculate corrections
    call calculate_corrections(clim_data)

    ! Calculate elevation feedback every year (could save some time with if condition below)
    call calculate_climate_elevation_feedback(clim_data)
!     if (climate_type == 'backandforth_climate' .or. climate_type == 'reorder_climate') then
!       call calculate_climate_elevation_feedback()
!     end if

    !ABo, 9-Sep-2024: Deprecated, will be removed:
    ! Store climate data including all corrections:
!     if (store_input_netcdf) call store_climate(clim_data)

    ! Define quasi-global arrays for use in smb_emb.F90 and physics/ routines (ABo: change later):
!     temperature   = clim_data%temp
!     precipitation = clim_data%precip
    dewpoint      = clim_data%dewpt
    ! windspeed     = clim_data%windsp
    longwrd       = clim_data%lwrd
    shortwrd      = clim_data%swrd

    if (debug > 1) write(*,*) 'Climate input ready for year ', clim_data%current_clim_year

  end subroutine update_climate


!ABo, 9-Sep-2024: Deprecated, will be removed:
! ################################################  
!   subroutine store_climate(climate_data_loc)
!     ! Store climate input data in output directory, ABo: not working well, needs re-write similar to bessi_data
!     implicit none
!     class(climinp_data_type), intent(in)  :: climate_data_loc
!     integer                               :: id
!     integer                               :: filehandle_netcdf
! 
!     write (netcdf_output_climate_filename, '( "Climate_", I7.7, ".nc" )') climate_data_loc%year
! 
!     !ABo, several unused variables, only three are written (see next code block below):
!     call init_netcdf_climate_file(TRIM(adjustl(output_directory)) // netcdf_output_climate_filename,&
!         filehandle_netcdf, ny, nx, int(dy), int(dx), precip_varid, temp_varid, swrad_varid,&
!         dev_precip_varid, dev_temp_varid, dev_swrad_varid )
! 
!     do id = 1, ndays, 1
!       call writeNCDFGridValues(filehandle_netcdf, id, precip_varid, real(climate_data_loc%precip(:, :, id)), ny, nx)
!       call writeNCDFGridValues(filehandle_netcdf, id, temp_varid, real(climate_data_loc%temp(:, :, id)), ny, nx)
! !       call writeNCDFGridValues(filehandle_netcdf3, id, dev_precip_varid, real(deviation_precip(:, :, id)) , ny, nx)
! !       call writeNCDFGridValues(filehandle_netcdf3, id, dev_temp_varid, real(deviation_temp(:, :, id)), ny, nx)
!       call writeNCDFGridValues(filehandle_netcdf, id, swrad_varid, real(climate_data_loc%swrd(:, :, id)), ny, nx)
! !       call writeNCDFGridValues(filehandle_netcdf3, id, dev_swrad_varid, real(deviation_P_sun(:, :, id)), ny, nx)
!     end do
! 
!     call closeNCDFFile(filehandle_netcdf)
!   end subroutine store_climate


! ################################################
  subroutine read_climate_variables(climate_data_loc, input_file, myear)
    ! Read climate data from file

    implicit none
    character(128)                          :: input_file
    integer, intent(in)                     :: myear
    class(climinp_data_type), intent(inout) :: climate_data_loc

    call read_climate_once(input_file, nx, ny,         &
        TRIM(adjustl(climate_input_temp_varname)),     &
        TRIM(adjustl(climate_input_precip_varname)),   &
        TRIM(adjustl(climate_input_dewpt_varname)),    &
        ! TRIM(adjustl(climate_input_wind_varname)),     &
        TRIM(adjustl(climate_input_lwrd_varname)),     &
        TRIM(adjustl(climate_input_swradboa_varname)), &
        climate_data_loc%temp, climate_data_loc%precip, climate_data_loc%dewpt, climate_data_loc%lwrd, climate_data_loc%swrd)

    climate_data_loc%year = myear

  end subroutine read_climate_variables


  ! ################################################
  subroutine check_climate_variables(climate_data_loc)
  ! Calculate climate data corrections

    implicit none
    class(climinp_data_type), intent(inout) :: climate_data_loc
    logical :: all_non_negative
    real :: min_value

    if (temp_unit == 2) then
      if (debug .ge. 2) write(*,*) 'change T unit to K'
      climate_data_loc%temp   = climate_data_loc%temp + kelvin ! degC -> K
    end if
    if (dewpt_unit == 1) then
      if (debug .ge. 2) write(*,*) 'change dewpt unit to degC'
      climate_data_loc%dewpt   = climate_data_loc%dewpt - kelvin ! K -> degC since equations in energy_flux.f90 require dewpt in degC
    end if
    if (precip_unit == 2) then
      if (debug .ge. 2) write(*,*) 'change precip unit to m/s'
      climate_data_loc%precip = climate_data_loc%precip/3600./24./1000. ! mm/day -> m/sec
    end if
    if (swrd_unit == 2) then
      if (debug .ge. 2) write(*,*) 'change swrd unit to W/m2'
      climate_data_loc%swrd = climate_data_loc%swrd/3600./24. ! J/timestep (d) -> W/m2
    end if
    if (lwrd_unit == 2) then
      if (debug .ge. 2) write(*,*) 'change lwrd unit to W/m2'
      climate_data_loc%lwrd = climate_data_loc%lwrd/3600./24. ! J/timestep (d) -> W/m2
    end if
    ! NOTE add for climate_data_loc%windsp

    ! SWRD
    all_non_negative = all(climate_data_loc%swrd >= 0)
    if (.not. all_non_negative) then
       min_value = minval(climate_data_loc%swrd)
       write(*,*) "Negative values for variable swrd, minimum value is ", min_value
       write(*,*) "Setting all negative values in swrd to 0."
       where (climate_data_loc%swrd < 0)
          climate_data_loc%swrd = 0
       end where
    end if

    ! LWRD
    all_non_negative = all(climate_data_loc%lwrd >= 0)
    if (.not. all_non_negative) then
       min_value = minval(climate_data_loc%lwrd)
       write(*,*) "Negative values for variable lwrd, minimum value is ", min_value
       write(*,*) "Setting all negative values in lwrd to 0."
       where (climate_data_loc%lwrd < 0)
          climate_data_loc%lwrd = 0
       end where
    end if

    ! PRECIP
    all_non_negative = all(climate_data_loc%precip >= 0)
    if (.not. all_non_negative) then
       min_value = minval(climate_data_loc%precip)
       write(*,*) "Negative values for variable precip, minimum value is ", min_value
       write(*,*) "Setting all negative values in precip to 0."
       where (climate_data_loc%precip < 0)
          climate_data_loc%precip = 0
       end where
    end if

  end subroutine check_climate_variables


! ################################################
  subroutine calculate_corrections(climate_data_loc)
  ! Calculate climate data corrections

    implicit none
    integer                                 :: day
    class(climinp_data_type), intent(inout) :: climate_data_loc

    if (longwave_downscaling) then
      do day=1,ndays,1
        climate_data_loc%lwrd(:,:,day) = climate_data_loc%lwrd(:,:,day)/climate_data_loc%temp(:,:,day)**4 *(climate_data_loc%temp(:,:,day) + ((climinp_ref_topo - elevation) * temperature_lapse_rate))**4
      end do
    end if

  end subroutine calculate_corrections


! ################################################
  subroutine calculate_climate_elevation_feedback(climate_data_loc)
    ! Calculate the elevation feedback.

    implicit none
    integer                                 :: day
    class(climinp_data_type), intent(inout) :: climate_data_loc

    if(active_temperature_lapserate) then
      do day=1,ndays,1
        climate_data_loc%temp(:,:,day) = climate_data_loc%temp(:,:,day) - (elevation - climinp_ref_topo) * temperature_lapse_rate
      end do
    end if

    if (active_dewpoint_lapserate) then
      do day=1,ndays,1
        climate_data_loc%dewpt(:,:,day) = climate_data_loc%dewpt(:,:,day) - (elevation - climinp_ref_topo) * dewpoint_lapse_rate
      end do
    end if

    ! Calculate elevation feedback on short wave radiation
		!if (short_wave_damping) then
		!	call swrad_damping(shortwrd, P_sun0, elevation, climinp_ref_topo)
		!end if

		! add short wave radiation deviation
    	!do id=1,ndays,1
		!	 shortwrd(:,:,id) = shortwrd(:,:,id) + deviation_P_sun(:,:,id)
		!	 where(shortwrd(:,:,id).lt. 0.)
		!		 shortwrd(:,:,id) = 0.
		!			end where
		!		end do

end subroutine calculate_climate_elevation_feedback

end module bessi_climate
