module bessi_boundary
	! Load and calculate boundary (masks, etc.) data

	use bessi_defs
	use bessi_io

	implicit none

	private
	public :: load_mask
    public :: load_restart_state
    public :: update_elevation
    public :: load_latitudes

contains
  ! ################################################
  subroutine load_mask()
    ! Load ice and bedrock masks
    implicit none

    ! Read the relaxed Bedrock from file
    if(debug > 1 ) then
      print *, "*** Read '", TRIM(adjustl(netcdf_topo_varname)) ,"' from file: ", TRIM(adjustl(netcdf_topo_file))
    end if

    elevation = read_variable(netcdf_topo_file, nx, ny, netcdf_topo_varname)
    if (cumulative_elevation_change) then
      bedrock = read_variable(netcdf_bed_file, nx, ny, netcdf_bed_varname)
    end if

    ! 1 = water, 0 = normal case, 3 = unstable grid points
    landmask = get_landmask(elevation, sea_level)
    if (debug .ge. 1) print *, "*** Created landmask"
  end subroutine load_mask

  ! ################################################
  subroutine load_latitudes()
    ! UKK!Load latitude data
    implicit none

    if(debug > 1 ) then
      print *, "*** Read latitude data from file: ", TRIM(adjustl(latitudes_filename))
    end if

    latitudes = read_variable(latitudes_filename, nx, ny, 'lat')
  end subroutine load_latitudes

  ! ################################################
	subroutine load_restart_state()
		! Load state (snowmass, liquidmass, etc. ) file to restart BESSI from this state

		implicit none

        snowmass = read_snow_data(restart_file, nx, ny,n_snowlayer, TRIM(adjustl('snowmass')))
        lwmass(1:nx, 1:ny, 1:n_snowlayer) = read_snow_data(restart_file, nx, ny, n_snowlayer, TRIM(adjustl('lwmass')))
        snow_temp(1:nx, 1:ny, 1:n_snowlayer) = read_snow_data(restart_file, nx, ny, n_snowlayer, TRIM(adjustl('snowtemp')))
        rho_snow(1:nx, 1:ny, 1:n_snowlayer) = read_snow_data(restart_file, nx, ny, n_snowlayer, TRIM(adjustl('snowdensity')))
	end subroutine load_restart_state


  subroutine update_elevation(smb_ice)
    ! Update elevation and land mask. Sea level remains unchanged even if GrIS mass changes.
    implicit none
    real(kind=8), dimension(nx,ny), intent(in) :: smb_ice
    integer                                    :: ix,iy

    do iy=1,ny,1
      do ix=1,nx,1
        elevation(ix,iy) = max(elevation(ix,iy)-smb_ice(ix,iy),bedrock(ix,iy))
      end do
    end do
    landmask = get_landmask(elevation, sea_level)
  end subroutine

end module bessi_boundary
