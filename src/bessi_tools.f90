module bessi_tools
  ! Helper functions for BESSI wrapper

  use bessi_defs
  use bessi_io
  use bessi_climate

  implicit none

  real :: clock_start
  real :: clock_end
  real :: runtime

  private :: runtime, clock_start, clock_end
  public  :: start_time
  public  :: stop_time
  public  :: print_heartbeat
  public  :: calculate_year
  public  :: its_time
  public  :: is_lowest_snow_box
  public  :: decide_cells_to_run
contains


! ################################################  
  subroutine start_time()
    ! Start time measurement for loop over BESSI
    implicit none

    call cpu_time(clock_start)
  end subroutine start_time


! ################################################  
  subroutine stop_time()
    ! Stop time measurement
    implicit none

    call cpu_time(clock_end)
  end subroutine stop_time


! ################################################  
  subroutine print_heartbeat(myear)
    ! Print heartbeat and some statistics
    implicit none
    integer, intent(in) :: myear

    runtime = clock_end - clock_start

    if(debug > 0 ) then
      print *, '-----------------'
      print *, 'model yrs/hour : ', 3600e0/runtime
      print *, '-----------------'
      print *, 'End of year ', myear
      print *, '========================'
    end if

  end subroutine print_heartbeat


! ################################################
  integer function calculate_year(it) result(myear)
    ! Calculate year from timestep
    implicit none
    integer, intent(in) :: it

    ! Time series of diagnostics
    myear = it! ABo: This is not very clever, but the old code was worse. Maybe calculate_year needs an overhaul.
!     myear = it * int(real(dt)/(3600.*24.*365.)) ! the current simulation year
  end function calculate_year


! ################################################
  logical function its_time(time_in,modulo_in,dt) result(it_is)
    ! Check if the current model time (time_in) is a certain integer multiple (modulo_in), within the precision of the time step (dt)
    ! andreas.born@uib.no
    integer, intent(in)     :: time_in
    integer, intent(in)     :: modulo_in
    real(4), intent(in)     :: dt

    it_is = mod(real(time_in),real(modulo_in)) .lt. dt
  end function its_time

! ################################################
  logical function is_lowest_snow_box(ix,iy,iz) result(it_is)
    ! Check if a grid box is at the bottom of the snow column, either because all lower boxes have a snow mass of zero or because the current box is the deepest in the grid.
    integer, intent(in)     :: ix,iy,iz

    if ((iz .eq. n_snowlayer) .or. (snowmass(ix,iy,iz+1) .le. 0d0)) then
      it_is = .true.
    else
      it_is = .false.
    end if
  end function is_lowest_snow_box


! ################################################
  function decide_cells_to_run(found_water, made_ice, slow_year) result(run_cell)
    ! Decide which cells to run in a given year.

    implicit none
    integer                                :: ix,iy
!     integer,                   intent(in)   :: myear
    logical, dimension(nx,ny), intent(in)  :: found_water
    logical, dimension(nx,ny), intent(in)  :: made_ice
    logical, intent(in)                    :: slow_year
    logical, dimension(nx,ny)              :: run_cell

    do iy = 1, ny, 1
      do ix = 1, nx, 1

        if ((.not. fast_bessi) .or. slow_year) then
!         if ((.not. fast_bessi) .or. its_time(myear,skip_smb_years,5e-1)) then
          ! run everywhere but on water:
          run_cell(ix,iy) = .true.
          if (landmask(ix,iy) .eq. 1) run_cell(ix,iy) = .false.

        else! fast_bessi is .true.
          ! What type of grid cell are we on?
          if (landmask(ix,iy) .eq. 0) then
            ! ICE, don't run unless there is liquid water
            ! If the lowest grid cell over ice doesn't have snow or if no mass is transferred to the ice model,
            ! assume we're not in EQ and need to spin up the model.
            if ((snowmass(ix,iy,n_snowlayer) .eq. 0d0) .or. (.not. made_ice(ix,iy)) .or. found_water(ix,iy)) then
              run_cell(ix,iy)           = .true.
            else
              run_cell(ix,iy)           = .false.
            end if

          end if

          if (landmask(ix,iy) .eq. 1) then
            ! WATER, never run
            run_cell(ix,iy)             = .false.
          end if

          if (landmask(ix,iy) .eq. 2) then
            ! ICE-FREE LAND, run only when next to ice:
            ! This is a bit clunky and also not ideal for glacial inceptions. Maybe use a criterion based on the 'upper' mass
            ! balance, i.e., if the snow mass balance in BESSI is positive. 'made_ice' only checks the 'lower' mass balance.
            if (minval(landmask((ix-1):(ix+1),(iy-1):(iy+1))) .ne. 0) then
!             (landmask(ix-1,iy) .eq. 0) .or. (landmask(ix+1,iy) .eq. 0) .or. (landmask(ix,iy-1) .eq. 0) .or. (landmask(ix,iy+1) .eq. 0)) then
              run_cell(ix,iy)             = .false.
            else
              run_cell(ix,iy)             = .true.
            end if
          end if
        end if! if (.not. fast_bessi)
      end do
    end do

  end function decide_cells_to_run

end module bessi_tools
