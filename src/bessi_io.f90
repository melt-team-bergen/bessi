! I/O for the surface and energy balance model BESSI
! This file contains netcdf writing and reading routines for the climate input and mass and energy balance output
! based on netcdf4, model output uses standard names following the CF-conventions http://cfconventions.org/

module bessi_io

  use bessi_defs
  use netcdf
  
CONTAINS
    ! =======================================================================================================
    ! READ
    ! =======================================================================================================

    !--------------------------------------------------------------
    ! NetCDF Stuff
    !--------------------------------------------------------------

! ################################################  
    function read_variable(filename, NLONS, NLATS, variable)
        ! Reads the variable (8bit real) from a 2D (long x lat, without time) netcdf file.
        !
        ! With inspirations from here:
        ! http://www.unidata.ucar.edu/software/netcdf/examples/programs/simple_xy_rd.f90
        ! http://www.unidata.ucar.edu/software/netcdf/examples/programs/sfc_pres_temp_rd.f

        implicit none

        character(len=*), intent(in) :: filename
        ! input
        integer,          intent(in) :: NLATS
        integer,          intent(in) :: NLONS
        character(*),     intent(in) :: variable

        ! output
        real(kind=8)                 :: read_variable(NLONS, NLATS)

        ! This will be the netCDF ID for the file and data variable.
        integer                      :: ncid, varid

        ! Open the file, Read only
        ! Trim file path: http://stackoverflow.com/questions/15093712/trimming-string-for-directory-path
        call check(nf90_open(TRIM(adjustl(filename)), NF90_NOWRITE, ncid))
        if (debug > 1) print *, "io_read.f90: NetCDF File opened: ", filename

        ! Get the varid of the data variable, based on its name.
        call check(nf90_inq_varid(ncid, variable, varid))
        if (debug > 1) print *, "io_read.f90: NetCDF varid for variable received: ", variable

        ! read the data
        call check(nf90_get_var(ncid, varid, read_variable))
        if (debug > 1) print *, "io_read.f90: NetCDF read the data from the file"

        ! Close the file, freeing all resources.
        call check(nf90_close(ncid))
        if (debug > 1) print *, "io_read.f90: NetCDF file closed"

        ! If we got this far, everything worked as expected. Yipee!
        if (debug > 0) print *,"*** Successful reading the ",TRIM(adjustl(variable))," from NetCDF file: ", TRIM(adjustl(filename)), ""
        return
    end function read_variable

    
! ################################################  
  function get_landmask(elevation_loc, sea_level_loc) result(new_mask)
    ! Returns a watermask
    ! 1 = water, 2 = land

    implicit none
    real(kind=8), intent(in) :: sea_level_loc
    real(kind=8), intent(in) :: elevation_loc(nx,ny)
    integer                  :: new_mask(nx,ny)
    integer                  :: i,j

    ! first set everything to water:
    new_mask = 1
    do i = 1,nx,1
      do j = 1,ny,1
        if(elevation_loc(i,j).gt. sea_level_loc) then
          new_mask(i,j) = 2
        end if
      end do
    end do

        ! At the end, remove the lakes from the watermask
!         call clean_watermask(read_watermask, NLONS, NLATS)

  end function get_landmask

! ################################################  
!     subroutine clean_watermask(watermask, NLONS, NLATS)
!         ! Removes the great lakes from the watermask
!         ! Removes isolated water points sourounded by the land.
!         implicit none
!         integer, intent(in) :: NLONS
!         integer, intent(in) :: NLATS
!         integer, intent(inout) :: watermask(NLONS, NLATS)
!         integer :: ix,iy !local running variables
! 
!         ! Remove the lakes in North America
!         !if (NLONS .eq. 625) then
!         !    watermask(60:300, 120:176) = 2
!         !else
!         !    watermask(40:150, 60:88) = 2
!         !    ! Over whole america
!         !    watermask(50:120, 1:80) = 2
!         !    watermask(120:130, 42:50) = 2
!         !end if
!         ! Remove lakes with the size of one grid box
!         ! 0 = ice
!         ! 1 = water
!         ! 2 = no ice
!         do ix=2,NLONS-1,1
!             do iy=2,NLATS-1,1
!                 ! In X Direction: 2 water boxes, 1 ice boxes
!                 if ( (watermask(ix, iy) .eq. 1) .and. &
!                     (watermask(ix-1, iy) .ne. 1) .and. &
!                     (watermask(ix+1, iy) .ne. 1) .and. &
!                     (watermask(ix, iy-1) .ne. 1) .and. &
!                     (watermask(ix, iy+1) .ne. 1) ) then
! 
!                     watermask(ix:ix, iy:iy) = 2
!                 endif
!             end do
!         end do
! 
! 
!     end subroutine clean_watermask

    !--------------------------------------------------------------
    ! NetCDF Stuff
    !--------------------------------------------------------------

! ################################################  
    ! special function to initialize netcdf file for climate data
    subroutine init_netcdf_climate_file(filename, ncid, nlats, nlons, lat_distance, long_distance, &
        precip_varid, temp_varid, swrad_varid, dev_precip_varid, dev_temp_varid, dev_swrad_varid)

        ! Init ncdf file to write into it.
        ! param filename: path and filename to the file
        ! param ncid: filehandle for the ncid file, will be overwritten
        ! param nlats: Number of latititudes (integer)
        ! param nlons: Number of longitued (integer)
        ! param lat_distance: Length in m between the points of latitude
        ! param long_distance: Length in m between the points of longitude
        !
        ! Initialise the variables to write in 2D data with time dependency
        ! Prepare the following variables to write into the file:
        ! - Thickness (2D Real Array): varid =
        ! - Bedrock (2D Real Array)
        ! - Elevation Line Position (2D Real Array)
        ! - Mass Balance (in preparation)


        use netcdf
        implicit none
        character(len=*), intent(in) :: filename
        integer, intent(out) :: ncid
        ! input
        integer, intent(in) :: NLATS
        integer, intent(in) :: NLONS
        integer, intent(in) :: lat_distance
        integer, intent(in) :: long_distance
        ! output
        integer, intent(out) :: precip_varid!     Variable for the netCDF Height, parameter
        integer, intent(out) :: temp_varid!       Variable for the netCDF Bedrock, parameter
        integer, intent(out) :: dev_precip_varid! Variable for the netCDF accumulation, parameter
        integer, intent(out) :: dev_temp_varid!   Variable for the netCDF Diffusivity, parameter
        integer, intent(out) :: swrad_varid!      Variable for the netCDF Diffusivity, parameter
        integer, intent(out) :: dev_swrad_varid!  Variable for the netCDF Diffusivity, parameter

        ! Copied from Example file!
        !--------------------------
        ! We are writing 2D data with time, We will need 3 netCDF dimensions. (Lats, Long, Time)
        integer, parameter :: NDIMS = 3

        character*(*) :: LAT_NAME, LON_NAME, REC_NAME

        parameter (LAT_NAME='y', LON_NAME='x')
        parameter (REC_NAME = 'time')
        integer lon_dimid, lat_dimid, rec_dimid

        ! The start and count arrays will tell the netCDF library where to
        ! write our data.
        !integer start(NDIMS), count(NDIMS)

        ! In addition to the latitude and longitude dimensions, we will also
        ! create latitude and longitude netCDF variables which will hold the
        ! actual latitudes and longitudes. Since they hold data about the
        ! coordinate system, the netCDF term for these is: "coordinate
        ! variables."
        real :: lats(NLATS)
        real :: lons(NLONS)
        integer lat_varid, lon_varid, rec_varid
        real START_LAT, START_LON
        parameter (START_LAT = 0, START_LON = 0)
        !
        !    ! Variables
        character*(*) PRECIP_NAME, TEMP_NAME, DEV_PRECIP_NAME, DEV_TEMP_NAME, SWRAD_NAME, DEV_SWRAD_NAME
        parameter (PRECIP_NAME='precip')
        parameter (TEMP_NAME='temp')
        parameter (DEV_PRECIP_NAME='dev_precip')
        parameter (DEV_TEMP_NAME='dev_temp')
        parameter (SWRAD_NAME='swrad')
        parameter (DEV_SWRAD_NAME='dev_swrad')

        integer dimids(NDIMS)
        !
        !    ! It's good practice for each variable to carry a "units" attribute.
        character*(*) :: UNITS
        parameter (UNITS = 'units')
        character*(*) PRECIP_UNITS, TEMP_UNITS, DEV_PRECIP_UNITS, LAT_UNITS, LON_UNITS, &
             DEV_TEMP_UNITS, REC_UNITS, SWRAD_UNITS, DEV_SWRAD_UNITS

        parameter (PRECIP_UNITS = 'm/s', TEMP_UNITS = 'K', DEV_PRECIP_UNITS = 'm/s', &
             DEV_TEMP_UNITS = 'K', SWRAD_UNITS = 'W/m2', DEV_SWRAD_UNITS = 'W/m2' )
        ! Units of Dimensions
        parameter (LAT_UNITS = 'm')
        parameter (LON_UNITS = 'm')
        parameter (REC_UNITS = '1000years')
	!character*(*) :: dummy_string
        integer :: lat,lon !local loop variables

	!parameter (REC_UNITS = trim(adjustl(dummy_string)))

        ! Distance of the lat/long points
        do lat = 1, NLATS
            lats(lat) = START_LAT + (lat - 1) * lat_distance
        end do
        do lon = 1, NLONS
            lons(lon) = START_LON + (lon - 1) * long_distance
        end do

        !print *, '*** Init NetCDF file: ', TRIM(adjustl(filename))

        ! Create the file.
        call check(nf90_create(path=filename, cmode=nf90_clobber, ncid=ncid))
        !
        !
        !    ! Define dimensions
        call check(nf90_def_dim(ncid, LAT_NAME, NLATS, lat_dimid))
        call check(nf90_def_dim(ncid, LON_NAME, NLONS, lon_dimid))
        !    ! Time dimension
        call check(nf90_def_dim(ncid, REC_NAME, NF90_UNLIMITED, rec_dimid))

        ! Define the coordinate variables. They will hold the coordinate
        ! information, that is, the latitudes and longitudes. A varid is
        ! returned for each.
        !if (gridtype.eq.0) then !lon lat regular grid
        call check(nf90_def_var(ncid, LAT_NAME, NF90_DOUBLE, lat_dimid, lat_varid))
        call check(nf90_def_var(ncid, LON_NAME, NF90_DOUBLE, lon_dimid, lon_varid))
        !elseif (gridtype .eq. 1) then ! curvilinear grid (not regular lon lat)
        !   dimids2D(1)=x_dimid
        !   dimids2D(2)=y_dimid
        !   call check(nf90_def_var(ncid, LON_NAME, NF90_REAL, dimids2D, lon_varid))
        !   call check(nf90_def_var(ncid, LAT_NAME, NF90_REAL, dimids2D, lat_varid))
        !else
        !   print*,'ERROR gridtype must be 0 or 1 !!!'
        !   stop
        !endif


        ! Time
        call check(nf90_def_var(ncid, REC_NAME, NF90_DOUBLE, rec_dimid, rec_varid))

        ! Assign units attributes to coordinate var data. This attaches a
        ! text attribute to each of the coordinate variables, containing the
        ! units.
        call check(nf90_put_att(ncid, lat_varid, UNITS, LAT_UNITS))
        call check(nf90_put_att(ncid, lon_varid, UNITS, LON_UNITS))
        ! Time
        call check(nf90_put_att(ncid, rec_varid, UNITS, REC_UNITS))

        ! Define the netCDF variables. The dimids array is used to pass the
        ! dimids of the dimensions of the netCDF variables.
        dimids(1) = lon_dimid
        dimids(2) = lat_dimid
        dimids(3) = rec_dimid

        ! Define the netCDF variables for the "real" values
        call check(nf90_def_var(ncid, PRECIP_NAME, NF90_DOUBLE, dimids, precip_varid))
        call check(nf90_def_var(ncid, TEMP_NAME, NF90_DOUBLE, dimids, temp_varid))
        call check(nf90_def_var(ncid, DEV_PRECIP_NAME, NF90_DOUBLE, dimids, dev_precip_varid))
        call check(nf90_def_var(ncid, DEV_TEMP_NAME, NF90_DOUBLE, dimids, dev_temp_varid))
        call check(nf90_def_var(ncid, SWRAD_NAME, NF90_DOUBLE, dimids, swrad_varid))
        call check(nf90_def_var(ncid, DEV_SWRAD_NAME, NF90_DOUBLE, dimids, dev_swrad_varid))

        ! Assign units attributes to the pressure and temperature netCDF
        ! variables.
        call check(nf90_put_att(ncid, precip_varid, UNITS, PRECIP_UNITS))
        call check(nf90_put_att(ncid, temp_varid, UNITS, TEMP_UNITS))
        call check(nf90_put_att(ncid, dev_precip_varid, UNITS, DEV_PRECIP_UNITS))
        call check(nf90_put_att(ncid, dev_temp_varid, UNITS, DEV_TEMP_UNITS))
        call check(nf90_put_att(ncid, swrad_varid, UNITS, SWRAD_UNITS))
        call check(nf90_put_att(ncid, dev_swrad_varid, UNITS, DEV_SWRAD_UNITS))

        ! End define mode.
        call check(nf90_enddef(ncid))

        ! Write the coordinate variable data. This will put the latitudes
        ! and longitudes of our data grid into the netCDF file.
        call check(nf90_put_var(ncid, lat_varid, lats))
        call check(nf90_put_var(ncid, lon_varid, lons))
        if (debug > 2) print *,'*** Output netCDF file defined: ', TRIM(adjustl(filename))

    end subroutine init_netcdf_climate_file
    ! subroutine to open a netcdf file for accumulation data


! ################################################  
    ! subroutine to close an open netcdf file
    subroutine closeNCDFFile(ncid)
        implicit none
        integer, intent(in) :: ncid

        ! Close the file.
        call check(nf90_close(ncid))

    end subroutine closeNCDFFile


! ################################################  
    subroutine writeNCDFGridValues(ncid, year, varid, values, nlats, nlons)
        ! Writes the Values (Real(kind=8)) into the netCDF File
        ! Param ncid: File handle of the netCDF File
        ! Param year: The year of the symmulation (integer)
        ! Param varid: Variable ID (a, bedrock, ...) the values belong to
        ! Param values: Array (lon, lat) with values as REAL(kind=8)
        ! param nlats: Number of latititudes (integer)
        ! param nlons: Number of longitued (integer)
        !
        ! Flushs the data to the disk afterwards
        implicit none
        ! Parameters
        integer, intent(in) :: ncid
        integer, intent(in) :: year
        integer, intent(in) :: varid
        integer, intent(in) :: nlats
        integer, intent(in) :: nlons
        real(kind=4), dimension(nlons,nlats), intent(in) :: values

        ! We are writing 2D data with time, We will need 3 netCDF dimensions. (Lats, Long, Time)
        integer, parameter :: NDIMS = 3

        ! The start and count arrays will tell the netCDF library where to
        ! write our data.
        integer start(NDIMS), count(NDIMS)


        ! These settings tell netcdf to write one timestep of data. (The
        ! setting of start(3) inside the loop below tells netCDF which
        ! timestep to write.)
        count(1) = NLONS
        count(2) = NLATS
        count(3) = 1
        start(1) = 1
        start(2) = 1
        start(3) = year

        ! Write the pretend data. This will write the data.
        ! The arrays only hold one timestep worth of data.
        call check(nf90_put_var(ncid, varid, values, start, count))

        ! Flush data to the disk
        call check(nf90_sync(ncid))

    end subroutine writeNCDFGridValues


! ################################################  
    subroutine writeNCDFGridIntegerValues(ncid, year, varid, values, nlats, nlons)
        ! Writes the Values (Integer) into the netCDF File
        ! Param ncid: File handle of the netCDF File
        ! Param year: The year of the symmulation (integer)
        ! Param varid: Variable ID (a, bedrock, ...) the values belong to
        ! Param values: Array (lon, lat) with values as Integer
        ! param nlats: Number of latititudes (integer)
        ! param nlons: Number of longitued (integer)
        !
        ! Flushs the data to the disk afterwards
        implicit none
        ! Parameters
        integer, intent(in) :: ncid
        integer, intent(in) :: year
        integer, intent(in) :: varid
        integer, intent(in) :: nlats
        integer, intent(in) :: nlons
        integer, dimension(nlons,nlats), intent(in) :: values

        ! We are writing 2D data with time, We will need 3 netCDF dimensions. (Lats, Long, Time)
        integer, parameter :: NDIMS = 3

        ! The start and count arrays will tell the netCDF library where to
        ! write our data.
        integer start(NDIMS), count(NDIMS)


        ! These settings tell netcdf to write one timestep of data. (The
        ! setting of start(3) inside the loop below tells netCDF which
        ! timestep to write.)
        count(1) = NLONS
        count(2) = NLATS
        count(3) = 1
        start(1) = 1
        start(2) = 1
        start(3) = year

        ! Write the pretend data. This will write the data.
        ! The arrays only hold one timestep worth of data.
        call check(nf90_put_var(ncid, varid, values, start, count))

        ! Flush data to the disk
        call check(nf90_sync(ncid))

    end subroutine writeNCDFGridIntegerValues


    !33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
    ! ----------------------- 3D NetCDF  -----------------------------------------------------
    !33333333333333333333333333333333333333333333333333333333333333333333333333333333333333333

! ################################################  
      subroutine writeNCDFSNOW3DValues(ncid, year, varid, values, nlats, nlons, N_LAYER, N_REC)
        ! Writes the Values (Real(kind=4)) into the netCDF File
        ! Param ncid: File handle of the netCDF File
        ! Param year: The year of the symmulation (integer)
        ! Param varid: Variable ID (a, bedrock, ...) the values belong to
        ! Param values: Array (lon, lat) with values as REAL(kind=8)
        ! param nlats: Number of latititudes (integer)
        ! param nlons: Number of longitud (integer)
        ! param N_LAYER: Number of vertical layers (integer)
        ! param N_REC: Number of records 1 for annual, 365 for daily (integer)
        !
        ! Flushs the data to the disk afterwards
        implicit none
        ! Parameters
        integer, intent(in) :: ncid
        integer, intent(in) :: year
        integer, intent(in) :: varid
        integer, intent(in) :: nlats
        integer, intent(in) :: nlons
        integer, intent(in) :: N_LAYER
        integer, intent(in) :: N_REC
        real(kind=4), dimension(nlons,nlats,N_LAYER,N_REC), intent(in) :: values

        ! We are writing 3D data with time, We will need 4 netCDF dimensions. (Lats, Long, z, Time)
        integer, parameter :: NDIMS = 4

        ! The start and count arrays will tell the netCDF library where to
        ! write our data.
        integer, dimension(NDIMS) :: start, count


        ! These settings tell netcdf to write one timestep of data. (The
        ! setting of start(3) inside the loop below tells netCDF which
        ! timestep to write.)
        count(1) = NLONS
        count(2) = NLATS
        count(3) = N_LAYER
        count(4) = N_REC
        start(1) = 1
        start(2) = 1
        start(3) = 1
        start(4) = year
        
        ! Write the pretend data. This will write the data.
        ! The arrays only hold one timestep worth of data.
        call check(nf90_put_var(ncid, varid, values, start, count))
        !if (retval .ne. nf_noerr) then
        !    write(*,*) start
        !    write(*,*) count
        !    write(*,*) 'shape variable: ',shape(values)
        !    print *, 'Could not write 3D variable to NetCDF: ', varid
        !end if

        ! Flush data to the disk
        call check(nf90_sync(ncid))

    end subroutine writeNCDFSNOW3DValues
    

! ################################################  
    subroutine writeNCDFSNOW2DValues(ncid, year, varid, values, nlats, nlons, N_REC)
        ! Writes the Values (Real(kind=4)) into the netCDF File
        ! Param ncid: File handle of the netCDF File
        ! Param year: The year of the symmulation (integer)
        ! Param varid: Variable ID (a, bedrock, ...) the values belong to
        ! Param values: Array (lon, lat) with values as REAL(kind=8)
        ! param nlats: Number of latititudes (integer)
        ! param nlons: Number of longitued (integer)
        !
        ! Flushs the data to the disk afterwards
        !used for the Annual data
        implicit none
        ! Parameters
        integer, intent(in) :: ncid
        integer, intent(in) :: year
        integer, intent(in) :: varid
        integer, intent(in) :: nlats
        integer, intent(in) :: nlons
        integer, intent(in) :: N_REC
        real(kind=4), dimension(nlons,nlats,N_REC), intent(in) :: values

        ! We are writing 2D data with time, We will need 3 netCDF dimensions. (Lats, Long, Time)
        integer, parameter :: NDIMS = 3

        ! The start and count arrays will tell the netCDF library where to
        ! write our data.
        integer, dimension(NDIMS) :: start, count


        ! These settings tell netcdf to write one timestep of data. (The
        ! setting of start(3) inside the loop below tells netCDF which
        ! timestep to write.)
        count(1) = NLONS
        count(2) = NLATS
        count(3) = N_REC
        start(1) = 1
        start(2) = 1
        start(3) = year

        ! Write the pretend data. This will write the data.
        ! The arrays only hold one timestep worth of data.
        call check(nf90_put_var(ncid, varid, values, start, count))

        ! Flush data to the disk
        call check(nf90_sync(ncid))

      end subroutine writeNCDFSNOW2DValues

      subroutine writeNCDFSNOW2DIntValues(ncid, year, varid, values, nlats, nlons,N_REC)
        ! Writes the Values (Integer) into the netCDF File
        ! Param ncid: File handle of the netCDF File
        ! Param year: The year of the symmulation (integer)
        ! Param varid: Variable ID (a, bedrock, ...) the values belong to
        ! Param values: Array (lon, lat) with values as INTEGER
        ! param nlats: Number of latititudes (integer)
        ! param nlons: Number of longitued (integer)
        !
        ! Flushs the data to the disk afterwards
        !used for the Annual data
        implicit none
        ! Parameters
        integer, intent(in) :: ncid
        integer, intent(in) :: year
        integer, intent(in) :: varid
        integer, intent(in) :: nlats
        integer, intent(in) :: nlons
        integer, intent(in) :: N_REC
        integer, dimension(nlons,nlats,N_REC), intent(in) :: values

        ! We are writing 2D data with time, We will need 4 netCDF dimensions. (Lats, Long, Time)
        integer, parameter :: NDIMS = 3

        ! The start and count arrays will tell the netCDF library where to
        ! write our data.
        integer, dimension(3) :: start, count


        ! These settings tell netcdf to write one timestep of data. (The
        ! setting of start(3) inside the loop below tells netCDF which
        ! timestep to write.)
        count(1) = NLONS
        count(2) = NLATS
        count(3) = N_REC
        start(1) = 1
        start(2) = 1
        start(3) = year

        ! Write the pretend data. This will write the data.
        ! The arrays only hold one timestep worth of data.
        call check(nf90_put_var(ncid, varid, values, start, count))

        ! Flush data to the disk
        call check(nf90_sync(ncid))

    end subroutine writeNCDFSNOW2DIntValues
    
    subroutine writeNCDFSNOW2D_pointwise(ncid, year, varid, values, iy, ix)
        ! Writes the Value (Real(kind=8)) for 1 point into the netCDF File
        ! Param ncid: File handle of the netCDF File
        ! Param year: The year of the symmulation (integer)
        ! Param varid: Variable ID (a, bedrock, ...) the values belong to
        ! Param values: Array (lon, lat) with values as REAL(kind=8)
        ! param nlats: Number of latititudes (integer)
        ! param nlons: Number of longitued (integer)
        !
        ! Flushs the data to the disk afterwards
        ! This routine saves the data pointwise 
        implicit none
        ! Parameters
        integer, intent(in) :: ncid
        integer, intent(in) :: year
        integer, intent(in) :: varid
        integer, intent(in) :: iy
        integer, intent(in) :: ix
        real(kind=4), intent(in) :: values

        ! We are writing 2D data with time, We will need 3 netCDF dimensions. (Lats, Long, Time)
        integer, parameter :: NDIMS = 3

        ! The start and count arrays will tell the netCDF library where to
        ! write our data.
        integer, dimension(3) :: start

!         print*,values
        ! These settings tell netcdf to write one timestep of data. (The
        ! setting of start(3) inside the loop below tells netCDF which
        ! timestep to write.)
        start(1) = ix
        start(2) = iy
        start(3) = year

        ! Write the pretend data. This will write the data.
        ! The arrays only hold one timestep worth of data.
        call check(nf90_put_var(ncid, varid, values, start))

        ! Flush data to the disk
        call check(nf90_sync(ncid))

    end subroutine writeNCDFSNOW2D_pointwise
    
    
subroutine writeNCDFSNOW2D_pointwise_t_xy(ncid, starttime, varid, values, iy, ix, endtime)
        ! Writes the Values (Real(kind=8)) into the netCDF File
        ! Param ncid: File handle of the netCDF File
        ! Param year: The year of the symmulation (integer)
        ! Param varid: Variable ID (a, bedrock, ...) the values belong to
        ! Param values: Array (lon, lat) with values as REAL(kind=8)
        ! param nlats: Number of latititudes (integer)
        ! param nlons: Number of longitued (integer)
        !
        ! Flushs the data to the disk afterwards
        implicit none
        ! Parameters
        integer, intent(in) :: ncid
        integer, intent(in) :: starttime
        integer, intent(in) :: endtime
        integer, intent(in) :: varid
        integer, intent(in) :: iy
        integer, intent(in) :: ix
        real(kind=4), dimension(endtime-starttime), intent(in) :: values

        ! We are writing 3D data with time, We will need 4 netCDF dimensions. (Lats, Long, z, Time)
        integer, parameter :: NDIMS = 4
        
        !local variable timestep
        integer :: timestep

        ! The start and count arrays will tell the netCDF library where to
        ! write our data.
        integer start(NDIMS), count(NDIMS)

        timestep=endtime-starttime+1
!         print*,values
        ! These settings tell netcdf to write one timestep of data. (The
        ! setting of start(3) inside the loop below tells netCDF which
        ! timestep to write.)
        count(1) = timestep
        count(2) = 1
        count(3) = 1
        start(1) = starttime
        start(2) = ix
        start(3) = iy

        ! Write the pretend data. This will write the data.
        ! The arrays only hold one timestep worth of data.
        call check(nf90_put_var(ncid, varid, values, start, count))

        ! Flush data to the disk
        call check(nf90_sync(ncid))

    end subroutine writeNCDFSNOW2D_pointwise_t_xy
    
    subroutine writeNCDFSNOW3D_pointwise_t_zxy(ncid, starttime, varid, values, iy, ix, N_LAYER, endtime)
        ! Writes the Values (Real(kind=8)) into the netCDF File
        ! Param ncid: File handle of the netCDF File
        ! Param year: The year of the symmulation (integer)
        ! Param varid: Variable ID (a, bedrock, ...) the values belong to
        ! Param values: Array (lon, lat) with values as REAL(kind=8)
        ! param nlats: Number of latititudes (integer)
        ! param nlons: Number of longitued (integer)
        !
        ! Flushs the data to the disk afterwards
        ! used for the daily and monthly data output, reorder of variables reduces the writing time significantly
        implicit none
        ! Parameters
        integer, intent(in) :: ncid
        integer, intent(in) :: starttime
        integer, intent(in) :: endtime
        integer, intent(in) :: varid
        integer, intent(in) :: iy
        integer, intent(in) :: ix
        integer, intent(in) :: N_LAYER
        real(kind=4), dimension(365,N_LAYER), intent(in) :: values

        ! We are writing 3D data with time, We will need 4 netCDF dimensions. (Lats, Long, z, Time)
        integer, parameter :: NDIMS = 4
        
        !local variable timestep
        integer :: timestep

        ! The start and count arrays will tell the netCDF library where to
        ! write our data.
        integer start(NDIMS), count(NDIMS)

        timestep=endtime-starttime+1
!         print*,values
        ! These settings tell netcdf to write one timestep of data. (The
        ! setting of start(3) inside the loop below tells netCDF which
        ! timestep to write.)
        count(1) = timestep
        count(2) = N_LAYER
        count(3) = 1
        count(4) = 1
        start(1) = starttime
        start(2) = 1
        start(3) = ix
        start(4) = iy

        ! Write the pretend data. This will write the data.
        ! The arrays only hold one timestep worth of data.
        call check(nf90_put_var(ncid, varid, values, start, count))

        ! Flush data to the disk
        call check(nf90_sync(ncid))

    end subroutine writeNCDFSNOW3D_pointwise_t_zxy

    subroutine writeNCDFSNOW3B_pointwise_time_interval(ncid, year, varid, values, iy, ix, N_LAYER, endtime)
        ! Writes the Values (Real(kind=8)) into the netCDF File
        ! Param ncid: File handle of the netCDF File
        ! Param year: The year of the symmulation (integer)
        ! Param varid: Variable ID (a, bedrock, ...) the values belong to
        ! Param values: Array (lon, lat) with values as REAL(kind=8)
        ! param nlats: Number of latititudes (integer)
        ! param nlons: Number of longitued (integer)
        !
        ! Flushs the data to the disk afterwards
        use netcdf
        implicit none
        ! Parameters
        integer, intent(in) :: ncid
        integer, intent(in) :: year
        integer, intent(in) :: endtime
        integer, intent(in) :: varid
        integer, intent(in) :: iy
        integer, intent(in) :: ix
        integer, intent(in) :: N_LAYER
        real(kind=4), dimension(N_LAYER,365), intent(in) :: values

        ! We are writing 3D data with time, We will need 4 netCDF dimensions. (Lats, Long, z, Time)
        integer, parameter :: NDIMS = 4
        
        !local variable timestep
        integer :: timestep

        ! The start and count arrays will tell the netCDF library where to
        ! write our data.
        integer start(NDIMS), count(NDIMS)

        timestep=endtime-year+1
!         print*,values
        ! These settings tell netcdf to write one timestep of data. (The
        ! setting of start(3) inside the loop below tells netCDF which
        ! timestep to write.)
        count(1) = 1
        count(2) = 1
        count(3) = N_LAYER
        count(4) = endtime
        start(1) = ix
        start(2) = iy
        start(3) = 1
        start(4) = 1

        ! Write the pretend data. This will write the data.
        ! The arrays only hold one timestep worth of data.
        call check(nf90_put_var(ncid, varid, values, start, count))

        ! Flush data to the disk
        call check(nf90_sync(ncid))

    end subroutine writeNCDFSNOW3B_pointwise_time_interval
    
    subroutine writeNCDF3DGridValues(ncid, year, varid, values, nlats, nlons, N_LAYER)
        ! Writes the Values (Real(kind=8)) into the netCDF File
        ! Param ncid: File handle of the netCDF File
        ! Param year: The year of the symmulation (integer)
        ! Param varid: Variable ID (a, bedrock, ...) the values belong to
        ! Param values: Array (lon, lat) with values as REAL(kind=8)
        ! param nlats: Number of latititudes (integer)
        ! param nlons: Number of longitued (integer)
        !
        ! Flushs the data to the disk afterwards
        implicit none
        ! Parameters
        integer, intent(in) :: ncid
        integer, intent(in) :: year
        integer, intent(in) :: varid
        integer, intent(in) :: nlats
        integer, intent(in) :: nlons
        integer, intent(in) :: N_LAYER
        real(kind=4), dimension(nlons,nlats,N_LAYER), intent(in) :: values

        ! We are writing 3D data with time, We will need 4 netCDF dimensions. (Lats, Long, z, Time)
        integer, parameter :: NDIMS = 4

        ! The start and count arrays will tell the netCDF library where to
        ! write our data.
        integer start(NDIMS), count(NDIMS)


        ! These settings tell netcdf to write one timestep of data. (The
        ! setting of start(3) inside the loop below tells netCDF which
        ! timestep to write.)
        count(1) = NLONS
        count(2) = NLATS
        count(3) = N_LAYER
        count(4) = 1
        start(1) = 1
        start(2) = 1
        start(3) = 1
        start(4) = year

        ! Write the pretend data. This will write the data.
        ! The arrays only hold one timestep worth of data.
        call check(nf90_put_var(ncid, varid, values, start, count))

        ! Flush data to the disk
        call check(nf90_sync(ncid))

    end subroutine writeNCDF3DGridValues
    
    function read_snow_data(filename, NLONS, NLATS, NLAYER, variable)
        ! Reads an firn initialization file from an output file of this model for restarts etc.
        ! used in case of restart = .true.
        implicit none
        ! input
        character(len=*), intent(in) :: filename
        character(len=*), intent(in) :: variable
        integer, intent(in) :: NLATS
        integer, intent(in) :: NLONS
        integer, intent(in) :: NLAYER

        ! Zeitschritte
        ! ndays

        ! output
        ! TODO: Zeitschritt einbinden
        real(kind=8) :: read_snow_data(NLONS, NLATS, NLAYER)

        ! This will be the netCDF ID for the file and data variable.
        integer :: ncid, varid
        
        
        !print*,filename
        call check(nf90_open(TRIM(adjustl(filename)), nf90_nowrite, ncid))

        ! Read Data
        ! Get the varid of the data variable, based on its name.
        call check(nf90_inq_varid(ncid, TRIM(adjustl(variable)), varid))

        ! read the data
        call check(nf90_get_var(ncid, varid, read_snow_data))
        
        ! Close the file, freeing all resources.
        call check(nf90_close(ncid))

        if(debug > 0) print *,"*** Read ", TRIM(adjustl(variable))," as initial profile from NetCDF file: ", TRIM(adjustl(filename)), ""

        return

      end function read_snow_data

    function read_snow_data2D(filename, NLONS, NLATS, variable)
        ! Reads an firn initialization file from an output file of this model for restarts etc.
        ! used in case of restart = .true.
        implicit none
        ! input
        character(len=*), intent(in) :: filename
        character(len=*), intent(in) :: variable
        integer, intent(in) :: NLATS
        integer, intent(in) :: NLONS

        ! output
        real(kind=8) :: read_snow_data2D(NLONS, NLATS)

        ! This will be the netCDF ID for the file and data variable.
        integer :: ncid, varid
        
        !print*,filename
        call check(nf90_open(TRIM(adjustl(filename)), nf90_nowrite, ncid))

        ! Read Data
        ! Get the varid of the data variable, based on its name.
        call check(nf90_inq_varid(ncid, TRIM(adjustl(variable)), varid))

        ! read the data
        call check(nf90_get_var(ncid, varid, read_snow_data2D))
        
        ! Close the file, freeing all resources.
        call check(nf90_close(ncid))

        if(debug .gt. 1) print *,"*** Read ", TRIM(adjustl(variable))," as initial profile from NetCDF file: ", TRIM(adjustl(filename)), ""

        return

      end function read_snow_data2D
     

    subroutine read_climate_once(filename, NLONS, NLATS, &
        variable1, variable2, variable3, variable4, variable5,  & !variable6 wind parameter, not used
        read_climate_long_1, read_climate_long_2, read_climate_long_3, read_climate_long_4, read_climate_long_5) ! & read_climate_long_6 wind parameter, not used
!     subroutine read_climate_once(filename, NLONS, NLATS, variable1, variable2, variable3, variable4, variable5, variable6, &
!         read_climate_long_1, read_climate_long_2, read_climate_long_3, read_climate_long_4, read_climate_long_5,read_climate_long_6,ndays_climate)
        ! Reads the temperature or precipitation data (8bit real) for 365 days out of the given NetCDF File.
        ! variable: prec/temp, units: m/yr and kelvin
        ! Returns 3D Array: Lon, Lat, Day

        implicit none
        ! input
        character(len=*), intent(in) :: filename
        character(len=*), intent(in) :: variable1
        character(len=*), intent(in) :: variable2
        character(len=*), intent(in) :: variable3
        character(len=*), intent(in) :: variable4
        character(len=*), intent(in) :: variable5
!         character(len=*), intent(in) :: variable6
        integer, intent(in) :: NLATS
        integer, intent(in) :: NLONS

        ! output
        ! TODO: Zeitschritt einbinden
        real(kind=8),intent(out) :: read_climate_long_1(NLONS, NLATS, 365)
        real(kind=8),intent(out) :: read_climate_long_2(NLONS, NLATS, 365)
        real(kind=8),intent(out) :: read_climate_long_3(NLONS, NLATS, 365)
        real(kind=8),intent(out) :: read_climate_long_4(NLONS, NLATS, 365)
        real(kind=8),intent(out) :: read_climate_long_5(NLONS, NLATS, 365)
!         real(kind=8),intent(out) :: read_climate_long_6(NLONS, NLATS, 365)
! ABom 18-Oct-2024, Christophe's code, apparently for leap years, not used here
!         integer,intent(out) :: ndays_climate

        ! This will be the netCDF ID for the file and data variable.
        integer :: ncid, varid, timeid

        ! Open the file, Read only
        ! Trim file path: http://stackoverflow.com/questions/15093712/trimming-string-for-directory-path
        call check(nf90_open(TRIM(adjustl(filename)), nf90_nowrite, ncid))

!         ! ### ndays:
!         ! Get the id for dimension time
!         call check(nf90_inq_dimid(ncid, "time", timeid))
!         ! Get the number of days in this file (max 366)
!         call check(nf90_inquire_dimension(ncid, timeid, len = ndays_climate))
!         if(debug > 1) print *,"*** Read ndays_climate = ",ndays_climate
        
        ! ### VARIABLE 1:
        ! Get the varid of the data variable, based on its name.
        call check(nf90_inq_varid(ncid, TRIM(adjustl(variable1)), varid))
        call check(nf90_get_var(ncid, varid, read_climate_long_1))
        if(debug > 1) print *,"*** Read ", TRIM(adjustl(variable1))," from NetCDF file: ", TRIM(adjustl(filename)), ""

        ! ### VARIABLE 2:
        call check(nf90_inq_varid(ncid, TRIM(adjustl(variable2)), varid))
        call check(nf90_get_var(ncid, varid, read_climate_long_2))
        if(debug > 1) print *,"*** Read ", TRIM(adjustl(variable2))," from NetCDF file: ", TRIM(adjustl(filename)), ""

        ! ### VARIABLE 3:
        call check(nf90_inq_varid(ncid, TRIM(adjustl(variable3)), varid))
        call check(nf90_get_var(ncid, varid, read_climate_long_3))
        if(debug > 1) print *,"*** Read ", TRIM(adjustl(variable3))," from NetCDF file: ", TRIM(adjustl(filename)), ""

        ! ### VARIABLE 4:
        call check(nf90_inq_varid(ncid, TRIM(adjustl(variable4)), varid))
        call check(nf90_get_var(ncid, varid, read_climate_long_4))
        if(debug > 1) print *,"*** Read ", TRIM(adjustl(variable4))," from NetCDF file: ", TRIM(adjustl(filename)), ""

        ! ### VARIABLE 5:
        call check(nf90_inq_varid(ncid, TRIM(adjustl(variable5)), varid))
        call check(nf90_get_var(ncid, varid, read_climate_long_5))
        if(debug > 1) print *,"*** Read ", TRIM(adjustl(variable5))," from NetCDF file: ", TRIM(adjustl(filename)), ""
        
!         ! ### VARIABLE 6:
!         call check(nf90_inq_varid(ncid, TRIM(adjustl(variable6)), varid))
!         call check(nf90_get_var(ncid, varid, read_climate_long_6(:,:,1:ndays_climate)))
!         if(debug > 1) print*,"*** Read ", TRIM(adjustl(variable6))," from NetCDF file: ", TRIM(adjustl(filename)), ""

        ! Close the file, freeing all resources.
        call check(nf90_close(ncid))
        
        return

    end subroutine read_climate_once

    ! --------------------------------------------------------------
    ! SAVE SOME PARAMETERS TO TXT FILE
    ! --------------------------------------------------------------
    subroutine save_parameters(path,date,description)

    implicit none
	character(len=*), intent(in) :: path
	character(len=*), intent(in) :: date
	character(len=*), intent(in) :: description

	open (1337, file = trim(adjustl(path)) // 'values.txt', form='formatted')

	write(1337,*) "Date: ",  trim(adjustl(date))
	write(1337,*) "Experiment description: ", trim(adjustl(description))
	write(1337,*)
	write(1337,*) 'Climate input directory: ', climate_input_dir
	write(1337,*) 'Climate type = ', climate_type
	if (climate_type == 'backandforth_climate') then
	  write(1337,*) 'Climate period = ', clim_year_start, '-',  clim_year_turn
	end if
	write(1337,*)'Resolution =' , dx, '*', dy
	write(1337,*)'Gridsize =', L, '*', W
	write(1337,*)'Gridpoints =', nx, '*', ny
	write(1337,*)'Albedo_module, 1 = constant, 2 = harmonic, 3 = oerlemans, 4 = Aoki 5 Bougamont', albedo_module
	write(1337,*)'latent_heat_flux_on=', latent_heat_flux_on
    write(1337,*)'D_lf analog to D_sf:', latent_heat_flux_analog_to_sensible_heat_flux
	write(1337,*)
	write(1337,*) "Output parameters of simulation:"
	write(1337,*)
	!	write(1337,*)' = ', 
	write(1337,*)'maxyears = ', maxyears
	write(1337,*)'Which data is written = ','annual:',annual_data, 'monthly:',monthly_data, 'daily:', daily_data
	write(1337,*)'write annual data ever x years = ', annual_data_freq
	write(1337,*)'write daily data ever x years = ', daily_data_freq
	write(1337,*)'write monthly data ever x years = ', monthly_data_freq
    write(1337,*)'write detailed data = ', daily_data_start, '-', daily_data_end
    write(1337,*)
	write(1337,*)'n_snowlayer = ', n_snowlayer
	write(1337,*)'soll_mass = ', soll_mass
	write(1337,*)'lower_massbound = ', lower_massbound
	write(1337,*)'upper_massbound = ', upper_massbound
	write(1337,*)
	write(1337,*) "Physical constants used in this simulation:"
	write(1337,*)
	write(1337,*)'albedo_snow_new = ', albedo_snow_new
	write(1337,*)'albedo_snow_wet = ', albedo_snow_wet
	write(1337,*)'albedo_ice = ', albedo_ice
	write(1337,*)'D_sf = ', D_sf
	write(1337,*)'ratio = ', ratio
    write(1337,*)'effective D_lf = ', ratio*D_sf/1003.*0.622*(L_v+L_lh)/10000.
	write(1337,*)'eps_air = ', eps_air
	write(1337,*)'latent_heat_flux = ', latent_heat_flux_on
	write(1337,*)'albedo_module = ', albedo_module
	write(1337,*)'max_lwc = ', max_lwc
	write(1337,*)
	write(1337,*) "Input_files"
	write(1337,*)
	write(1337,*)'Grid', netcdf_topo_file

	! we are done with writing the file
	close(1337)
	print*,"Parameters saved at: ", trim(adjustl(path)) // 'spam.txt'
    end subroutine save_parameters

    subroutine check(status)
      integer, intent ( in) :: status

      if(status /= nf90_noerr) then
         print *, trim(nf90_strerror(status))
         stop "Stopped"
      end if
    end subroutine check
    
end module bessi_io
