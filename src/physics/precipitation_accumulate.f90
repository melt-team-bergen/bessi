module precipitation_accumulate
  !precipitation_accumulate
  ! Routine for calculating the amount of new snow

  use bessi_defs
  use regridding

  implicit none

  private
  public :: accumulate
  
contains
    subroutine check4melt_cond(T_air, lwrd_l, diurnal_mp)
        real(kind=8), intent(in) :: T_air
        logical, intent(out)     :: diurnal_mp
!         logical, intent(in)      :: output_flag
        real(kind=8), parameter  :: T_min     = 268.   ! min air temperature for which we expect melt
        real(kind=8), parameter  :: emiss_max = .9     ! neglect diurnal cycle if too cloudy
        real(kind=8)  :: emiss_l
        real(kind=8)  :: lwrd_l
        ! UKK this is essentially one line and could be included in the calling
        ! routine, also it is not really essential since we know Tsnow => check4melt might be
        ! skipped
        emiss_l    = lwrd_l/(sigma*(T_air)**4)
        diurnal_mp = ((T_air > T_min) .AND. (emiss_l < emiss_max)) ! maybe obsolete now!
!         if ( output_flag ) then
!           print *, 'mc', emiss_l, T_air, diurnal_mp
!         end if
    end subroutine check4melt_cond

    subroutine diurnal_meltperiod(lati, decli, T_air, lwd, swd, albedo_dep, albedo_indep)
        real(kind=8), intent(in)  :: lati
        real(kind=8), intent(in)  :: decli
        real(kind=8), intent(in)  :: lwd
        real(kind=8), intent(in)  :: swd
        real(kind=8), intent(in)  :: T_air
        real(kind=8), intent(out) :: albedo_indep   ! albedo_indepent terms 
        real(kind=8), intent(out) :: albedo_dep   ! swd_mp term which will be multiplied with 1-A, note: swd_mp = q*swd > swd
!         logical,      intent(in)  :: output_flag
        real(16),      parameter  :: PI_8 = 4 * atan (1.0_8)    !UKK TODO move to bessi_defs
        real(kind=8),  parameter  :: angle = 15.0               !UKK TODO move to bessi_defs Could be IMPROVED
        real(kind=8)              :: q_mp
        real(kind=8)              :: dt_mp
        real(kind=8)              :: sinphisind
        real(kind=8)              :: cosphicosd
        real(kind=8)              :: ha0
        real(kind=8)              :: sol_flxfac0      ! full day insolation
        real(kind=8)              :: sol_flxfacMP     ! insolation during melt period
        real(kind=8)              :: ha_elev, Ksw_os, Ksw_ns, Ksw_ice, air_temp_mp

        sinphisind     = sin(PI_8/180.*lati)*sin(PI_8/180.*decli)
        cosphicosd     = cos(PI_8/180.*lati)*cos(PI_8/180.*decli)
        ! hourangle of sun rise (hour angle is just convertin 24 hour cycle to 2*pi cycle
        ! with zero being at solar noon, ha(sun rise) = -ha(sun set)) 
        ha0            = acos(max(-1.,min(1.,-sinphisind/cosphicosd)))
        sol_flxfac0    = (sinphisind*ha0+cosphicosd*sin(ha0))/PI_8
        ! hour angle of sun rising above critical elevation angle:     
        ha_elev        = acos(min(1.,max(-1.,((sin(angle*PI_8/180.)-sinphisind)/cosphicosd))))
        ! calculate time dt_mp, the sun spends above this critical elevation angle
        ! and calculate q_mp, which relates mean meltperiod insolation as a multiple of mean daily insolation
        ! (q is usually > 0)
        ! q=SWD_mp/dt_mp / (SWD/dt_FullDay)
        sol_flxfacMP   = (sinphisind*ha_elev+cosphicosd*sin(ha_elev))/PI_8
        dt_mp          = ha_elev*dt_firn/PI_8   ! duration of the melt period in seconds  
        q_mp           = max(0.,min(1.,(sol_flxfacMP/sol_flxfac0))) ! ratio of melt_period relative to dt_firn 

        call calc_PDD(T_air, air_temp_mp)
        ! the heat uptake during melt period is just a rough estimate. The full day calculation will however 
        ! be accurate in terms of energy conservation
        ! underestimates net heat uptake as we are assuming that surface temperature is at melt point here
        albedo_indep = ((air_temp_mp)*D_sf + eps_snow*lwd-sigma*eps_snow*kelvin**4.)*dt_mp
        albedo_dep   = q_mp*swd*dt_firn
        !MPMP_ice=(albedo_indep + (1-A)*albedo_dep)/(L_lh+c_i*(kelvin-T_air))/dt_firn

        ! alternatively, if we want to neglect that snow is not a perfectly black absorber...
        ! MPMP_os=(((air_temp_mp)*D_sf + lwrd_l-sigma*eps_snow*kelvin**4.) + q_mp*Ksw_os)*dt_mp

    end subroutine diurnal_meltperiod


    subroutine calc_PDD(T_air,  air_temp_mp)
      !UKK dt_mp is not used so far, but might be handy in case we want to
      !assume  a typical daily amplitude, or make other asumptions => maybe
      !rename routine?
       real(kind=8), intent(in) :: T_air
       real(kind=8), intent(out):: air_temp_mp
       real                     :: T
       real(kind=8) ,parameter  :: stddevT = 2.0    !TODO UKK move to Bessidefs
       real(16),     parameter  :: PI_8 = 4 * atan(1.0_8)
       !stddev./sqrt(2*pi).*exp(-(T.^2)/(2*stddev.^2))+T/2.*erfc(-T./(sqrt(2)*stddev));
       T=T_air-kelvin
       air_temp_mp = stddevT/sqrt(2*PI_8)*exp(-T**2/(2*(stddevT)**2))+T/2*erfc(-T/(sqrt(2.)*stddevT))

    end subroutine calc_PDD


    subroutine accumulate(ix, iy, time, rain, snow, nday_snowfall, H_lh, K_lh, T_air, precip, decl, mp_runoff, ice_melt, snow_melt)
                            
    ! Determines the amount [kg/m2] of snow that falls on a gridcell
    ! and adjusts the inner energy of the gridcell i.e. T. in case of rain the
    ! fraction that can freeze shall be determined and inner energy will be
    ! adjusted too.
    ! In case of snowfall the albedo is increased.

    implicit none
    integer,      intent(in)    :: ix
    integer,      intent(in)    :: iy 
    integer,      intent(in)    :: time
    real(kind=8), intent(in)    :: T_air      !??? UKK why is this the 3-D field and not the local value
    real(kind=8), intent(in)    :: precip      !??? UKK same here
    real(kind=8), intent(in)    :: decl
    integer,      intent(inout) :: nday_snowfall
    real(kind=8), intent(inout) :: rain
    real(kind=8), intent(inout) :: snow
    real(kind=8), intent(inout) :: H_lh
    real(kind=8), intent(inout) :: K_lh
    real(kind=8), intent(inout) :: ice_melt
    real(kind=8), intent(inout) :: mp_runoff
    real(kind=8), intent(inout) :: snow_melt                         !UKK new
    integer                     :: regrid                           !UKK new
    real(kind=8)                :: ns_melt                          !UKK new  
    real(kind=8)                :: os_melt                          !UKK new
    real(kind=8)                :: massum
    real(kind=8)                :: liquid_fraction
    real(kind=8)                :: albedo_dep                       !UKK new
    real(kind=8)                :: albedo_indep                     !UKK new
    real(kind=8)                :: mp_rest                          !UKK new
    real(kind=8)                :: mp_uptake                        !UKK new
    real(kind=8)                :: mp_liquid                        !UKK new
!     logical                     :: oflag
!     logical                     :: dEBM_flag
    integer                     :: count_layers! ABo: They don't seem to be used for anything
    !real(kind=8), 	intent(in) 	:: solid_precip_temp
    !real(kind=8), 	intent(in) 	:: liquid_precip_temp
!     oflag     = .false.
!     dEBM_flag = .true.
    H_lh      = 0.
    K_lh      = 0.
    snow      = 0.
    rain      = 0.
    massum    = 0.
    os_melt   = 0.
    ice_melt  = 0.
    snow_melt = 0.
    ns_melt   = 0d0
    mp_uptake = 0.! heat taken up and used during melt period- this needs to be subtracted from full day budget later on
    mp_liquid = 0.
    mp_rest   = 1.
    mp_runoff = 0.
  

    select case(precip_transition_type)
    ! UKK this is not really new, just newly organized/ decluttered
      case ('constant_threshold')
      if ((T_air - kelvin) .le. snow_fall_temperature) then
        ! It snows
        snow = precip*rho_w
        rain = 0d0
      else if(snowmass(ix,iy,1) .gt. 0.) then
        ! It rains
        snow = 0d0
        rain = precip*rho_w! kg/m2/s
      end if
      case ('linear')
      if (T_air - kelvin .le. solid_precip_temp) then
        ! 100% snow below threshold
        snow = precip*rho_w
        rain = 0d0
      else if (T_air - kelvin .ge. liquid_precip_temp) then
        ! 100% rain above threshold
        snow = 0d0
        rain = precip*rho_w! kg/m2/s
      else
        ! Mixed precipitation, linear transition between snow and rain
        liquid_fraction = T_air/(liquid_precip_temp - solid_precip_temp) - (solid_precip_temp/(liquid_precip_temp - solid_precip_temp))
        snow            = precip * rho_w * (1d0-liquid_fraction)
        rain            = precip * rho_w * liquid_fraction
      end if
     end select
     !******************************************************************************************************************************
     !****************************************************************************************************************************** 
     ! UKK all of the following is new
     ! Here we estimate melt for intense daytime insolation (=diurnal meltperiod ie. solar elevation angle > 15)
     ! We consider this to happen adiabatic and the underlying snowpack will not cool
     ! meltwater estimated here is refered to lwmass or runoff and the consumed melt energy will be subtracted from the daily
     ! budget lateron. So Qsurf will be negative in many cases
     ! and if there is a thick snowlayer the surplus of melt will refreeze again and the total mass balance will not change 
     ! ... however, if snow is thin/wet/absent this will mean an intensification of melt
     ! note this  estimate does not include latent heat, sublimation etc. but it could be included
     ! also it is possible to speed things up (at the moment the diurnal calculation slows BESSI down by a factor of ~2...   
     !*****************************************************************************************************************************
     !******************************************************************************************************************************
     if (dEBM_flag) then
       if (T_air .gt. 265d0) then! this is to switch dEBM off, if no melt is expected,
         call diurnal_meltperiod(latitudes(ix,iy), decl, T_air, longwrd(ix,iy,time), shortwrd(ix,iy,time), albedo_dep, albedo_indep)
         ns_melt = (albedo_indep + (1-albedo_snow_new)*albedo_dep)/(L_lh+c_i*(kelvin-min(kelvin,T_air)))! units: kg/m2
       else
         ns_melt = 0d0
       end if
!      else
!        ns_melt = 0d0
!      end if! if (dEBM_flag) then

       if (ns_melt .gt. 1d0) then! Less than 1mm/day (1 kg/m2) will be neglected.
         if (ns_melt/seconds_per_day .lt. snow) then! if there is more snowfall than melting, melt only freshly fallen snow:
           snow         = snow-ns_melt/seconds_per_day! unit of snow: kg/m2/s (mm/s), unit of ns_melt mm/day
           mp_liquid    = mp_liquid + ns_melt
           mp_uptake    = ns_melt*(L_lh+c_i*max(0d0,kelvin-T_air))/seconds_per_day
         elseif (ns_melt/seconds_per_day .gt. snow) then! not enough snowfall, turn to the table silver.. melt the snowpack
           mp_rest      = (1d0-seconds_per_day*snow/ns_melt)! proportion of the melt period that remains after new snow was melted
           ns_melt      = snow*seconds_per_day! all snowfall melts
           mp_liquid    = mp_liquid + ns_melt
           mp_uptake    = ns_melt*(L_lh+c_i*max(0d0, kelvin-T_air))/seconds_per_day! energy consumed for heating and melting new snow
           snow         = 0d0
           count_layers = 0
           do while (mp_rest > 0d0)
             count_layers = count_layers + 1
             ! snow pack melts adiabatically (snow temperature does not change yet)
             ! ABo: This code assumes that at most the topmost layer can experience melting. This is reasonable in the current setup, but not necessarily for thinner layers such as the ones that Christophe implemented. The solution could be to write a generic layer removal code that can be used here and elsewhere.
             if (snowmass(ix,iy,1) .gt. 0d0) then
               os_melt = mp_rest*(albedo_indep + (1-albedo_dynamic(ix,iy))*albedo_dep)/(L_lh+c_i*(kelvin-snow_temp(ix,iy,1))) 
               if (os_melt < snowmass(ix,iy,1)) then
                 snowmass(ix,iy,1)    = snowmass(ix,iy,1)-os_melt
                 mp_rest              = 0d0
                 mp_uptake            = mp_uptake + os_melt*(L_lh+c_i*(kelvin-snow_temp(ix,iy,1)))/seconds_per_day
                 mp_liquid            = mp_liquid + os_melt
               else! The entire grid cell melts
                 os_melt              = snowmass(ix,iy,1)
                 mp_uptake            = mp_uptake + snowmass(ix,iy,1)/dt_firn*(L_lh+c_i*(kelvin-snow_temp(ix,iy,1)))
                 mp_rest              = mp_rest*(1-snowmass(ix,iy,1)/os_melt) ! proportion of the melt period that remains after last snow layer
                 mp_liquid            = mp_liquid + os_melt
                 snow_temp(ix, iy, 1) = snow_temp(ix, iy, 2)
                 rho_snow(ix, iy, 1)  = rho_snow(ix, iy, 2)
                 snowmass(ix,iy,1)    = 0.

                 if (snowmass(ix,iy,2) == 0d0) then
                   mp_runoff          = (mp_liquid+snowmass(ix,iy,1)+lwmass(ix, iy, 1))/seconds_per_day
                   mp_liquid          = 0.
                 end if
                 call regrid_point(ix,iy, regrid)
               end if
               snow_melt = snow_melt+os_melt
             else! there is only ice left to melt, i.e. reset grid cell
               ice_melt               = mp_rest*(albedo_indep + (1-albedo_ice)*albedo_dep)/(L_lh+c_i*max(0d0,kelvin-T_air))
               mp_runoff              = mp_runoff+ice_melt/seconds_per_day
               mp_rest                = 0d0
               mp_uptake              = mp_uptake + ice_melt*(L_lh+c_i*max(0.,kelvin-T_air))/seconds_per_day
               snowmass(ix, iy, :)    = 0d0
               lwmass(ix, iy, :)      = 0d0
               !Temperature to 273K
               snow_temp(ix, iy, :)   = 0d0
               rho_snow(ix, iy, :)    = rho_s
               albedo_dynamic(ix, iy) = albedo_ice
               mp_liquid              = 0d0
             end if
           end do
         else
           write(*,*) 'dBESSI, something is not right'
         end if! if (ns_melt/dt_firn .lt. snow) then
         snow_melt = snow_melt+ns_melt
       end if
     else
       ns_melt   = 0d0
       mp_uptake = 0d0
!      end if! if (ns_melt .gt. 1.) then
     end if! if (dEBM_flag) then

     H_lh = snow*c_i
     !UKK now make sure that the dayly mean energy uptake remains the same,... Maybe K_lh is not the best place to put the mp_uptake?
     ! Also do the usual things to update K_lh, rho, albedo ...
     K_lh = snow*c_i* T_air + rain*c_w*(T_air-kelvin)-mp_uptake
     massum = snowmass(ix, iy, 1) + snow*dt_firn
     lwmass(ix, iy, 1) = lwmass(ix, iy, 1) + (mp_liquid+rain*dt_firn)
     if(snowmass(ix, iy, 1) == 0d0) then
     ! Temperature for first snow is the air temperature.
       snow_temp(ix, iy, 1) = T_air
     else
       ! Important to catch division by zero in rare cases, e.g., at the first time step when no snowmass exists yet and it also doesn't snow in Jan 1.
       if (massum .ne. 0d0) rho_snow(ix, iy, 1) = (snowmass(ix, iy, 1)*rho_snow(ix, iy, 1) + snow*dt_firn*rho_s) / massum
     end if
     snowmass(ix, iy, 1) = massum
     ! keep track of when the last snowfall occurred, important for snow aging and albedo.
     if (snow .gt. 0d0) nday_snowfall = time
     !ABo: This is the same as without dEBM but needs checking (Tobias's code):
     if(albedo_module > 0) then
       ! Based on oerlemans depth scale inverse, but not entirely similar
       ! Should albedo be the same as with fresh snow, or can we make it wet snow?
       albedo_dynamic(ix, iy) = min(albedo_snow_new, albedo_dynamic(ix, iy) + (albedo_snow_new - albedo_snow_wet)* (1 - exp(-snow*dt_firn/3.)))
       ! Alternative also strange  UKK???: sounds more intuitive to me, as background snow might be dry and quite new...
       ! albedo_dynamic(ix, iy, 1) = min(albedo_snow_new, albedo_dynamic(ix, iy, 1) + &
       !								(albedo_snow_new - albedo_dynamic(ix, iy, 1))* &
       !                            	(1 - exp(-snow*dt_firn))
     end if
  end subroutine accumulate

end module precipitation_accumulate
