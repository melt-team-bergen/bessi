module melting
  ! Melting routines for ice and snow, and calculation routines
  ! for the surface mass balance
  
  use bessi_defs
  use regridding
  
  implicit none
  
  private
  public :: melt_snow
  public :: melt_ice
  public :: smb_point
contains
  subroutine melt_snow(ix, iy, time, T_air, K_sw, K_lh, H_lh, Q_heat, melted_snow, runoff_water, ice_melt, &
       vaporflux, D_lf, dummy_melt_ice, dummy_regrid, used_q, l_heat)
    implicit none
    
    integer, 		intent(in) 		:: ix
    integer, 		intent(in) 		:: iy
    integer, 		intent(in) 		:: time
    real(kind=8), 	intent(in) 		:: H_lh
    real(kind=8), 	intent(in) 		:: T_air
    real(kind=8), 	intent(in) 		:: K_sw
    real(kind=8), 	intent(in) 		:: K_lh
    real(kind=8), 	intent(in) 		:: D_lf
    real(kind=8), 	intent(inout) 	:: Q_heat
    real(kind=8), 	intent(inout) 	:: melted_snow
    real(kind=8), 	intent(inout) 	:: runoff_water
    real(kind=8), 	intent(inout) 	:: ice_melt
    real(kind=8), 	intent(inout) 	:: used_q
    real(kind=8), 	intent(inout) 	:: l_heat
    real(kind=8), 	intent(inout) 	:: vaporflux
    real(kind=8), 	intent(inout) 	:: dummy_melt_ice
    integer, 		intent(inout) 	:: dummy_regrid
    
    ! Local variables
    real(kind=8) :: QQ
    real(kind=8) :: Qp_lw!    Longwave/thermal radiation     
    real(kind=8) :: Qp_sh!    Turbulent sensible heat flux
    real(kind=8) :: Qp_lh!    
    real(kind=8) :: Q_v
    real(kind=8) :: dT
    real(kind=8) :: dm
    real(kind=8) :: dewpoint
    real(kind=8) :: lwrd_l

    
    if (.not. dEBM_flag) then
      ! these must not be reset if the "high noon" melt parameterization is used, check smb_emb.f90
      melted_snow  = 0.
      runoff_water = 0.
    end if
    ice_melt     = 0.
    QQ           = 0.
    l_heat       = 0.
    used_q       = 0.

    
    ! Melt the snow depending on energy upptaken by the snowcover if it
    ! reaches 273K
    if(longwave_from_air_temp) then
       ! Longwave radiation parameterized from air temperature if not given as input
       lwrd_l = sigma*eps_air*(T_air)**4.
    else
       ! Longwave radiation from input
       lwrd_l = longwrd(ix, iy, time)
    end if
    
    Qp_lw =lwrd_l - sigma*eps_snow*(kelvin)**4. ! Eq.10 in [Zolles and Born, 2021] I think it's wrong, it should be snow temp not 0C
    Qp_sh = D_sf*(T_air-kelvin)                 ! Eq.11 in [Zolles and Born, 2021] I think it's wrong, it should be snow temp not 0C
    Qp_lh = K_lh - H_lh*snow_temp(ix, iy, 1)    ! Eq.14+15 in [Zolles and Born, 2021]
    Q_v   = vaporflux
    
    QQ    = max((K_sw + Qp_lw + Qp_sh + Qp_lh + Q_v)*dt_firn - Q_heat, real(0.))
    used_q = QQ
    
    Q_heat = 0.
    do while (QQ .gt. 0.)
        ! Maximum possible heating
        dT = QQ/c_i/snowmass(ix, iy, 1)
        
        if (kelvin - snow_temp(ix, iy, 1) .gt. dT) then
           ! QQ too small to melt something

           snow_temp(ix, iy, 1) = snow_temp(ix, iy, 1) + dT
           QQ = 0.
        else if(snowmass(ix, iy, 1) .gt. c_i*snowmass(ix, iy, 1)*(dT - (kelvin - snow_temp(ix, iy, 1)))/L_lh) then 
           ! Snow is heated to 0C and partially melted
           dm = QQ/L_lh - c_i*snowmass(ix, iy, 1)*(kelvin - snow_temp(ix, iy, 1))/L_lh
           l_heat = l_heat + dm*L_lh
           snow_temp(ix, iy, 1) = kelvin
           snowmass(ix, iy, 1) = snowmass(ix, iy, 1) - dm
           lwmass(ix, iy, 1)  = lwmass(ix, iy, 1) + dm
           melted_snow = melted_snow + dm
           QQ = 0.
        else if(snowmass(ix, iy, 2) .gt. 0.) then
           ! The entire grid cell melts 
           QQ = QQ - c_i*snowmass(ix, iy, 1)*(kelvin - snow_temp(ix, iy, 1)) - snowmass(ix, iy, 1)*L_lh
           lwmass(ix, iy, 1)  = lwmass(ix, iy, 1) + snowmass(ix, iy, 1)
           melted_snow = melted_snow + snowmass(ix, iy, 1)
           l_heat = l_heat + snowmass(ix, iy, 1)*L_lh
           snowmass(ix, iy, 1) = 0.
           snow_temp(ix, iy, 1) = snow_temp(ix, iy, 2)
           rho_snow(ix, iy, 1) = rho_snow(ix, iy, 2)
           call regrid_point(ix,iy, dummy_regrid)
        else
           ! Entire grid cell melts and no second layer to melt. i.e. reset grid cell
           QQ = QQ - c_i*snowmass(ix, iy, 1)*(kelvin - snow_temp(ix, iy, 1)) - snowmass(ix, iy, 1)*L_lh
           lwmass(ix, iy, 1) = lwmass(ix, iy, 1) + snowmass(ix, iy, 1)
           melted_snow = melted_snow + snowmass(ix, iy, 1)
           
           ! TODO: if balance is not ok, think about this line again
           runoff_water = runoff_water + lwmass(ix, iy, 1)    
           l_heat = l_heat + snowmass(ix, iy, 1)*L_lh
           snowmass(ix, iy, 1) = 0.
           snow_temp(ix, iy, 1) = snow_temp(ix, iy, 2)
           rho_snow(ix, iy, 1) = rho_snow(ix, iy, 2)
           
           ! The rest of the energy is used to melt ice.
           ! This underestimates the melt since the ice is darker than the snow
           dummy_melt_ice = max(-QQ/L_lh,0.)
           ice_melt       = ice_melt + min(-QQ/L_lh/rho_i, 0.)
           used_q         = used_q - QQ

           snowmass(ix, iy, :) = 0.
           lwmass(ix, iy, :) = 0.
           ! Temperature to 273K
           snow_temp(ix, iy, :) = 0. 
           rho_snow(ix, iy, :) = rho_s
           QQ = 0.
        end if
    end do
  end subroutine melt_snow

   
! ################################################  
   subroutine melt_ice(ix, iy, time, ice_melt, T_air, precipitation, vaporflux, D_lf, dummy_melt_ice, dummy_rain_ice, p_air)
     ! Melt ice in grid point without snow. It is assumed that blank ice has a surface temperature 0C.
     ! Water can melt the ice but doesn't freeze, rain and meltwater run off

     implicit none
     integer,      intent(in)    :: ix
     integer,      intent(in)    :: iy
     integer,      intent(in)    :: time
     real(kind=8), intent(in)    :: T_air
     real(kind=8), intent(in)    :: precipitation
     real(kind=8), intent(in   ) :: p_air
     real(kind=8), intent(inout) :: ice_melt
     real(kind=8), intent(inout) :: D_lf
     real(kind=8), intent(inout) :: dummy_melt_ice
     real(kind=8), intent(inout) :: dummy_rain_ice
     real(kind=8), intent(out)   :: vaporflux

     ! Local variables
     real(kind=8) :: ea
     real(kind=8) :: es
     real(kind=8) :: lwrd_l
     real(kind=8) :: dQ_lh
     real(kind=8) :: dQ_sh
     real(kind=8) :: dQ_sw
     real(kind=8) :: dQ_lw
     real(kind=8) :: dQ_v
     real(kind=8) :: dQ_tot

     dQ_lh  = 0
     dQ_sh  = 0
     dQ_sw  = 0
     dQ_lw  = 0
     dQ_v   = 0
     dQ_tot = 0

     ea = 610.8*exp(17.27*dewpoint(ix, iy, time)/(dewpoint(ix, iy, time) + 237.3))
     es = 611.2*exp(22.46*(kelvin - 273)/(272.62 + (kelvin - 273))) 

     vaporflux = D_lf/p_air*(ea - es)

     if (longwave_from_air_temp) then
        lwrd_l = sigma*eps_air*(T_air)**4.
     else 
        lwrd_l = longwrd(ix, iy, time)
     end if

     dQ_sw  = shortwrd(ix, iy, time)*(1. - albedo_ice)
     dQ_lw  = lwrd_l - sigma*eps_snow*(kelvin)**4.
     dQ_sh  = D_sf*(T_air - kelvin)
     dQ_lh  = (T_air - kelvin)*precipitation*c_w*rho_w
     dQ_v   = vaporflux
     dQ_tot = dQ_sw + dQ_lw + dQ_sh + dQ_lh + dQ_v
     ! Use ice density of ice model to get the right height
     ice_melt = ice_melt - max(dQ_tot,real(0))/L_lh*dt_firn/rho_i + dQ_v/(L_v + L_lh)* dt_firn/rho_i

     dummy_melt_ice = max(dQ_tot,real(0))/L_lh*dt_firn
     dummy_rain_ice = precipitation*rho_w

     albedo_dynamic(ix, iy) = albedo_ice
   end subroutine melt_ice

   
! ################################################
! ABo, 23-Aug-2024: This is just stupid. Combine smb_point and continuous_snow_removal.
  subroutine smb_point(ix, iy, dummy_runoff, dmass, dummy_ice2ice, dummy_energy, smb_ice)
     ! Calculate smb for ice model for a given grid point

     implicit none
     integer,      intent(in)    :: ix
     integer,      intent(in)    :: iy
     real(kind=8), intent(in)    :: dmass
     real(kind=8), intent(inout) :: dummy_runoff
     real(kind=8), intent(inout) :: dummy_ice2ice
     real(kind=8), intent(inout) :: dummy_energy
     real(kind=8), intent(inout) :: smb_ice(nx, ny)! ABo: Doesn't have to be 2D array in this subroutine.

     smb_ice(ix, iy) = max(dmass, 0.)/rho_i

     call continuous_snow_removal(ix, iy, dummy_runoff, dmass, dummy_ice2ice, dummy_energy)
   end subroutine smb_point


! ################################################
! ABo, 9-Jul-2024: Remove unnecessary local variables (d_m, nn)
   subroutine continuous_snow_removal(ix, iy, runoff, dmass, ice2ice, energy_ice2ice)
     ! Remove snow at the lower end of the snowmodel and convert it a smb for the ice model
     
     implicit none
     integer,      intent(in)    :: ix
     integer,      intent(in)    :: iy
     real(kind=8), intent(in)    :: dmass
     real(kind=8), intent(inout) :: runoff
     real(kind=8), intent(inout) :: ice2ice
     real(kind=8), intent(inout) :: energy_ice2ice

     ! local variables
     integer      :: nn
     real(kind=8) :: d_m
     real(kind=8) :: d_lw
     
     nn             = n_snowlayer
     d_m            = dmass
     ice2ice        = 0.
     runoff         = 0.
     energy_ice2ice = 0.

     do while (d_m .gt. 0.)
       if (d_m .gt. snowmass(ix, iy, nn) ) then
         ! If there is not enough mass in the lowest grid box, take as much as possible:
         d_m            = d_m - snowmass(ix, iy, nn)
         ice2ice        = ice2ice + snowmass(ix, iy, nn)
         runoff         = runoff + lwmass(ix, iy, nn)
         energy_ice2ice = energy_ice2ice + snowmass(ix, iy, nn)*c_i*snow_temp(ix, iy, nn)

         rho_snow(ix, iy, nn)  = rho_s
         snow_temp(ix, iy, nn) = 0.
         lwmass(ix, iy, nn)    = 0.
         snowmass(ix, iy, nn)  = 0.
        else
         snowmass(ix, iy, nn)  = snowmass(ix, iy, nn) - d_m
         ice2ice               = ice2ice + d_m
         energy_ice2ice        = energy_ice2ice + d_m*c_i*snow_temp(ix, iy, nn)
         d_lw                  = d_m * lwmass(ix, iy, nn) / snowmass(ix, iy, nn)
         runoff                = runoff + d_lw
         lwmass(ix, iy, nn)    = lwmass(ix, iy, nn) - d_lw
         d_m                   = 0d0
        end if
        
        nn = nn - 1
     end do
   end subroutine continuous_snow_removal

end module melting
 
