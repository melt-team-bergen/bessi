module regridding
	! Routines for regridding, fusing and splitting of boxes

	use bessi_defs

	implicit none

	private
	public :: regrid
	public :: regrid_point
contains
	subroutine regrid(ix, iy, dummy_regrid)
		! Fuse and split boxes if necessary

		implicit none

		integer, intent(in) :: ix
	    integer, intent(in) :: iy
	    integer, intent(inout) :: dummy_regrid

	    if ((snowmass(ix, iy, 1) .gt. upper_massbound) .or. (snowmass(ix, iy, 1) .lt. lower_massbound)) then
          	! Fuse and split boxes
!             if ((ix .eq. 190) .and. (iy .eq. 90)) then
!               write(*,*) '#regrid 1#', snowmass(ix,iy,1)
!             end if

            call regrid_point(ix, iy, dummy_regrid)

!             if ((ix .eq. 190) .and. (iy .eq. 90)) then
!               write(*,*) '#regrid 2#', snowmass(ix,iy,1)
!             end if

!             do while((snowmass(ix, iy, 1) .gt. upper_massbound))
!                 call regrid_point(ix, iy, dummy_regrid)
!             end do

!             if ((ix .eq. 190) .and. (iy .eq. 90)) then
!               write(*,*) '#regrid 3#', snowmass(ix,iy,1)
!             end if

          end if
	end subroutine regrid

	subroutine regrid_point(ix, iy, dummy_regrid)
		! Adjust boxsizes and move boxes arround, 
		! depending on the soll_mass.

	    implicit none
	    
	    integer, intent(in) :: ix
	    integer, intent(in) :: iy
	    integer, intent(inout) :: dummy_regrid

	    real(kind=8) :: masssum
	    integer :: ii
	    integer :: kk

	    dummy_regrid = dummy_regrid + 1
	    
	    if (snowmass(ix, iy, 1) .gt. upper_massbound) then
	    	! Split top box (accumulating)
	    	! Fuse the two lowest boxes
	        masssum = snowmass(ix, iy, n_snowlayer) + snowmass(ix, iy, n_snowlayer-1)

	        if (masssum == 0) then
	            rho_snow(ix, iy, n_snowlayer) = rho_s
	            snow_temp(ix, iy, n_snowlayer) = 0.
	        else if(snowmass(ix, iy, n_snowlayer) == 0) then
	            rho_snow(ix, iy, n_snowlayer) = rho_snow(ix, iy, n_snowlayer - 1)
	            snow_temp(ix, iy, n_snowlayer) = snow_temp(ix, iy, n_snowlayer - 1)
	        else
	            rho_snow(ix, iy, n_snowlayer) = masssum / (snowmass(ix, iy, n_snowlayer)/rho_snow(ix, iy, n_snowlayer) + &
	            						snowmass(ix, iy, n_snowlayer - 1)/rho_snow(ix, iy, n_snowlayer - 1))

	            snow_temp(ix, iy, n_snowlayer) = (snowmass(ix, iy, n_snowlayer)*snow_temp(ix, iy, n_snowlayer) + &
	            							snowmass(ix, iy, n_snowlayer - 1)*snow_temp(ix, iy, n_snowlayer - 1))/masssum
	        end if

	        snowmass(ix, iy, n_snowlayer) = masssum
	        lwmass(ix, iy, n_snowlayer) = lwmass(ix, iy, n_snowlayer) + lwmass(ix, iy, n_snowlayer - 1)

	        ! Push all layers down, except the toplayer
	        kk = n_snowlayer - 1;
	        do while (kk .gt. 2)
	            snowmass(ix, iy, kk) = snowmass(ix, iy, kk - 1)
	            rho_snow(ix, iy, kk) = rho_snow(ix, iy, kk - 1)
	            snow_temp(ix, iy, kk) = snow_temp(ix, iy, kk - 1)
	            lwmass(ix, iy, kk) = lwmass(ix, iy, kk - 1)

	            kk = kk - 1
	        end do

	        ! Split top box
	        ! Move proper amount of content down to second box
	        snowmass(ix, iy, 2) = soll_mass
	        rho_snow(ix, iy, 2) = rho_snow(ix, iy, 1)
	        snow_temp(ix, iy, 2) = snow_temp(ix, iy, 1)
	        lwmass(ix, iy, 2) = lwmass(ix, iy, 1)*soll_mass/snowmass(ix, iy, 1)

	        ! Adjust content in topbox (except density and temperature)
	        snowmass(ix, iy, 1) = snowmass(ix, iy, 1) - soll_mass
	        lwmass(ix, iy, 1) = lwmass(ix, iy, 1) - lwmass(ix, iy, 2)
	    else if ((snowmass(ix, iy, 1) .lt. lower_massbound) .AND. (snowmass(ix, iy, 2) .gt. 0.)) then
	    	! Fusing top boxes (melting)
	        ! Splitting lowest box in a meaningfull way and fuse both uppermost boxes if there are two

	        masssum = snowmass(ix, iy, 1) + snowmass(ix, iy, 2)

	        rho_snow(ix, iy, 1) = masssum / (snowmass(ix, iy, 1)/rho_snow(ix, iy, 1) + snowmass(ix, iy, 2)/rho_snow(ix, iy, 2))
	        snow_temp(ix, iy, 1) = (snowmass(ix, iy, 1)*snow_temp(ix, iy, 1) + snowmass(ix, iy, 2)*snow_temp(ix, iy, 2))/masssum
	        snowmass(ix, iy, 1) = masssum
	        lwmass(ix, iy, 1) = lwmass(ix, iy, 1) + lwmass(ix, iy, 2)

	        ! Drag all layers up, except the toplayer
	        do ii = 2, n_snowlayer-1, 1
	            snowmass(ix, iy, ii) = snowmass(ix, iy, ii + 1)
	            snow_temp(ix, iy, ii) = snow_temp(ix, iy, ii + 1)
	            lwmass(ix, iy, ii) = lwmass(ix, iy, ii + 1)
	            rho_snow(ix, iy, ii) = rho_snow(ix, iy, ii + 1)
	        end do

	        ! Split second lowest box in 2. The upper should have the
	        ! soll_mass, the lowest not less than the soll_mass
	        if(snowmass(ix, iy, n_snowlayer - 1) .ge. 2*soll_mass) then
	            ! Move down to lowest box (density and temperature do not change)
	            snowmass(ix, iy, n_snowlayer) = snowmass(ix, iy, n_snowlayer - 1) - soll_mass
	            lwmass(ix, iy, n_snowlayer)= (1. - soll_mass/snowmass(ix, iy, n_snowlayer - 1))*lwmass(ix, iy, n_snowlayer - 1)

	            ! Adjust content in second lowest box (except density and temperature)
	            snowmass(ix, iy, n_snowlayer - 1) = soll_mass
	            lwmass(ix, iy, n_snowlayer - 1)= lwmass(ix, iy, n_snowlayer - 1) - lwmass(ix, iy, n_snowlayer)

	        else
	            ! Reset lowest box
	            rho_snow(ix, iy, n_snowlayer) = rho_s
	            snowmass(ix, iy, n_snowlayer) = 0.
	            snow_temp(ix, iy, n_snowlayer) = 0.
	            lwmass(ix, iy, n_snowlayer) = 0.
	        end if
	    end if
	end subroutine regrid_point
end module regridding
