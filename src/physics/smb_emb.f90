module smb_emb
    ! This module calculates the surface mass and energy balance in a multilayer firn
    ! The accumulation must be calculated for every year separately due to their descrete accumulation
    ! SMB is cummulative for one year

!   8 888888888o   8 8888888888      d888888o.      d888888o.    8 8888 
!   8 8888    `88. 8 8888          .`8888:' `88.  .`8888:' `88.  8 8888 
!   8 8888     `88 8 8888          8.`8888.   Y8  8.`8888.   Y8  8 8888 
!   8 8888     ,88 8 8888          `8.`8888.      `8.`8888.      8 8888 
!   8 8888.   ,88' 8 888888888888   `8.`8888.      `8.`8888.     8 8888 
!   8 8888888888   8 8888            `8.`8888.      `8.`8888.    8 8888 
!   8 8888    `88. 8 8888             `8.`8888.      `8.`8888.   8 8888 
!   8 8888      88 8 8888         8b   `8.`8888. 8b   `8.`8888.  8 8888 
!   8 8888    ,88' 8 8888         `8b.  ;8.`8888 `8b.  ;8.`8888  8 8888 
!   8 888888888P   8 888888888888  `Y8888P ,88P'  `Y8888P ,88P'  8 8888 

    use bessi_defs
    use bessi_data
    use bessi_io
    use bessi_tools

    use regridding
    use precipitation_accumulate
    use densification
    use radiation
    use energy_flux
    use melting
    use percolation_refreezing
    use conservation

    use omp_lib

    implicit none

    public :: calculate_firn

contains

    subroutine calculate_firn(ndays, climdata, landmask, smb_ice, myear)
        ! Calculating the surface mass balance and energy balance of the firn for every grid point
        ! calculates one full year

        implicit none
        integer,      intent(in)      :: ndays
        type(climinp_data_type)       :: climdata         ! climate data for the current model year
        integer,      intent(in)      :: myear            ! current model year
        integer,      intent(in)      :: landmask(nx, ny) ! ice = 0, water  = 1, ice-free land = 2
        real(kind=8), intent(out)     :: smb_ice(nx, ny)  ! height in ice equivalents for the ice wrapper

        ! for dEBM_flag:
        real(8),      parameter       :: PI_8 = 4 * atan (1.0_8)! TODOUKK Move to BessiDefs 
        integer,      parameter       :: spring_equinox = 79    ! TODOUKK Move to BessiDefs
        integer,      parameter       :: fall_equinox = 266     ! TODOUKK Move to BessiDefs 
        real(8),      parameter       :: obl = 23.44            ! TODOUKK Move to BessiDefs 
        integer                       :: half_orbit             ! UKK
        real(kind=8), dimension(ndays) :: decl          ! UKK declination
        real(kind=8)                  :: mp_runoff      ! UKK relevant things from highnoon
        real(kind=8)                  :: mp_icemelt
        real(kind=8)                  :: mp_snowmelt

!         real(kind=8)                  :: air_temp_ice(nx, ny, ndays)
!         real(kind=8)                  :: precip_total(nx, ny, ndays)
        real(kind=8)                  :: D_lf
        real(kind=8)                  :: Q_heat
        real(kind=8)                  :: dummy
        real(kind=8)                  :: H_lh
        real(kind=8)                  :: K_lh
        real(kind=8)                  :: K_sw
        real(kind=8)                  :: snow
        real(kind=8)                  :: rain
        real(kind=8)                  :: lwc
        real(kind=8)                  :: vaporflux
        real(kind=8)                  :: dummy_melt_ice
        real(kind=8)                  :: dummy_rain_ice
        real(kind=8), dimension(n_snowlayer) :: dz
        real(kind=8), dimension(nx,ny):: albedo_runtime ! UKK is this used anywhere?
        real(kind=8), dimension(nx,ny):: p_air!         Air pressure at the grid points elevation
        real(kind=8), dimension(nx,ny):: t_mean!        Mean Temperature at the level of the ice elevation
        logical,      dimension(nx,ny):: run_cell_this_year
        logical,      dimension(nx,ny):: found_liquid_water
        logical, save,dimension(nx,ny):: made_ice
        logical                       :: force_melting	
        logical                       :: write_daily
        logical                       :: write_monthly
        logical                       :: write_annual

        integer                       :: time
        integer                       :: nday_snowfall
        integer                       :: dummy_regrid

        real(kind=8)                  :: dummy_energy  !J/m2
        real(kind=8)                  :: dummy_heat    !J/m2
        real(kind=8)                  :: dummy_e_qq    !J/m2
        real(kind=8)                  :: dummy_melt
        real(kind=8)                  :: dummy_runoff
        real(kind=8)                  :: dummy_refreeze
        real(kind=8)                  :: dummy_ice2ice

        real(kind=8)                  :: mass0
        real(kind=8)                  :: dmass
        
        ! Loop variables 
        integer                       :: ii, ix, iy, kk
        ! Timing variables
        real(kind=8)                  :: tm, tn
        !ABo: Objects for output files, not yet written to disk but created in case they are needed:
        type(nc_file)                 :: ncfile_annual
        type(nc_file)                 :: ncfile_monthly
        type(nc_file)                 :: ncfile_daily
        type(npz_file)                :: npzfile_annual
        type(npz_file)                :: npzfile_daily

        ! initialize variables
        albedo_runtime = 0.
        mass0          = 0.
        rain           = 0.
        snow           = 0.
        K_lh           = 0.
        H_lh           = 0.
        K_sw           = 0.
        dz             = 0.
        vaporflux      = 0.
        nday_snowfall  = 0
        lwc            = 0
        dmass          = 0
        time           = 0
        found_liquid_water = .false.
!         made_ice       = .false.
        force_melting  = .false.

        dummy_energy   = 0.          
        dummy_heat     = 0.            
        dummy_e_qq     = 0.     
        dummy_melt     = 0.
        dummy_refreeze = 0.
        dummy_regrid   = 0    
        dummy_melt_ice = 0
        dummy_rain_ice = 0
        dummy_runoff   = 0.

        ! ***** UKK calculate Earth's declination as a function of days (aka time)
        if (dEBM_flag) then
          half_orbit = fall_equinox-spring_equinox
          do time = 1, 91, 1
            decl(time) = obl*sin((time - spring_equinox)*PI_8/(ndays-half_orbit))
          end do
          do time = 92, 182, 1
            decl(time) = obl*sin((time - spring_equinox)*PI_8/half_orbit)
          end do
          do time = 183, 273, 1
            decl(time) = obl*sin( PI_8 + (time - fall_equinox)*PI_8/half_orbit)
          end do
          do time = 274, ndays, 1
            decl(time) = obl*sin(-PI_8 + (time - fall_equinox)*PI_8/(ndays-half_orbit))
          end do
        end if

        t_mean = 0d0
        ! compute mean temperature t_mean from climdata%temp over the year (nx, ny, ndays)
        do iy = 1, ny, 1
          do ix = 1, nx, 1
             do kk = 1, ndays, 1
                t_mean(ix,iy) = t_mean(ix,iy) + climdata%temp(ix,iy,kk)
             end do
             t_mean(ix,iy) = t_mean(ix,iy) / ndays
          end do
        end do

        ! Calculate pressure coordinates of elevation
        p_air(:, :) = P_atm*exp(-g*mm_air*elevation(:, :)/t_mean(:, :)/R_uni)
        
        ! Calculate exchange coefficient for turbulent latent heat flux based on ratio and sensible heat flux    
        if (latent_heat_flux_on) then
            if (latent_heat_flux_analog_to_sensible_heat_flux) then
                 D_lf = ratio*D_sf/cp_air*0.622*(L_v + L_lh)
            else
                 D_lf = D_sf/cp_air*0.622*(L_v + L_lh)
            end if
        else
            D_lf = 0
        end if

        ! Intitialize conservation variables
        if (check_conservation) call check_init()

        ! Decide whether to write daily/monthly/annual data this year or not:
        if (daily_data   .and. its_time(myear,daily_data_freq,5e-1)) then
          write_daily   = .true.
        else
          write_daily   = .false.
        end if
        if (monthly_data .and. its_time(myear,monthly_data_freq,5e-1)) then
          write_monthly = .true.
        else
          write_monthly = .false.
        end if
        if (annual_data  .and. its_time(myear,annual_data_freq,5e-1)) then
          write_annual  = .true.
        else
          write_annual  = .false.
        end if
        ! Write daily data continuously for certain time interval (override daily_data and daily_data_freq above):
        if ((myear .ge. daily_data_start) .and. (myear .le. daily_data_end)) then
          write_daily   = .true.
        end if

        ! Initialize output files
        if (write_annual) then
          if (debug > 2) print*,'*** Inititalizing files for annual data, year ', myear
          if (.not. write_only_npz) then
            write (ncfile_annual%filename, '( "ANNUAL_.", I7.7, ".nc" )') myear

            ncfile_annual%NLATS          = ny
            ncfile_annual%NLONS          = nx
            ncfile_annual%N_LAYER        = n_snowlayer
            ncfile_annual%N_TIME         = 1
            ncfile_annual%lat_distance   = int(dy)
            ncfile_annual%long_distance  = int(dx)
            ncfile_annual%z_distance     = 1
            ncfile_annual%save_3D        = annual_3D_data

            call ncfile_annual%init
          end if

          if (write_only_npz) call npzfile_annual%init('ANNUAL',myear,annual_3D_data)
        end if

        if (write_monthly) then
          if (debug > 2) print*,'*** Inititalizing files for monthly data', myear
          write (ncfile_monthly%filename, '( "MONTHLY.", I7.7, ".nc" )') myear
       
          ncfile_monthly%NLATS         = ny
          ncfile_monthly%NLONS         = nx
          ncfile_monthly%N_LAYER       = n_snowlayer
          ncfile_monthly%N_TIME        = 12
          ncfile_monthly%lat_distance  = int(dy)
          ncfile_monthly%long_distance = int(dx)
          ncfile_monthly%z_distance    = 1
          ncfile_monthly%save_3D       = monthly_3D_data

          call ncfile_monthly%init
        end if

        if (write_daily) then
          if (debug > 2) print*,'*** Inititalizing files for daily data', myear
          if (.not. write_only_npz) then
            write (ncfile_daily%filename, '( "DAILY__.", I7.7, ".nc" )') myear

            ncfile_daily%NLATS           = ny
            ncfile_daily%NLONS           = nx
            ncfile_daily%N_LAYER         = n_snowlayer
            ncfile_daily%N_TIME          = ndays
            ncfile_daily%lat_distance    = int(dy)
            ncfile_daily%long_distance   = int(dx)
            ncfile_daily%z_distance      = 1
            ncfile_daily%save_3D         = daily_3D_data

            call ncfile_daily%init
          end if

          if (write_only_npz)       call npzfile_daily%init('DAILY_',myear,daily_3D_data)
        end if

        !ABo, 23-Aug-2024: deactivate fast_calculation and reset smb_ice:
!         fast_calculation = .false.
!         smb_ice = 0d0

!         if ((myear .lt. start_speed_up ) .or. (myear .ge. end_speed_up)) fast_calculation = .false.
! #ifdef OMPRUN
!             tn = omp_get_wtime()
! #else
            call cpu_time(tn)
! #endif
! #ifdef OMPRUN
        !$OMP PARALLEL DO COLLAPSE(2) SCHEDULE(static, 48) NUM_THREADS(48) &
        !$OMP     firstprivate(mass0, time, H_lh, K_lh, snow, rain, dummy_melt, dummy_runoff, dummy_refreeze, &
        !$OMP         dummy_melt_ice, dummy_rain_ice, dummy_regrid, nday_snowfall, dz, lwc, K_sw, dummy_energy, &
        !$OMP         dummy_ice2ice, dummy_heat, vaporflux, D_lf, Q_heat, force_melting, dmass)
! #endif

        run_cell_this_year = decide_cells_to_run(found_liquid_water, made_ice, its_time(myear,skip_smb_years,5e-1))

        do iy = 1, ny, 1
          do ix = 1, nx, 1
!             ! only run the model where there is land:
!             if (landmask(ix,iy) .ne. 1) then
!               if ((fast_calculation(ix, iy) .eqv. .false.) .or. (mod(myear, calc_rate_snow) == 0)) then
            if (.not. run_cell_this_year(ix,iy)) then
!               if (landmask(ix,iy) .eq. 0) write(*,*) "Hey", ix,iy,smb_ice(ix,iy)
              ! Keep last year's values. Most values don't have meaning and are set to zero.
              call npzfile_annual%store_data(ix,iy,1,0d0,0d0,0d0,0d0,0d0,0d0,0d0,0,0d0,smb_ice(ix,iy),0d0,0d0,0d0,run_cell=run_cell_this_year(ix,iy), mask=landmask(ix,iy))

            else! if run_cell_this_year(ix,iy) is .true.

              found_liquid_water(ix,iy) = .false.
              made_ice(ix,iy)           = .false.
              ! smb_ice is only reset for cells that run this year.
              ! Where run_cell_this_year(ix,iy) = .false., the previous year's value is used again.
              smb_ice(ix, iy)           = 0d0
              mass0                     = sum(snowmass(ix,iy,:))

              ! Time loop
              do time = 1, ndays, 1
                ! Check for energy conservation
                if (check_conservation) call check_start_loop(ix, iy, time)

                ! Reset dummy variables
                dummy_melt     = 0.    
                dummy_runoff   = 0.  
                dummy_refreeze = 0.
                dummy_melt_ice = 0.
                dummy_rain_ice = 0.
                dummy_regrid   = 0

                ! Accumulation
                call accumulate(ix, iy, time, rain, snow, nday_snowfall, H_lh, K_lh, climdata%temp(ix,iy,time), climdata%precip(ix,iy,time),decl(time), mp_runoff, mp_icemelt, mp_snowmelt)
                dummy_runoff   = dummy_runoff   + mp_runoff
                dummy_melt_ice = dummy_melt_ice + mp_icemelt
                dummy_melt     = dummy_melt     + mp_snowmelt

                ! Further calculations only where there is snow
                if(snowmass(ix, iy, 1) > 0d0) then
                  if (check_conservation) call check_accumulation(ix, iy, time, snow, rain)

                  ! Splitting and fusing of boxes
                  call regrid(ix, iy, dummy_regrid)

                  ! Densification
                  call densify(ix, iy, snow + rain, dz)

                  ! Calculate liquid water content
                  lwc = lwmass(ix, iy, 1)/snowmass(ix, iy, 1)/rho_w/(1./rho_snow(ix, iy, 1) - 1./rho_i)

                  ! Net solar radiation 
                  call calculate_incoming_solar(ix, iy, nday_snowfall, time, K_sw, lwc)

                  ! TODO, ABo: subroutine for lw radiation, incl. corrections for T?

                  ! Energy Fluxes
                  dummy_energy = get_check_energy(ix, iy)

                  call energy_balance_calculation(ix, iy, time, climdata%temp(ix, iy, time), dz, K_sw, &
                    H_lh, K_lh, Q_heat, dummy_heat, vaporflux, D_lf, p_air(ix, iy), force_melting)

                  if (check_conservation) call check_flux(ix, iy, time, dummy_energy, dummy_heat)

                  ! Melt snow
                  if(force_melting) then
                    found_liquid_water(ix,iy) = .true.
                    dummy_energy = get_check_energy(ix, iy)

                    call melt_snow(ix,iy,time, climdata%temp(ix,iy,time), K_sw, K_lh, H_lh, Q_heat, dummy_melt, &
                      dummy_runoff, smb_ice(ix,iy), vaporflux, D_lf, dummy_melt_ice, dummy_regrid, dummy_e_qq, dummy_heat)

                    if (check_conservation) call check_melt(ix, iy, time, dummy_melt, dummy_energy, dummy_heat, dummy_runoff, dummy_e_qq)
                  end if

                  ! Water Percolation
                  if(maxval(lwmass(ix,iy,:)) .gt. 0d0) then
                    found_liquid_water(ix,iy) = .true.

                    call percolate(ix, iy, dummy_runoff)

                    if (check_conservation) call check_runoff(time, dummy_runoff)

                    dummy_energy = get_check_energy(ix, iy)              
                    ! Refreezing
                    call refreeze(ix, iy, dummy_refreeze, dummy_heat)

                    if (check_conservation) call check_refreeze(ix, iy, time, dummy_heat, dummy_refreeze, dummy_energy)
                  end if

                else
                  ! Melt ice of snow free, but ice containing, grid cells
                  call melt_ice(ix, iy, time, smb_ice(ix, iy), climdata%temp(ix,iy,time), climdata%precip(ix,iy,time), &
                    vaporflux, D_lf, dummy_melt_ice, dummy_rain_ice, p_air(ix, iy))
                end if

                ! Store data in temporary variables
                if (write_annual) then
                  if (.not. write_only_npz) call ncfile_annual%store_data(ix,iy,time,snow,vaporflux,mass0,dummy_runoff,dummy_refreeze,dummy_melt,dummy_melt_ice,dummy_regrid,dummy_rain_ice,smb_ice(ix,iy),rain,albedo_dynamic(ix,iy),snow_temp(ix,iy,1))
                  if (write_only_npz)       call npzfile_annual%store_data(ix,iy,time,snow,vaporflux,mass0,dummy_runoff,dummy_refreeze,dummy_melt,dummy_melt_ice,dummy_regrid,dummy_rain_ice,smb_ice(ix,iy),rain,albedo_dynamic(ix,iy),snow_temp(ix,iy,1),run_cell=run_cell_this_year(ix,iy), mask=landmask(ix,iy))
                end if
                if (write_monthly) then
                  call ncfile_monthly%store_data(ix,iy,time,snow,vaporflux,mass0,dummy_runoff,dummy_refreeze,dummy_melt,dummy_melt_ice,dummy_regrid,dummy_rain_ice,smb_ice(ix,iy),rain,albedo_dynamic(ix,iy),snow_temp(ix,iy,1))
                end if
                if (write_daily) then
                  if (.not. write_only_npz) call ncfile_daily%store_data(ix,iy,time,snow,vaporflux,mass0,dummy_runoff,dummy_refreeze,dummy_melt,dummy_melt_ice,dummy_regrid,dummy_rain_ice,smb_ice(ix,iy),rain,albedo_dynamic(ix,iy),snow_temp(ix,iy,1))
                  if (write_only_npz)       call npzfile_daily%store_data(ix,iy,time,snow,vaporflux,mass0,dummy_runoff,dummy_refreeze,dummy_melt,dummy_melt_ice,dummy_regrid,dummy_rain_ice,smb_ice(ix,iy),rain,albedo_dynamic(ix,iy),snow_temp(ix,iy,1))
                end if

                ! TODO: Can this check be omitted?!
                if(time .lt. ndays) then
                  if (check_conservation) call check_end_loop(ix, iy, time)
                end if
              end do! of time loop

              ! Calculate upper_massbound
              dmass = sum( snowmass(ix,iy,:)) - n_snowlayer * soll_mass *1.5
              ! Calculate mass balance for ice model after each year
              if (dmass .gt. 0d0) then
                made_ice(ix,iy) = .true.
                ! Maximum mass in one snow column is limited to upper_massbound*n_snowlayer. All surplus snow is moved to the ice model
                call smb_point(ix, iy, dummy_runoff, dmass, dummy_ice2ice, dummy_energy, smb_ice)

                ! Add smb_ice from smb_point(), which is outside the time loop:
                if (write_daily .and. write_only_npz)  call npzfile_daily%store_smbice(ix,iy,smb_ice(ix,iy))
                if (write_annual .and. write_only_npz) call npzfile_annual%store_smbice(ix,iy,smb_ice(ix,iy))

                if (check_conservation) call check_smb(dummy_ice2ice, dummy_energy, dummy_runoff)
              end if

              if (check_conservation) call check_end(ix, iy)

            end if! if (run_cell_this_year(ix,iy) .eq. .true.)

!               end if! if ((fast_calculation(ix, iy) .eqv. .false.) .or. (mod(myyear(it), calc_rate_snow) == 0))
!             end if! if (landmask(ix,iy) .ne. 1)

!             if ((landmask(ix,iy) .eq. 0) .and. (smb_ice(ix,iy) .gt. 0d0)) write(*,*) "Hey", ix,iy,myear,run_cell_this_year(ix,iy),smb_ice(ix,iy)

            ! Write daily values to disk
            if (write_daily .and. .not. write_only_npz) then
              call ncfile_daily%write_data(ix,iy)
            end if
            ! Write monthly values to disk
            if (write_monthly) then
              call ncfile_monthly%write_data(ix,iy)
            end if
          end do
        end do

!         write(*,*) "Hey", myear,run_cell_this_year(31,60),smb_ice(31,60),found_liquid_water(31,60),made_ice(31,60)
!         write(*,*) snowmass(31,60,:)
! #ifdef OMPRUN
        !$OMP END PARALLEL DO
! #endif        

! #ifdef OMPRUN
!             tm = omp_get_wtime()
!             print *, 'end of xy loop ', tm - tn
! #else
        call cpu_time(tm)
! #endif

        if (minval(snowmass)<0.) print*,'EOY: lightest snow grid cell', minval(snowmass),'heaviest snow grid cell', maxval(snowmass)
        if (minval(lwmass)<0.) print*,'EOY: negative lw content', minval(lwmass),'wettest snow grid cell', maxval(lwmass)

        ! Write annual data to disk:
        if (write_annual) then
          if (.not. write_only_npz) call ncfile_annual%write_data(-999,-999)! parameters ix,iy not used here
          if (write_only_npz)       call npzfile_annual%write_data
        end if

        if (write_daily) then
          if (write_only_npz)       call npzfile_daily%write_data
        end if
        
        if (check_conservation) then
            ! Creating a txt file with all mass and energy fluxes
            call write_check_mass(myear)
            call write_check_energy(myear)
        end if

    end subroutine calculate_firn

end module smb_emb
