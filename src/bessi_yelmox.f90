!> Contains the interface of the Bergen Snow Simulator (BESSI) with the 3D ice-sheet-shelf model Yelmo
module bessi_yelmox 

   use bessi_defs
   use bessi_io
   use smb_emb
   use bessi_climate
   use bessi_tools

   implicit none
   type(climinp_data_type) :: climate_data         !! climate input for th current model year
   integer                 :: maxyears_in_curr = 0 !! How many years bessi should run
   integer                 :: it = 1               !! number of iterations the model has gone through
   integer                 :: result_cmd = 1       !! don't now, why it exists, I just copied it from bessi.f90 xD
   integer                 :: model_year = 0       !! Current model year
   logical, parameter :: run_once = .true.  !! whether Bessi should run every year, or just once in a yelmo-timestep
   ! Note that when run_every_year is true and the length of one Bessi timestep is not a factor of the length of a yelmo-timestep in years, this could lead to inconsistencies in how often bessi gets called each timestep.
   
contains
! TODO: implement working precision wp, not important, but nice
! TODO: implement restart mechanism such that the layer initialization isn't necessary anymore (to make it faster we coult just not print the bessi data)

!> Initializes the surface mass balance model BESSI.
subroutine bessi_init(surface_height, init_layers) 

      implicit none
      real(4), intent(in) :: surface_height(nx,ny) !! Surface elevation [m], needed for calculating landmask
      logical, intent(in) :: init_layers  !! Boolean, whether to run BESSI for some time to fill up all the layers

      ! local variables
      integer :: init_layers_time = 150 !! Time [BESSI-Timestep length] how long BESSI should run, to fill up its layers
      integer :: init_myear = 131      !! index of year used to initialize bessi layers
      integer :: ii                    !! counter variable
      real(8) :: u

      ! Initialize output directory. The output of BESSI goes into the subdirectory "bessi_output" of the directory where the Yelmo-output goes.
      result_cmd = system('mkdir ./bessi_output')
      output_directory = './bessi_output/'

      ! Initialize climate input
      call climate_data%init

      if (init_layers) then
         ! Replacement for call load_mask() of bessi.f90: 
         elevation = dble(surface_height) ! BESSI needs the surface height from Yelmo
	 landmask = get_landmask(elevation, sea_level) ! And uses it to create a landmask

         write(*,*) "Running BESSI for", init_layers_time, "years to create snowlayers"
         do ii = 1, init_layers_time, 1
            call random_number(u)
            init_myear = 1 + FLOOR((131)*u)
            write(*,*) 'init_myear: ', init_myear

            call climate_data%update(init_myear)
            call calculate_firn(ndays, climate_data%temp, climate_data%precip, landmask, surface_mass_balance, init_myear)
         end do
         write(*,*) "Finished creating snowlayers"
         surface_mass_balance = 0
      endif


      ! Restart BESSI from prior run, sadly not implemented trustworthy anymore
      if (restart) call load_restart_state()

      write(*,*) '#####################################'
      write(*,*) '### Succesfully initialized BESSI ###'
      write(*,*) '#####################################'

   end subroutine bessi_init

   !> Calculates the surface mass balance (second argument), using BESSI. It needs the surface elevation (first argument) and the length of one yelmo_timestep (third argument).
   subroutine bessi_one_yelmo_timestep(surface_height, bessi_smb, timestep_length)
      implicit none
      real(4), intent(in)  :: surface_height(nx,ny) !! Surface elevation [m]
      real(4), intent(out) :: bessi_smb(nx,ny)      !! surface mass balance of bessi [m/yr] 
      real(4), intent(in)  :: timestep_length       !! length of one yelmo timestep [yr]


      ! local variables
      integer :: model_year_before = 0 !! Model year in last timestep. I created this variable such that the model year before is saved and the method calculate_year does not have to be called twice.
      integer :: it_start !! how many iterations were before the function gets called

      it_start = it
      elevation = dble(surface_height) ! BESSI needs the surface height from Yelmo
      landmask = get_landmask(elevation, sea_level) ! And uses it to create a landmask (i.e. "which gridpoints are land? → 2" "which are not? → 1")

      surface_mass_balance = 0
      bessi_smb = 0

      if (run_once) then
         model_year = model_year + 1! FIXME timestep_length
         write(*,*) 'model_year', model_year
         call climate_data%update(model_year)
         call calculate_firn(ndays, climate_data%temp, climate_data%precip, landmask, surface_mass_balance, model_year)
         bessi_smb = sngl(surface_mass_balance)

      else
         maxyears_in_curr = maxyears_in_curr + timestep_length ! Everytime the method gets called, the maxyears must be updated, such that the model runs for additional timestep_length years.

         do while (model_year_before < maxyears_in_curr)
            ! call start_time()

            ! Calculate the current model year:
            model_year = calculate_year(it) ! when dt (i.e. timestep lenght of bessi) is k years, then calculate_year(x)=x*k

            write(*,*) 'Start of year ', model_year

            ! Update climate
            call climate_data%update(model_year)
            call calculate_firn(ndays, climate_data%temp, climate_data%precip, landmask, surface_mass_balance, model_year)
            bessi_smb = bessi_smb + sngl(surface_mass_balance)

            ! some output to screen
            ! call stop_time()
            ! call print_heartbeat(myyear(it))

            ! Step one timestep forward
            it = int(it) + 1
            model_year_before = model_year
         end do
         ! bessi smb is calculated by adding the yearly mass balance of all years of this yelmo_timestep and then dividing by the number of times this happened, thus resulting in the average smb per year  
         bessi_smb = bessi_smb/(it - it_start)
      end if


      write(*,*) 'Gave new information to yelmo, year was', model_year


   end subroutine bessi_one_yelmo_timestep


end module
