module bessi_defs
    ! maximum simulation duration in years:
    integer, parameter        :: maxyears     = 10
    ! Output directory leading name:
    character(256), parameter :: run_name     = 'testrun'

    !=========================
    ! Domain
    !=========================
    real(kind=8), parameter ::              dx = 16000.!! [m] distance between two grid points in X-Direction
    real(kind=8), parameter ::              dy = 16000.!! [m] distance between two grid points in Y-Direction
    ! Length of domain: Greenland:   1640km = 1.64e6 = 82 * 20 km
    !                   NH20:       12480km = 1.248e7 = 624 * 20 km
    !                   NH40:       12480km = 1.248e7 = 312 * 40 km
    integer, parameter ::                    L = 1.64e6
    ! Width of domain:  Greenland:   2800km = 2.8e6 = 140 * 20 km
    !                   NH20:       12480km = 1.248e7 = 624 * 20
    !                   NH40:       12480km = 1.248e7 = 312 * 40 km
    integer, parameter ::                    W = 2.8e6
    integer, parameter ::                   nx = 281
    integer, parameter ::                   ny = 165
!     integer, parameter :: nx = floor(real(W/dx)) + 1! number of grid points in X Direction, +1 because 0 is also a grid box
!     integer, parameter :: ny = floor(real(L/dy)) + 1! number of grid points in Y Direction
    ! Vertical grid:
    integer,      parameter ::     n_snowlayer =  15 ! number of snowlayers, min 1
    real(kind=8), parameter ::       soll_mass = 300. ! aimed mass/area per layer
    real(kind=8), parameter :: lower_massbound = 100.
    real(kind=8), parameter :: upper_massbound = 500.

    !=========================
    ! Climate control
    !=========================
    ! Type of climate forcing; options: single_year, backandforth_climate, reorder_climate
    character(50), parameter ::   climate_type = 'single_year'
    ! Single forcing year:
    integer ::                       clim_year = 1980
    !> Lower bound for cycled time period
    integer ::                 clim_year_start = 1960 ! 79
    !> Upper bound for cycled time periode
    integer ::                  clim_year_turn = 1980
    !> Setting for cyclic climate
    logical ::               climate_backwards = .false.

    ! Sequence for the "reorder_climate" option:
    character(20), parameter :: reorder_climate_file = 'not-defined-for-option-single_year'

    !Initialize the firn cover with a previous output file of BESSI
    logical, parameter ::              restart = .false.
    !Firn initialization from a previous run
    character(256),parameter ::   restart_file = 'not-defined'

    ! DEBUG before use, do NOT change parameters
    ! Snow Speed up routine
!     logical ::               adaptive_timestep = .false.
    integer, parameter ::       calc_rate_snow = 1! calc_rate_climate
    integer, parameter ::       start_speed_up = 1000000
    integer, parameter ::         end_speed_up = 1000000

    !=========================
    ! Input from files
    !=========================
    !> Input topography:
    character(256), parameter :: netcdf_topo_file               = '/scratch2/leon/grl16/grl16_topo.nc'  ! '/Home/siv35/leber4267/interpolation/output_files/result.nc'
    ! The NetCDF Variable name of the relaxed bedrock
!     character(256), parameter :: netcdf_input_bedrock_variable  = 'ETOPO'
    ! The NetCDF Variable name of the nonrelaxed pd bedrock
!     character(256), parameter :: netcdf_input_pd_bedrock_variable = 'ETOPO'
    !> The NetCDF Variable name of the surface with pd ice sheet
    character(256), parameter :: netcdf_topo_varname            = 'ROSE'
    !> Climate directory with transient forcing (multiple years)
    character(128), parameter :: climate_input_dir              = '/scratch2/leon/luisas_eem_daten/AWI-ESM-1-1-LR/BESSI_input/'   ! alternative: '/scratch2/leon/grl16/'
    !> Climate reference topography
    character(128), parameter :: netcdf_climate_input_ref_topo  = '/scratch2/leon/grl16/grl16_ERA5_reftopo.nc' 
                
    !example: "ERAinterim_",'I4.4',".interp.cdf" for ERAinterim_yyyy.interp.cdf
    character(128),parameter :: netcdf_input_name_leading_string = "grl16_ERA5_AWI-ESM-1-1-LR_"
    character(128),parameter :: netcdf_input_name_end_string    = ".nc"
    ! for 4 length digit (yyyy)
    integer, parameter :: netcdf_input_digit_specification      = 4

    ! Input variable names
    character(256), parameter :: climate_input_temp_varname     = 't2m'  ! 2m temperature
    integer, parameter        :: temp_unit                      = 2      ! 1: K, 2: degC
    character(256), parameter :: climate_input_precip_varname   = 'tp'   ! precipitation
    integer, parameter        :: precip_unit                    = 2      ! 1: m/s, 2: mm/day
    character(256), parameter :: climate_input_swradboa_varname = 'ssrd' ! short wave radiation
    integer, parameter        :: swrd_unit                      = 1      ! 1: W/m2, 2: J/timestep (d)
    character(256), parameter :: climate_input_lwrd_varname     = 'strd' ! long wave radiation
    integer, parameter        :: lwrd_unit                      = 1      ! 1: W/m2, 2: J/timestep (d)
    character(256), parameter :: climate_input_dewpt_varname    = 'd2m'  ! 2m dew point temperature (degC)
    integer, parameter        :: dewpt_unit                     = 2      ! 1: K, 2: degC
    !character(256), parameter :: climate_input_wind_varname     = 'u10'  !'WIND_INTERP' ! wind speed
    !integer, parameter        :: wind_unit                      = 1      ! 1: m/s
    character(256), parameter :: climate_input_ref_topo_varname = 'geopot'

    ! NOTE choice of humiditysyle other than d2m not implmented, thus disabled for now
    !character(9), parameter   :: humiditystyle                  = "D2M"  !Q2M, RH_water, RH_ice
    !> Save netcdf input files including corrections to output directory:
    logical, parameter :: store_input_netcdf                    = .false.

    !=========================
    ! Model output
    !=========================
    ! Root output directory where a subdirectory with a timestamp and run_name is created
    ! ABo: This variable is not a parameter because it gets overwritten by init_output_directory at the beginning of a run. Not ideal.
    character(512)     :: output_directory  = '/work/leon/coupling_test/'
    !Snow model output frequency
    logical, parameter :: annual_data       = .true.
    logical, parameter :: monthly_data      = .false.
    logical, parameter :: daily_data        = .true.

    integer, parameter :: annual_data_freq  =  10 !write annual data every X years, extremly high value if no intervals are wanted
    integer, parameter :: monthly_data_freq = 100 !write monthly data every X years
    integer, parameter :: daily_data_freq   = 10  !write daily data every X years

    integer, parameter :: daily_data_start  = 10000 !from which all data that is set to .true. is written every year, overrides the intervals
    integer, parameter :: daily_data_end    = 10000
    
    logical, parameter :: annual_3D_data    = .true.
    logical, parameter :: monthly_3D_data   = .false.
    logical, parameter :: daily_3D_data     = .true.

    !=========================
    ! Flags
    !=========================
    ! Elevation is modified with lower-boundary mass balance (smb_ice). This should NOT be used when coupled with an ice sheet model:
    logical, parameter :: cumulative_elevation_change = .false.
    ! should melting ice be treated as run off on all cells
    logical, parameter :: include_values_over_ice = .false.
    ! Flag, initial ice sheet on Greenland, ABo, MARKED FOR DELETION 1-Aug-2023
!     logical, parameter ::       initial_gis = .true.
    !> Debug Level: 0 nothing, 1 basic info, 2 all
    integer, parameter ::             debug = 1
    !> model for snow densification for densities > 550 kg/m^3; 0 Barnola Pimienta, 1 Herron Langway
    integer, parameter ::                hl = 0
    !> model for snow temperature diffusivity
    integer, parameter ::        diff_model = 1!           1 Yen, 2 Sturm, 3 Dusen
    ! Constants for the energy and mass balance component of the model
    real(8), parameter :: snow_fall_temperature = 1.! determines below which temperature we consider precipitation as snowfall [°C]
    ! ABo: Rebekka's code. Does not work yet.
    ! Choose how precipitation is determined to be solid/liquid with precip_transition_type
    !  'linear': linear transition between and .
    !  'constant_threshold': everything below snow_fall_temperature is considered solid precipitation
    character(512) :: precip_transition_type = 'constant_threshold'
    real(8), parameter :: solid_precip_temp = -2, liquid_precip_temp = 2 ! For linear transition [degC]

    ! Ratio turbulent latent/sensible heat flux, to account for differences is the roughness length
    real(8), parameter      :: ratio        = 1
    !Choose albedo module, 1=constant, 2=Bougamont, 3=Oerlemans, 4=Aoki, 5=harmonic
    integer,parameter       :: albedo_module             = 4
    !Format of the humidity in the climate input file
    logical, parameter      :: latent_heat_flux_on = .true.
    ! Treat sublimation always as sublimation and not as vaporization at the melting point (enthalpy influence)
    logical, parameter      :: sublimation_only = .true.
    ! should the ratio between sensible and latent heat flux be considered
    logical, parameter      :: latent_heat_flux_analog_to_sensible_heat_flux = .true.
    ! if false, longwave downward radiation is taken from climate model input file; if true: sigmaT_air*4
    logical, parameter      :: longwave_from_air_temp = .false.
    ! downscale longwave radiation to the model topography based on the atmospheric temperature lapse rate
    logical, parameter      :: longwave_downscaling = .true.
    
    !> Snow densification model
    logical, parameter ::                   densification_model = .true.
    ! Lapse rates for temperature, dew point, and precipitation (the last only above a threshold; Budd and Smith, 1979):
    logical, parameter ::          active_temperature_lapserate = .true.
    logical, parameter ::             active_dewpoint_lapserate = .true.
    logical, parameter ::      active_elevation_desertification = .false. 
    !> Check for conservation of mass and energy
    logical, parameter ::                    check_conservation = .false.
    !> create txt to check mass conservation
    logical, parameter ::                         mass_checking = .false.
    !> create txt to check energy conservation
    logical, parameter ::                       energy_checking = .false.

!ABo, MARKED FOR DELETION, JULY 16 2023:
!     ! Interval of diagnostics in log file
!     integer, parameter :: log_file_interval = 5
    
    !=========================
    !  Meta Data NetCDF files
    !=========================
    character(512), parameter :: title       = 'BESSI model output'
    character(512), parameter :: history     = 'Created with BESSI 2.3'
    character(512), parameter :: source      = 'BESSI 2.3 model output'
    character(512), parameter :: institution = 'University of Bergen, Department of Earth Sciences'
!     character(512), parameter ::      forcing_data = ''
!     character(512), parameter ::      comment = 'Runtime Test'

    integer :: nc_counter = 1 !next entry to write in open nc file

    !===========
    ! Constants
    !===========
    real(kind=8), parameter ::                 seconds_per_year = 31536000d0
    real(kind=8), parameter ::                               dt = seconds_per_year!> time step [s]
    real(kind=8), parameter ::                                g = 9.81!>     acceleration of gravity [m s^-2]
    integer, parameter      ::                            ndays = 365!>      number of days per year (also used as number of timesteps)
    ! physical constants
    real(kind=8), parameter ::                            rho_w = 1000d0!   density of water [kg/m3]
    real(kind=8), parameter ::                            rho_s = 350d0!    density of falling snow [kg/m3]
    real(kind=8), parameter ::                            rho_i = 917d0!    density of ice [kg/m3]
    real(kind=8), parameter ::                            rho_e = 830d0!    density of ice at bubble close-off [kg/m3]
    real(kind=8), parameter ::                              c_w = 4181.!    heat capacity of water at 25C [J/kg/K]
    real(kind=8), parameter ::                              c_i = 2110.!    heat capacity of ice at -10C [J/kg/K]
    real(kind=8), parameter ::                             L_lh = 3.337e5!  latent heat of ice [J/kg]
    real(kind=8), parameter ::                              L_v = 2.501e6!  latent heat of vaporization
    !real(kind=8), parameter ::                             L_f = 337000.!  latent heat of fusion 
    real(kind=8), parameter ::                              K_i = 2.33!     thermal diffusivity of ice [W/m/K]
    real(kind=8), parameter ::                           cp_air = 1003!     heat capacity of air
    real(kind=8), parameter ::                            P_atm = 1e5!      standard air pressure [Pa]
    real(kind=8), parameter ::                       ocean_area = 3.625d14! global ocean area [m^2]

    real(kind=8), parameter ::                  albedo_snow_new = 0.85!>     albedo of fresh snow
    real(kind=8), parameter ::                  albedo_snow_wet = 0.72!>      albedo of wet snow
    real(kind=8), parameter ::                       albedo_ice = 0.3!>      albedo of ice 0.4 0.2
    real(kind=8), parameter ::                           kelvin = 273d0
    real(kind=8), parameter ::                             D_sf = 3!>        sensible heat flux snow-air [W/m2/K]

    real(kind=8), parameter ::                            sigma = 5.670373e-8 !> Stefan-Boltzmann constant [W/m2/K4]
    real(kind=8), parameter ::                          eps_air = 0.8!>      longwave emissivity of air (very uncertain)
    real(kind=8), parameter ::                         eps_snow = 0.98!>     longwave emissivity of snow
    real(kind=8), parameter ::                          max_lwc = 0.1!>      liquid water holding capacity of snow

    real(kind=8), parameter ::                          dt_firn = 365. * 3600. * 24./real(ndays)!> time step length [s]
    real(kind=8), parameter ::                      rho_pass_on = rho_e!>    density at which firn leaves the domain of BESSI

    real(kind=8), parameter ::           temperature_lapse_rate = 0.0065!> [K/m]
    real(kind=8), parameter ::              dewpoint_lapse_rate = 0.002
    !> extinction of short wave radiation in atmosphere (not in use, ABo July 17, 2023)
    !real(kind=8), parameter ::                        k_extinct = 0.0001 	! this value is just a guess, it corresponds to 10% per km
    ! Precipitation lapse rate (in SICOPOLIS gamma_p) above a specific elevation
    real(kind=8), parameter ::         precipitation_lapse_rate = 0.7/1000!> log(real(2)) [m^-1]
    integer, parameter ::               precipitation_threshold = 2000.!>    elevation threshold elevation desertification [m]

    !ABo: What are those good for? Remove?
!     real(kind=8), dimension(nx,ny,ndays) :: deviation_precip = 0.
!     real(kind=8), dimension(nx,ny,ndays) :: deviation_temp = 0.
!     real(kind=8), dimension(nx,ny,ndays) :: deviation_P_sun = 0.

    ! Runtime Variables
    !==================
    !- ICE -------------------------------------------------------------------------------
    ! Ice thickness
!     real(kind=8), dimension(nx,ny) :: ice_thickness = 0
    real(kind=8), dimension(nx,ny) :: elevation = 0 !! Elevation of ice surface above sea level [m]
    ! landmask: 0 = ice, 1 = water, 2 = no ice, 3 = unstable integration
    integer, dimension(nx, ny) :: landmask = 0   ! "Normal" case (ice) = 0, Water = 1, no ice in this time step = 2
    ! Sea level
    real(kind=8) :: sea_level = 0 !> Sea level [m]
    ! Output variables, with "realistic" values, values of the bedrock is not 0 at the sea
    !- BEDROCK ---------------------------------------------------------------------------
    ! The bedrock, with values below 0 on the water
!     real(kind=8), dimension(nx, ny) :: bedrock_netcdf
    ! Elevation with ice: variable 'elvevation' on land, bedrock_netcdf in the water
!     real(kind=8), dimension(nx, ny) :: elevation_netcdf
    ! (Potential) Temperature and Precipitation during runtime
    !- CLIMATE ---------------------------------------------------------------------------
    ! Temperature in Kelvin at the ice elevation for 365 days to calculate the accumulation and ablation out of it.
    real(kind=8), dimension(nx, ny, ndays) :: temperature = 0.
    ! Temperature in Kelvin at the sea level for 365 days to calculate the accumulation and ablation out of it.
    real(kind=8), dimension(nx, ny, ndays) :: potential_temperature = 0.
    ! Reference topography of the climate input data
    real(kind=8), dimension(nx,ny) :: climinp_ref_topo = 0.
    ! Precipitation (m/yr) for 365 days to calculate the accumulation and ablation out of it.
    real(kind=8), dimension(nx,ny,ndays) :: precipitation = 0.
    real(kind=8), dimension(nx,ny,ndays) :: dewpoint      = 0.
    real(kind=8), dimension(nx,ny,ndays) :: windspeed     = 0.
    real(kind=8), dimension(nx,ny,ndays) :: longwrd       = 0.
    real(kind=8), dimension(nx,ny,ndays) :: shortwrd      = 0.
    !- SNOW ------------------------------------------------------------------------------
    real(kind=8), dimension(nx,ny,n_snowlayer) :: snowmass             = 0d0!     snow mass kg/m2
    real(kind=8), dimension(nx,ny,n_snowlayer) :: lwmass               = 0d0!     liquid water in a gridcell in kg/m2
    real(kind=8), dimension(nx,ny,n_snowlayer) :: rho_snow             = rho_s!   density of snow in  kg/m3
    real(kind=8), dimension(nx,ny,n_snowlayer) :: snow_temp            = 0d0!     temperature of snow in C
    real(kind=8), dimension(nx,ny)             :: albedo_dynamic       = albedo_snow_new
    real(kind=8), dimension(nx,ny)             :: surface_mass_balance = 0d0
    logical,      dimension(nx,ny)             :: fast_calculation     = .false.

    

end module bessi_defs
