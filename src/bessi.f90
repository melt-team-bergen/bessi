program bessi
  ! Run the Bergen Snow Simulator (BESSI)

  use bessi_defs
  use bessi_io 
  use smb_emb
  use bessi_climate
  use bessi_boundary
  use bessi_tools

  implicit none
  integer                      :: it     = 1
  integer, dimension(maxyears) :: myyear = 0!   model year
  integer                      :: result_cmd = 1
  type(climinp_data_type)      :: climate_data! climate input for the current model year

  ! Initialize output directory
  result_cmd        = system('mkdir ./output')
  output_directory  = './output/'
!   call init_output_directory(output_directory, TRIM(adjustl(run_name)))

  ! Initialize climate input
  call climate_data%init

  ! Load masks (ice, bedrock)
  call load_mask()

  ! Restart BESSI from prior run
  if (restart) call load_restart_state()

  write(*,*) '####################'
  write(*,*) '### Start time loop'
  write(*,*) '####################'

  do while (myyear(it-1) < maxyears)

    call start_time()

    ! Calculate the current model year:
    myyear(it) = calculate_year(it)
    write(*,*) 'Start of year ', myyear(it)

    ! Update surface elevation (only for stand-alone BESSI):
    if (cumulative_elevation_change) call update_elevation(surface_mass_balance)

    ! Update climate
    call climate_data%update(myyear(it))

    call calculate_firn(ndays, climate_data, landmask, surface_mass_balance, myyear(it))
!     call calculate_firn(ndays, climate_data%temp, climate_data%precip, landmask, surface_mass_balance, myyear(it))

    ! some output to screen
    call stop_time()
    call print_heartbeat(myyear(it))

    ! Step one year forward
    it = it + 1
  end do

write(*,*) 'End of simulation at year ', myyear(it-1)


end program
