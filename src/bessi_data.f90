module bessi_data
    ! Data and writing module for annual, monthly, and daily variables.
    ! Module stores all variables to be written to disk on a time basis,
    ! and contains routines for writing the variables to disk 

  use bessi_defs
  use bessi_io

  ! for .npz output:
  use m_npy

  implicit none

  private
!   public :: write_check_mass
!   public :: write_check_energy
  public :: init_netcdf
  public :: bessi_store_vars
  public :: bessi_write_vars
  public :: reset_tmp_vars
  public :: deallocate_vars

  type, public :: nc_file
    character(18)                   :: filename
    integer                         :: ncid
    ! Save 3D data?:
    logical                         :: save_3D
    ! Dimensions:
    integer                         :: NLATS
    integer                         :: NLONS
    integer                         :: N_LAYER
    integer                         :: N_TIME
    integer                         :: lat_distance
    integer                         :: long_distance
    integer                         :: z_distance
    integer                         :: lon_dimid, lat_dimid, z_dimid, rec_dimid
    integer                         :: NDIMS = 4!    4D data (lat, lon, z, time)
    integer                         :: dimids3D(4)
    integer                         :: dimids2D(3)
    ! Variables:
    integer                         :: snowmass_varid
    integer                         :: lwmass_varid
    integer                         :: rho_snow_varid
    integer                         :: snow_temp_varid
    integer                         :: albedo_varid
    integer                         :: latent_heat_varid
    integer                         :: accum_varid
    integer                         :: rain_varid
    integer                         :: melt_varid
    integer                         :: refreezing_varid
    integer                         :: snow_varid
    integer                         :: runoff_varid
    integer                         :: snow_mask_varid
    integer                         :: melt_ice_varid
    integer                         :: snow_srftemp_ave_varid
    integer                         :: real_mass_balance_varid
    integer                         :: regridding_varid
    ! Dimension-variables:
    integer                         :: lat_varid, lon_varid, rec_varid, z_varid
    ! Temporary storage to prepare variables for writing to disk:
    real(kind=4), allocatable       :: snowmass_tmp(:,:,:)
    real(kind=4), allocatable       :: lwmass_tmp(:,:,:)
    real(kind=4), allocatable       :: rho_snow_tmp(:,:,:)
    real(kind=4), allocatable       :: snow_temp_tmp(:,:,:)
    real(kind=4), allocatable       :: albedo_tmp(:,:)
    real(kind=4), allocatable       :: vaporflux_tmp(:,:)
    real(kind=4), allocatable       :: accum_tmp(:,:)
    real(kind=4), allocatable       :: rain_tmp(:,:)
    real(kind=4), allocatable       :: melt_tmp(:,:)
    real(kind=4), allocatable       :: refreezing_tmp(:,:)
    real(kind=4), allocatable       :: snow_tmp(:,:)
    real(kind=4), allocatable       :: runoff_tmp(:,:)
    real(kind=4), allocatable       :: melt_ice_tmp(:,:)
    real(kind=4), allocatable       :: snow_srftemp_tmp(:,:)
    real(kind=4), allocatable       :: real_mass_balance_tmp(:,:)
    integer,      allocatable       :: snow_mask_tmp(:,:)
    integer,      allocatable       :: regridding_tmp(:,:)
    contains
    procedure          :: init       => init_netcdf
    procedure          :: store_data => bessi_store_vars
    procedure          :: write_data => bessi_write_vars
    procedure          :: reset_data => reset_tmp_vars
    procedure          :: clear      => deallocate_vars
  end type nc_file

  type, public :: npz_file
    character(18)                   :: filename
    character(256)                  :: filepath
    character(256)                  :: outp_type
    ! Save 3D data?:
    logical                         :: save_3D
    ! Temporary storage to prepare variables for writing to disk:
    real(kind=4), allocatable       :: snowmass_tmp(:,:,:,:)
    real(kind=4), allocatable       :: lwmass_tmp(:,:,:,:)
    real(kind=4), allocatable       :: rho_snow_tmp(:,:,:,:)
    real(kind=4), allocatable       :: snow_temp_tmp(:,:,:,:)
    real(kind=4), allocatable       :: albedo_tmp(:,:,:)
    real(kind=4), allocatable       :: vaporflux_tmp(:,:,:)
    real(kind=4), allocatable       :: accum_tmp(:,:,:)
    real(kind=4), allocatable       :: rain_tmp(:,:,:)
    real(kind=4), allocatable       :: melt_tmp(:,:,:)
    real(kind=4), allocatable       :: refreezing_tmp(:,:,:)
    real(kind=4), allocatable       :: snow_tmp(:,:,:)
    real(kind=4), allocatable       :: runoff_tmp(:,:,:)
    real(kind=4), allocatable       :: melt_ice_tmp(:,:,:)
    real(kind=4), allocatable       :: snow_srftemp_tmp(:,:,:)
    real(kind=4), allocatable       :: real_mass_balance_tmp(:,:,:)
!     integer,      allocatable       :: snow_mask_tmp(:,:,:)
    integer,      allocatable       :: regridding_tmp(:,:,:)
    integer,      allocatable       :: mask_tmp(:,:)
    integer,      allocatable       :: runcell_tmp(:,:)
    contains
    procedure          :: init         => init_npz
    procedure          :: store_data   => store_vars_npz
    procedure          :: store_smbice => add_smbice_end_of_year
    procedure          :: write_data   => write_npz
    procedure          :: reset_data   => reset_tmp_npz
    procedure          :: clear        => dealloc_npz
  end type npz_file

contains

! ################################################  
  subroutine init_netcdf(output_file)
    use netcdf
    use bessi_defs
    implicit none
    class(nc_file), intent(inout) :: output_file
    ! local variables:
    integer                       :: retval!       return value

    ! In addition to the latitude and longitude dimensions, we will also create latitude and longitude netCDF variables which will hold the actual latitudes and longitudes. Since they hold data about the coordinate system, the netCDF term for these is: "coordinate variables."
    real                          :: lats(output_file%NLATS)
    real                          :: lons(output_file%NLONS)
    real                          :: zs(output_file%N_LAYER)
    real                          :: timecounter(output_file%N_TIME)

    !ABo: can be replaced with numbers later (find below)
    real START_LAT, START_LON
    parameter (START_LAT = 0, START_LON = 0)
    !ABo: remove these two:
    character(256)                :: parameters
    character(512)                :: settings

    ! Dimensions:
    character*(*)                 :: lat_NAME, lon_NAME, rec_NAME, z_NAME
    parameter (lat_NAME='y', lon_NAME='x', z_NAME='layer', rec_NAME = 'time')
    ! Variables
    character*(*) snowmass_NAME, lwmass_NAME, rho_snow_NAME, snow_temp_NAME
    character*(*) albedo_NAME,latent_heat_NAME, melt_ice_NAME
    character*(*) snow_temp_ave_surface_NAME, regridding_NAME, real_mass_balance_NAME
    character*(*) snow_mask_NAME, accum_NAME, rain_NAME, melt_NAME, refreezing_NAME, snow_NAME, runoff_NAME
    parameter (snowmass_NAME              = 'snowmass')
    parameter (lwmass_NAME                = 'lwmass')
    parameter (rho_snow_NAME              = 'snowdensity')
    parameter (snow_temp_NAME             = 'snowtemp')
    parameter (albedo_NAME                = 'albedo')
    parameter (latent_heat_NAME           = 'latent_heat_flux')
    parameter (melt_ice_NAME              = 'melt_of_ice')
    parameter (snow_temp_ave_surface_NAME = 'snow surface temperature')
    parameter (regridding_NAME            = 'number_of_regrids')
    parameter (real_mass_balance_NAME     = 'mass_balance')
    parameter (snow_mask_NAME             = 'snow_mask')
    parameter (accum_NAME                 = 'accum_ice_model')
    parameter (rain_NAME                  = 'rain')
    parameter (melt_NAME                  = 'melt')
    parameter (refreezing_NAME            = 'refreezing')
    parameter (snow_NAME                  = 'snow')
    parameter (runoff_NAME                = 'runoff')

    character*(*) :: FillValue
    parameter (FillValue = '_FillValue')
    character*(*) :: STANDARDNAME!ABo: really needed?
    parameter (STANDARDNAME = 'standard_NAME')

    ! Define units:
    character*(*) UNITS
    parameter (UNITS = 'units')
    character*(*) snowmass_UNITS, lwmass_UNITS, rho_snow_UNITS, snow_temp_UNITS, albedo_UNITS, latent_heat_UNITS, melt_ice_UNITS, snow_temp_ave_surface_UNITS, regridding_UNITS, real_mass_balance_UNITS, LAT_UNITS, LON_UNITS, REC_UNITS, z_UNITS, accum_UNITS, rain_UNITS, melt_UNITS, refreezing_UNITS, snow_UNITS, runoff_UNITS
    parameter (LAT_UNITS = 'm')
    parameter (LON_UNITS = 'm')
    parameter (Z_UNITS   = 'n/a')
    parameter (REC_UNITS = '1000years')
    parameter (snowmass_UNITS = 'kg/m2', lwmass_UNITS = 'kg/m2',regridding_UNITS='#', rho_snow_UNITS = 'kg/m3', snow_temp_UNITS = 'K', snow_temp_ave_surface_UNITS='K', albedo_UNITS = '', latent_heat_UNITS='kg/m2/d', melt_ice_UNITS='kg/m2/d')
    parameter (accum_UNITS = 'kg/m2/d', rain_UNITS = 'kg/m2/d', melt_UNITS = 'kg/m2/d', refreezing_UNITS = 'kg/m2/d', snow_UNITS = 'kg/m2/d', runoff_UNITS = 'kg/m2/d', real_mass_balance_UNITS='kg/m2/d')

    !ABo: clean up:
    integer :: tttt, lat, lon, zz
    ! Distance of the lat/long points
    do lat = 1, output_file%NLATS
      lats(lat) = START_LAT + (lat - 1) * output_file%lat_distance
    end do
    do lon = 1, output_file%NLONS
      lons(lon) = START_LON + (lon - 1) * output_file%long_distance
    end do
    do zz = 1, output_file%N_LAYER
      zs(zz) = 1 + (real(zz) - 1) * output_file%z_distance
    end do
    do tttt = 1, output_file%N_TIME
      timecounter(tttt)= real(tttt)
    end do
    !print *, '*** Init NetCDF file: ', TRIM(adjustl(filename))

    ! Create the file
    call check(nf90_create(path=TRIM(adjustl(output_directory)) // output_file%filename, cmode=or(nf90_clobber,NF90_NETCDF4), ncid=output_file%ncid))
!     retval = nf90_create(path=TRIM(adjustl(output_directory)) // output_file%filename, cmode=or(nf90_clobber,NF90_HDF5), ncid=output_file%ncid)
!     if (retval .ne. nf_noerr) call handle_err(retval)

    ! Define dimensions:
    call check(nf90_def_dim(output_file%ncid, lat_name, output_file%NLATS, output_file%lat_dimid))
    call check(nf90_def_dim(output_file%ncid, lon_name, output_file%NLONS, output_file%lon_dimid))
    call check(nf90_def_dim(output_file%ncid, z_name, output_file%N_LAYER, output_file%z_dimid))
    call check(nf90_def_dim(output_file%ncid, rec_name, output_file%N_TIME, output_file%rec_dimid))
!     retval = nf_def_dim(output_file%ncid, lat_NAME, output_file%NLATS, output_file%lat_dimid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_dim(output_file%ncid, lon_NAME, output_file%NLONS, output_file%lon_dimid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_dim(output_file%ncid, z_NAME, output_file%N_LAYER, output_file%z_dimid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_dim(output_file%ncid, rec_NAME, output_file%N_TIME, output_file%rec_dimid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
    ! The dimids array is used to pass the dimids of the dimensions of the netCDF variables.
    if (output_file%N_TIME == 1) then
      ! For annual output files:
      output_file%dimids3D(1) = output_file%lon_dimid
      output_file%dimids3D(2) = output_file%lat_dimid
      output_file%dimids3D(3) = output_file%z_dimid
      output_file%dimids3D(4) = output_file%rec_dimid
      output_file%dimids2D(1) = output_file%lon_dimid
      output_file%dimids2D(2) = output_file%lat_dimid
      output_file%dimids2D(3) = output_file%rec_dimid
    else
      output_file%dimids3D(1) = output_file%rec_dimid
      output_file%dimids3D(2) = output_file%z_dimid
      output_file%dimids3D(3) = output_file%lon_dimid
      output_file%dimids3D(4) = output_file%lat_dimid
      output_file%dimids2D(1) = output_file%rec_dimid
      output_file%dimids2D(2) = output_file%lon_dimid
      output_file%dimids2D(3) = output_file%lat_dimid
    end if

    ! Define the coordinate variables. They will hold the coordinate information, that is, the latitudes and longitudes. A varid is returned for each.
    call check(nf90_def_var(output_file%ncid, LAT_NAME, NF90_REAL, output_file%lat_dimid, output_file%lat_varid))
    call check(nf90_def_var(output_file%ncid, LON_NAME, NF90_REAL, output_file%lon_dimid, output_file%lon_varid))
    call check(nf90_def_var(output_file%ncid, Z_NAME, NF90_REAL,   output_file%z_dimid,   output_file%z_varid))
    call check(nf90_def_var(output_file%ncid, rec_NAME, NF90_REAL, output_file%rec_dimid, output_file%rec_varid))
!     retval = nf_def_var(output_file%ncid, LAT_NAME, nf90_real, 1, output_file%lat_dimid, output_file%lat_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_var(output_file%ncid, LON_NAME, nf90_real, 1, output_file%lon_dimid, output_file%lon_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_var(output_file%ncid, Z_NAME,   nf90_real, 1, output_file%z_dimid,   output_file%z_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_var(output_file%ncid, rec_NAME, nf90_real, 1, output_file%rec_dimid, output_file%rec_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)

    ! Assign units attributes to coordinate var data. This attaches a text attribute to each of the coordinate variables, containing the units.
    call check(nf90_put_att(output_file%ncid, output_file%lat_varid, UNITS, LAT_UNITS))
    call check(nf90_put_att(output_file%ncid, output_file%lon_varid, UNITS, LON_UNITS))
    call check(nf90_put_att(output_file%ncid, output_file%z_varid,   UNITS, Z_UNITS))
    call check(nf90_put_att(output_file%ncid, output_file%rec_varid, UNITS, REC_UNITS))
!     retval = nf_put_att_text(output_file%ncid, output_file%lat_varid, UNITS, len(LAT_UNITS), LAT_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%lon_varid, UNITS, len(LON_UNITS), LON_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%z_varid,   UNITS, len(Z_UNITS),   Z_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%rec_varid, UNITS, len(REC_UNITS), REC_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)

    ! Define the netCDF variables:
    if (output_file%save_3D) then
      call check(nf90_def_var(output_file%ncid, snowmass_NAME, NF90_REAL,  output_file%dimids3D, output_file%snowmass_varid))
      call check(nf90_def_var(output_file%ncid, lwmass_NAME, NF90_REAL,    output_file%dimids3D, output_file%lwmass_varid))
      call check(nf90_def_var(output_file%ncid, rho_snow_NAME, NF90_REAL,  output_file%dimids3D, output_file%rho_snow_varid))
      call check(nf90_def_var(output_file%ncid, snow_temp_NAME, NF90_REAL, output_file%dimids3D, output_file%snow_temp_varid))
!       retval = nf_def_var(output_file%ncid, snowmass_NAME,  nf90_real, output_file%NDIMS,   output_file%dimids3D, output_file%snowmass_varid)
!       if (retval .ne. nf_noerr) call handle_err(retval)
!       retval = nf_def_var(output_file%ncid, lwmass_NAME,    nf90_real, output_file%NDIMS,   output_file%dimids3D, output_file%lwmass_varid)
!       if (retval .ne. nf_noerr) call handle_err(retval)
!       retval = nf_def_var(output_file%ncid, rho_snow_NAME,  nf90_real, output_file%NDIMS,   output_file%dimids3D, output_file%rho_snow_varid)
!       if (retval .ne. nf_noerr) call handle_err(retval)
!       retval = nf_def_var(output_file%ncid, snow_temp_NAME, nf90_real, output_file%NDIMS,   output_file%dimids3D, output_file%snow_temp_varid)
!       if (retval .ne. nf_noerr) call handle_err(retval)
    end if
    call check(nf90_def_var(output_file%ncid, albedo_NAME,                NF90_REAL, output_file%dimids2D, output_file%albedo_varid))
    call check(nf90_def_var(output_file%ncid, latent_heat_NAME,           NF90_REAL, output_file%dimids2D, output_file%latent_heat_varid))
    call check(nf90_def_var(output_file%ncid, accum_NAME,                 NF90_REAL, output_file%dimids2D, output_file%accum_varid))
    call check(nf90_def_var(output_file%ncid, rain_NAME,                  NF90_REAL, output_file%dimids2D, output_file%rain_varid))
    call check(nf90_def_var(output_file%ncid, melt_NAME,                  NF90_REAL, output_file%dimids2D, output_file%melt_varid))
    call check(nf90_def_var(output_file%ncid, refreezing_NAME,            NF90_REAL, output_file%dimids2D, output_file%refreezing_varid))
    call check(nf90_def_var(output_file%ncid, snow_NAME,                  NF90_REAL, output_file%dimids2D, output_file%snow_varid))
    call check(nf90_def_var(output_file%ncid, runoff_NAME,                NF90_REAL, output_file%dimids2D, output_file%runoff_varid))
    call check(nf90_def_var(output_file%ncid, SNOW_MASK_NAME,             NF90_INT,  output_file%dimids2D, output_file%snow_mask_varid))
    call check(nf90_def_var(output_file%ncid, melt_ice_NAME,              NF90_REAL, output_file%dimids2D, output_file%melt_ice_varid))
    call check(nf90_def_var(output_file%ncid, snow_temp_ave_surface_NAME, NF90_REAL, output_file%dimids2D, output_file%snow_srftemp_ave_varid))
    call check(nf90_def_var(output_file%ncid, regridding_NAME,            NF90_INT,  output_file%dimids2D, output_file%regridding_varid))
    call check(nf90_def_var(output_file%ncid, real_mass_balance_NAME,     NF90_REAL, output_file%dimids2D, output_file%real_mass_balance_varid))
!     retval = nf_def_var(output_file%ncid, albedo_NAME,      nf90_real, output_file%NDIMS-1, output_file%dimids2D, output_file%albedo_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_var(output_file%ncid, latent_heat_NAME, nf90_real, output_file%NDIMS-1, output_file%dimids2D, output_file%latent_heat_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_var(output_file%ncid, accum_NAME,       nf90_real, output_file%NDIMS-1, output_file%dimids2D, output_file%accum_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_var(output_file%ncid, rain_NAME,        nf90_real, output_file%NDIMS-1, output_file%dimids2D, output_file%rain_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_var(output_file%ncid, melt_NAME,        nf90_real, output_file%NDIMS-1, output_file%dimids2D, output_file%melt_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_var(output_file%ncid, refreezing_NAME,  nf90_real, output_file%NDIMS-1, output_file%dimids2D, output_file%refreezing_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_var(output_file%ncid, snow_NAME,        nf90_real, output_file%NDIMS-1, output_file%dimids2D, output_file%snow_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_var(output_file%ncid, runoff_NAME,      nf90_real, output_file%NDIMS-1, output_file%dimids2D, output_file%runoff_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_var(output_file%ncid, SNOW_MASK_NAME,   nf90_int, output_file%NDIMS-1, output_file%dimids2D, output_file%snow_mask_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_var(output_file%ncid, melt_ice_NAME,    nf90_real, output_file%NDIMS-1, output_file%dimids2D, output_file%melt_ice_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_var(output_file%ncid, snow_temp_ave_surface_NAME, nf90_real, output_file%NDIMS-1, output_file%dimids2D, output_file%snow_srftemp_ave_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_var(output_file%ncid, regridding_NAME,  nf90_int, output_file%NDIMS-1, output_file%dimids2D, output_file%regridding_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_def_var(output_file%ncid, real_mass_balance_NAME, nf90_real, output_file%NDIMS-1, output_file%dimids2D, output_file%real_mass_balance_varid)
!     if (retval .ne. nf_noerr) call handle_err(retval)

    ! Assign units attributes:
    if (output_file%save_3D) then
      call check(nf90_put_att(output_file%ncid, output_file%snowmass_varid,  UNITS, snowmass_UNITS))
      call check(nf90_put_att(output_file%ncid, output_file%lwmass_varid,    UNITS, lwmass_UNITS))
      call check(nf90_put_att(output_file%ncid, output_file%rho_snow_varid,  UNITS, rho_snow_UNITS))
      call check(nf90_put_att(output_file%ncid, output_file%snow_temp_varid, UNITS, snow_temp_UNITS))
!       retval = nf_put_att_text(output_file%ncid, output_file%snowmass_varid, UNITS, len(snowmass_UNITS), snowmass_UNITS)
!       if (retval .ne. nf_noerr) call handle_err(retval)
!       retval = nf_put_att_text(output_file%ncid, output_file%lwmass_varid, UNITS, len(lwmass_UNITS), lwmass_UNITS)
!       if (retval .ne. nf_noerr) call handle_err(retval)
!       retval = nf_put_att_text(output_file%ncid, output_file%rho_snow_varid, UNITS, len(rho_snow_UNITS), rho_snow_UNITS)
!       if (retval .ne. nf_noerr) call handle_err(retval)
!       retval = nf_put_att_text(output_file%ncid, output_file%snow_temp_varid, UNITS, len(snow_temp_UNITS), snow_temp_UNITS)
!       if (retval .ne. nf_noerr) call handle_err(retval)
    end if
    call check(nf90_put_att(output_file%ncid, output_file%albedo_varid,            UNITS, albedo_UNITS))
    call check(nf90_put_att(output_file%ncid, output_file%latent_heat_varid,       UNITS, latent_heat_UNITS))
    call check(nf90_put_att(output_file%ncid, output_file%accum_varid,             UNITS, accum_UNITS))
    call check(nf90_put_att(output_file%ncid, output_file%rain_varid,              UNITS, rain_UNITS))
    call check(nf90_put_att(output_file%ncid, output_file%melt_varid,              UNITS, melt_UNITS))
    call check(nf90_put_att(output_file%ncid, output_file%refreezing_varid,        UNITS, refreezing_UNITS))
    call check(nf90_put_att(output_file%ncid, output_file%snow_varid,              UNITS, snow_UNITS))
    call check(nf90_put_att(output_file%ncid, output_file%runoff_varid,            UNITS, runoff_UNITS))
    call check(nf90_put_att(output_file%ncid, output_file%melt_ice_varid,          UNITS, melt_ice_UNITS))
    call check(nf90_put_att(output_file%ncid, output_file%snow_srftemp_ave_varid,  UNITS, snow_temp_ave_surface_UNITS))
    call check(nf90_put_att(output_file%ncid, output_file%regridding_varid,        UNITS, regridding_UNITS))
    call check(nf90_put_att(output_file%ncid, output_file%real_mass_balance_varid, UNITS, real_mass_balance_UNITS))
!     retval = nf_put_att_text(output_file%ncid, output_file%albedo_varid, UNITS, len(albedo_UNITS), albedo_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%latent_heat_varid, UNITS, len(latent_heat_UNITS), latent_heat_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%accum_varid, UNITS, len(accum_UNITS), accum_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%rain_varid, UNITS, len(rain_UNITS), rain_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%melt_varid, UNITS, len(melt_UNITS), melt_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%refreezing_varid, UNITS, len(refreezing_UNITS), refreezing_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%snow_varid, UNITS, len(snow_UNITS), snow_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%runoff_varid, UNITS, len(runoff_UNITS), runoff_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%melt_ice_varid, UNITS, len(melt_ice_UNITS), melt_ice_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%snow_srftemp_ave_varid, UNITS, len(snow_temp_ave_surface_UNITS), snow_temp_ave_surface_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%regridding_varid, UNITS, len(regridding_UNITS), regridding_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%real_mass_balance_varid, UNITS, len(real_mass_balance_UNITS), real_mass_balance_UNITS)
!     if (retval .ne. nf_noerr) call handle_err(retval)

    ! Assign global attributes (ABo: title etc. defined in bessi_defs.F90 (maybe not ideal?)):
    call check(nf90_put_att(output_file%ncid, NF90_GLOBAL, 'title',       TRIM(adjustl(title))))
    call check(nf90_put_att(output_file%ncid, NF90_GLOBAL, 'history',     TRIM(adjustl(history))))
    call check(nf90_put_att(output_file%ncid, NF90_GLOBAL, 'source',      TRIM(adjustl(source))))
    call check(nf90_put_att(output_file%ncid, NF90_GLOBAL, 'institution', TRIM(adjustl(institution))))
!     retval = NF_PUT_ATT_TEXT (output_file%ncid, nf90_global, 'title', len(TRIM(adjustl(title))), TRIM(adjustl(title)))
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = NF_PUT_ATT_TEXT (output_file%ncid, nf90_global, 'history', len(TRIM(adjustl(history))), TRIM(adjustl(history)))
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = NF_PUT_ATT_TEXT (output_file%ncid, nf90_global, 'source', len(TRIM(adjustl(source))), TRIM(adjustl(source)))
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = NF_PUT_ATT_TEXT (output_file%ncid, nf90_global, 'institution',len(TRIM(adjustl(institution))), TRIM(adjustl(institution)))
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = NF_PUT_ATT_TEXT (output_file%ncid, nf90_global, 'forcing_data', len(TRIM(adjustl(new_input_file3))), TRIM(adjustl(new_input_file3)))     
!     if (retval .ne. nf_noerr) call handle_err(retval)
        
    write (parameters,'(A5,F8.4,A6,F8.4,A5,F8.4,A6,F8.4,A11,F8.4,A6,F8.4,A9,F8.4)') "a_fs:",albedo_snow_new," a_fi:",albedo_snow_wet," a_i:",albedo_ice, &
      " D_SH:",D_sf," D_LH/D_SH:",ratio," e_air:",eps_air," max_lwc:",max_lwc

    call check(nf90_put_att(output_file%ncid, NF90_GLOBAL, 'parameters', TRIM(adjustl(parameters))))
!     retval=NF_PUT_ATT_TEXT (output_file%ncid, nf90_global, 'parameters', len(TRIM(adjustl(parameters))), TRIM(adjustl(parameters)))
!     if (retval .ne. nf_noerr) call handle_err(retval)
        
    write (settings,'(A15,I1,A21,L1,A24,L1,A22,L1,A15,L1,A23,L1,A20,L1)') &
      " albedo_module:",          albedo_module, &
      " latent_heat_flux_on:",    latent_heat_flux_on, &
      " longwave_from_air_temp:", longwave_from_air_temp, &
      " longwave_downscaling:",   longwave_downscaling, &
      " densification:",          densification_model, &
      " temperature_lapserate:",  active_temperature_lapserate, &
      " dewpoint_lapserate:",     active_dewpoint_lapserate

    call check(nf90_put_att(output_file%ncid, NF90_GLOBAL, 'settings', TRIM(adjustl(settings))))
!     retval=NF_PUT_ATT_TEXT (output_file%ncid, nf90_global, 'settings', len(TRIM(adjustl(settings))), TRIM(adjustl(settings)))
!     if (retval .ne. nf_noerr) call handle_err(retval)

    !ABo: not sure what this is good for:
    if (output_file%save_3D) then
      call check(nf90_put_att(output_file%ncid, output_file%snowmass_varid,  STANDARDNAME, snowmass_NAME))
      call check(nf90_put_att(output_file%ncid, output_file%lwmass_varid,    STANDARDNAME, lwmass_NAME))
      call check(nf90_put_att(output_file%ncid, output_file%rho_snow_varid,  STANDARDNAME, rho_snow_NAME))
      call check(nf90_put_att(output_file%ncid, output_file%snow_temp_varid, STANDARDNAME, snow_temp_NAME))
!       retval = nf_put_att_text(output_file%ncid, output_file%snowmass_varid, STANDARDNAME, len(snowmass_NAME), snowmass_NAME)
!       if (retval .ne. nf_noerr) call handle_err(retval)
!       retval = nf_put_att_text(output_file%ncid, output_file%lwmass_varid, STANDARDNAME, len(lwmass_NAME), lwmass_NAME)
!       if (retval .ne. nf_noerr) call handle_err(retval)
!       retval = nf_put_att_text(output_file%ncid, output_file%rho_snow_varid, STANDARDNAME, len(rho_snow_NAME), rho_snow_NAME)
!       if (retval .ne. nf_noerr) call handle_err(retval)
!       retval = nf_put_att_text(output_file%ncid, output_file%snow_temp_varid, STANDARDNAME, len(snow_temp_NAME), snow_temp_NAME)
!       if (retval .ne. nf_noerr) call handle_err(retval)
    end if
    call check(nf90_put_att(output_file%ncid, output_file%albedo_varid,            STANDARDNAME, albedo_NAME))
    call check(nf90_put_att(output_file%ncid, output_file%latent_heat_varid,       STANDARDNAME, latent_heat_NAME))
    call check(nf90_put_att(output_file%ncid, output_file%accum_varid,             STANDARDNAME, accum_NAME))
    call check(nf90_put_att(output_file%ncid, output_file%rain_varid,              STANDARDNAME, rain_NAME))
    call check(nf90_put_att(output_file%ncid, output_file%melt_varid,              STANDARDNAME, melt_NAME))
    call check(nf90_put_att(output_file%ncid, output_file%refreezing_varid,        STANDARDNAME, refreezing_NAME))
    call check(nf90_put_att(output_file%ncid, output_file%snow_varid,              STANDARDNAME, snow_NAME))
    call check(nf90_put_att(output_file%ncid, output_file%runoff_varid,            STANDARDNAME, runoff_NAME))
    call check(nf90_put_att(output_file%ncid, output_file%melt_ice_varid,          STANDARDNAME, melt_ice_NAME))
    call check(nf90_put_att(output_file%ncid, output_file%snow_srftemp_ave_varid,  STANDARDNAME, snow_temp_ave_surface_NAME))
    call check(nf90_put_att(output_file%ncid, output_file%regridding_varid,        STANDARDNAME, regridding_NAME))
    call check(nf90_put_att(output_file%ncid, output_file%real_mass_balance_varid, STANDARDNAME, real_mass_balance_NAME))
!     retval = nf_put_att_text(output_file%ncid, output_file%albedo_varid, STANDARDNAME, len(albedo_NAME), albedo_NAME)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%latent_heat_varid, STANDARDNAME, len(latent_heat_NAME), latent_heat_NAME)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%accum_varid, STANDARDNAME, len(accum_NAME), accum_NAME)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%rain_varid, STANDARDNAME, len(rain_NAME), rain_NAME)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%melt_varid, STANDARDNAME, len(melt_NAME), melt_NAME)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%refreezing_varid, STANDARDNAME, len(refreezing_NAME), refreezing_NAME)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%snow_varid, STANDARDNAME, len(snow_NAME), snow_NAME)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%runoff_varid, STANDARDNAME, len(runoff_NAME), runoff_NAME)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%melt_ice_varid, STANDARDNAME, len(melt_ice_NAME), melt_ice_NAME)
!     if (retval .ne. nf_noerr) call handle_err(retval)    
!     retval = nf_put_att_text(output_file%ncid, output_file%snow_srftemp_ave_varid, STANDARDNAME, len(snow_temp_ave_surface_NAME), snow_temp_ave_surface_NAME)
!     if (retval .ne. nf_noerr) call handle_err(retval)  
!     retval = nf_put_att_text(output_file%ncid, output_file%regridding_varid, STANDARDNAME, len(regridding_NAME), regridding_NAME)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_att_text(output_file%ncid, output_file%real_mass_balance_varid, STANDARDNAME, len(real_mass_balance_NAME), real_mass_balance_NAME)
!     if (retval .ne. nf_noerr) call handle_err(retval)
        
    ! End define mode
    call check(nf90_enddef(output_file%ncid))
!     retval = nf_enddef(output_file%ncid)
!     if (retval .ne. nf_noerr) call handle_err(retval)

    ! Write the coordinate variable data. This will put the latitudes and longitudes of our data grid into the netCDF file.
    call check(nf90_put_var(output_file%ncid, output_file%lat_varid, lats))
    call check(nf90_put_var(output_file%ncid, output_file%lon_varid, lons))
    call check(nf90_put_var(output_file%ncid, output_file%z_varid,   zs))
    call check(nf90_put_var(output_file%ncid, output_file%rec_varid, timecounter))
!     retval = nf_put_var_real(output_file%ncid, output_file%lat_varid, lats)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_var_real(output_file%ncid, output_file%lon_varid, lons)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_var_real(output_file%ncid, output_file%z_varid,   zs)
!     if (retval .ne. nf_noerr) call handle_err(retval)
!     retval = nf_put_var_real(output_file%ncid, output_file%rec_varid, timecounter)
!     if (retval .ne. nf_noerr) call handle_err(retval)
    if (debug > 2) print *,'*** Output netCDF file initialized: ', TRIM(adjustl(output_file%filename))

    ! Allocate temporary storage variables (and reset to zero if necessary):
    if (output_file%N_TIME == 1) then
      ! Annual data:
      allocate(output_file%snowmass_tmp(output_file%NLONS,          output_file%NLATS, output_file%N_LAYER))
      allocate(output_file%lwmass_tmp(output_file%NLONS,            output_file%NLATS, output_file%N_LAYER))
      allocate(output_file%snow_temp_tmp(output_file%NLONS,         output_file%NLATS, output_file%N_LAYER))
      allocate(output_file%rho_snow_tmp(output_file%NLONS,          output_file%NLATS, output_file%N_LAYER))
      allocate(output_file%albedo_tmp(output_file%NLONS,            output_file%NLATS))
      allocate(output_file%vaporflux_tmp(output_file%NLONS,         output_file%NLATS))
      allocate(output_file%accum_tmp(output_file%NLONS,             output_file%NLATS))
      allocate(output_file%snow_tmp(output_file%NLONS,              output_file%NLATS))
      allocate(output_file%rain_tmp(output_file%NLONS,              output_file%NLATS))
      allocate(output_file%melt_tmp(output_file%NLONS,              output_file%NLATS))
      allocate(output_file%refreezing_tmp(output_file%NLONS,        output_file%NLATS))
      allocate(output_file%runoff_tmp(output_file%NLONS,            output_file%NLATS))
      allocate(output_file%snow_mask_tmp(output_file%NLONS,         output_file%NLATS))
      allocate(output_file%melt_ice_tmp(output_file%NLONS,          output_file%NLATS))
      allocate(output_file%snow_srftemp_tmp(output_file%NLONS,      output_file%NLATS))
      allocate(output_file%real_mass_balance_tmp(output_file%NLONS, output_file%NLATS))
      allocate(output_file%regridding_tmp(output_file%NLONS,        output_file%NLATS))
    elseif ((output_file%N_TIME == 12) .or. (output_file%N_TIME == ndays)) then
      ! Daily or monthly data (Need to create a degenerate dimension to be able to use for annual data):
      allocate(output_file%snowmass_tmp(output_file%N_TIME,         output_file%N_LAYER,1))
      allocate(output_file%lwmass_tmp(output_file%N_TIME,           output_file%N_LAYER,1))
      allocate(output_file%snow_temp_tmp(output_file%N_TIME,        output_file%N_LAYER,1))
      allocate(output_file%rho_snow_tmp(output_file%N_TIME,         output_file%N_LAYER,1))
      allocate(output_file%albedo_tmp(output_file%N_TIME,           1))
      allocate(output_file%vaporflux_tmp(output_file%N_TIME,        1))
      allocate(output_file%accum_tmp(output_file%N_TIME,            1))
      allocate(output_file%snow_tmp(output_file%N_TIME,             1))
      allocate(output_file%rain_tmp(output_file%N_TIME,             1))
      allocate(output_file%melt_tmp(output_file%N_TIME,             1))
      allocate(output_file%refreezing_tmp(output_file%N_TIME,       1))
      allocate(output_file%runoff_tmp(output_file%N_TIME,           1))
      allocate(output_file%snow_mask_tmp(output_file%N_TIME,        1))
      allocate(output_file%melt_ice_tmp(output_file%N_TIME,         1))
      allocate(output_file%snow_srftemp_tmp(output_file%N_TIME,     1))
      allocate(output_file%real_mass_balance_tmp(output_file%N_TIME,1))
      allocate(output_file%regridding_tmp(output_file%N_TIME,       1))
    end if

    ! Initialize all temporary variables with zeros:
    call output_file%reset_data()

  end subroutine init_netcdf


! ################################################  
  subroutine reset_tmp_vars(output_file)
    implicit none
    class(nc_file), intent(inout) :: output_file

    output_file%snowmass_tmp            = 0.
    output_file%lwmass_tmp              = 0.
    output_file%rho_snow_tmp            = 0.
    output_file%snow_temp_tmp           = 0.
    output_file%albedo_tmp              = 0.
    output_file%vaporflux_tmp           = 0.
    output_file%accum_tmp               = 0.
    output_file%snow_tmp                = 0.
    output_file%rain_tmp                = 0.
    output_file%melt_tmp                = 0.
    output_file%refreezing_tmp          = 0.
    output_file%runoff_tmp              = 0.
    output_file%melt_ice_tmp            = 0.
    output_file%snow_srftemp_tmp        = 0.
    output_file%real_mass_balance_tmp   = 0.
    output_file%snow_mask_tmp           = 0
    output_file%regridding_tmp          = 0
  end subroutine reset_tmp_vars


! ################################################  
  subroutine deallocate_vars(output_file)
    implicit none
    class(nc_file), intent(inout) :: output_file

    if (allocated(output_file%snowmass_tmp))          deallocate(output_file%snowmass_tmp)
    if (allocated(output_file%lwmass_tmp))            deallocate(output_file%lwmass_tmp)
    if (allocated(output_file%rho_snow_tmp))          deallocate(output_file%rho_snow_tmp)
    if (allocated(output_file%snow_temp_tmp))         deallocate(output_file%snow_temp_tmp)
    if (allocated(output_file%albedo_tmp))            deallocate(output_file%albedo_tmp)
    if (allocated(output_file%vaporflux_tmp))         deallocate(output_file%vaporflux_tmp)
    if (allocated(output_file%accum_tmp))             deallocate(output_file%accum_tmp)
    if (allocated(output_file%snow_tmp))              deallocate(output_file%snow_tmp)
    if (allocated(output_file%rain_tmp))              deallocate(output_file%rain_tmp)
    if (allocated(output_file%melt_tmp))              deallocate(output_file%melt_tmp)
    if (allocated(output_file%refreezing_tmp))        deallocate(output_file%refreezing_tmp)
    if (allocated(output_file%runoff_tmp))            deallocate(output_file%runoff_tmp)
    if (allocated(output_file%melt_ice_tmp))          deallocate(output_file%melt_ice_tmp)
    if (allocated(output_file%snow_srftemp_tmp))      deallocate(output_file%snow_srftemp_tmp)
    if (allocated(output_file%real_mass_balance_tmp)) deallocate(output_file%real_mass_balance_tmp)
    if (allocated(output_file%snow_mask_tmp))         deallocate(output_file%snow_mask_tmp)
    if (allocated(output_file%regridding_tmp))        deallocate(output_file%regridding_tmp)
  end subroutine deallocate_vars


! ################################################  
  subroutine bessi_store_vars(output_file, ix, iy, time, accum, vaporflux, mass0, runoff, refreeze, melt, melt_ice, regrid, rain_ice, smb_ice, rain, albedo, snow_srftemp)
    ! Store annual or daily data
    ! Annual data is annual averages for all 2D fields and end-of-year snapshots for 3D fields

    use bessi_defs
    implicit none
    class(nc_file), intent(inout) :: output_file
    integer,        intent(in)    :: ix
    integer,        intent(in)    :: iy
    integer,        intent(in)    :: time
    real(kind=8),   intent(in)    :: accum
    real(kind=8),   intent(in)    :: vaporflux
    real(kind=8),   intent(in)    :: mass0
    real(kind=8),   intent(in)    :: runoff
    real(kind=8),   intent(in)    :: refreeze
    real(kind=8),   intent(in)    :: melt
    real(kind=8),   intent(in)    :: melt_ice
    real(kind=8),   intent(in)    :: rain_ice
    real(kind=8),   intent(in)    :: rain
    real(kind=8),   intent(in)    :: smb_ice
    real(kind=8),   intent(in)    :: albedo
    real(kind=8),   intent(in)    :: snow_srftemp
    integer,        intent(in)    :: regrid

    ! If the output file has annual resolution (use N_TIME as proxy for that):
    if (output_file%N_TIME == 1) then
      output_file%snow_tmp(ix,iy)          = output_file%snow_tmp(ix,iy)          + real(accum*dt_firn)/ndays
      output_file%rain_tmp(ix,iy)          = output_file%rain_tmp(ix, iy)         + real(rain*dt_firn)/ndays   !rain on ice?
      output_file%vaporflux_tmp(ix,iy)     = output_file%vaporflux_tmp(ix, iy)    + real(vaporflux*dt_firn/(L_v + L_lh))/ndays
      output_file%runoff_tmp(ix,iy)        = output_file%runoff_tmp(ix, iy)       + real(runoff)/ndays
      output_file%refreezing_tmp(ix,iy)    = output_file%refreezing_tmp(ix, iy)   + real(refreeze)/ndays
      output_file%melt_tmp(ix,iy)          = output_file%melt_tmp(ix, iy)         + real(melt)/ndays !melt on ice
      output_file%melt_ice_tmp(ix,iy)      = output_file%melt_ice_tmp(ix, iy)     + real(melt_ice)/ndays
      output_file%albedo_tmp(ix,iy)        = output_file%albedo_tmp(ix, iy)       + real(albedo)/ndays
      output_file%accum_tmp(ix,iy)         = output_file%accum_tmp(ix,iy)         + real(smb_ice*rho_i*seconds_per_year)
      output_file%snow_srftemp_tmp(ix,iy)  = output_file%snow_srftemp_tmp(ix, iy) + real(snow_srftemp)/ndays
      output_file%regridding_tmp(ix,iy)    = output_file%regridding_tmp(ix, iy)   + regrid/ndays

      ! Only for the last time step (snapshots):
      if (time == ndays) then
        output_file%snowmass_tmp(ix,iy,:)  = real(snowmass(ix,iy,:))
        output_file%lwmass_tmp(ix,iy,:)    = real(lwmass(ix,iy,:))
        output_file%rho_snow_tmp(ix,iy,:)  = real(rho_snow(ix,iy,:))
        output_file%snow_temp_tmp(ix,iy,:) = real(snow_temp(ix,iy,:))
        output_file%real_mass_balance_tmp(ix,iy) = real(sum(snowmass(ix,iy,:)) - mass0 - output_file%melt_ice_tmp(ix,iy))
      end if

      if (include_values_over_ice) then
        output_file%runoff_tmp(ix, iy)     = output_file%runoff_tmp(ix, iy)       + real(melt_ice)/ndays
        output_file%rain_tmp(ix, iy)       = output_file%rain_tmp(ix, iy)         + real(rain_ice*dt_firn)/ndays
      end if

      if (snowmass(ix, iy, 1) .gt. 0.) output_file%snow_mask_tmp(ix,iy) = output_file%snow_mask_tmp(ix, iy) + 1 ! Unit: number of days with snow cover
    end if

    ! If the output file has daily resolution (use N_TIME as proxy for that):
    if (output_file%N_TIME == ndays) then
      output_file%snowmass_tmp(time,:,1)   = real(snowmass(ix,iy,:))
      output_file%lwmass_tmp(time,:,1)     = real(lwmass(ix,iy,:))
      output_file%rho_snow_tmp(time,:,1)   = real(rho_snow(ix,iy,:))
      output_file%snow_temp_tmp(time,:,1)  = real(snow_temp(ix,iy,:))
      output_file%albedo_tmp(time,1)       = real(albedo)
      output_file%vaporflux_tmp(time,1)    = real(vaporflux)
      output_file%accum_tmp(time,1)        = real(smb_ice*rho_i*seconds_per_year) !ABo: seconds_per_year for daily data?
      output_file%snow_tmp(time,1)         = real(accum*dt_firn)
      output_file%rain_tmp(time,1)         = real(rain*dt_firn)
      output_file%refreezing_tmp(time,1)   = real(refreeze)
      output_file%melt_tmp(time,1)         = real(melt)
      output_file%runoff_tmp(time,1)       = real(runoff)
      output_file%melt_ice_tmp(time,1)     = real(melt_ice)
      ! Extra for wet snow else useless as snowmass, in general not very useful
      if (snowmass(ix, iy, 1) .gt. 0.) output_file%snow_mask_tmp(time,1) = 1

      ! If there is no snow at the current location, set all temperatures to the freezing point. ABo: Maybe better to use more obvious value, e.g., -999?
      if (output_file%snow_temp_tmp(time,1,1) == 0) output_file%snow_temp_tmp(time,:,1) = 273.15
      ! Extra for wet snow else useless as snowmass, in general not very useful:
      if (snowmass(ix, iy, 1) .gt. 0.) output_file%snow_mask_tmp(time,1) = 1

      if (time .eq. 1) then
        output_file%real_mass_balance_tmp(time,1) = real(sum(snowmass(ix, iy, :))) - mass0 - real(melt_ice)
      else
        output_file%real_mass_balance_tmp(time,1) = real(sum(snowmass(ix, iy, :))) - sum(output_file%snowmass_tmp(time-1,:,1)) - real(melt_ice)
!         output_file%real_mass_balance_tmp(time,1) = real(sum(snowmass(ix, iy, :))) - output_file%real_mass_balance_tmp(time-1,1) - real(melt_ice)
      end if

      output_file%regridding_tmp(time,1)   = regrid
      output_file%snow_srftemp_tmp(time,1) = real(snow_srftemp)
            
      if (include_values_over_ice) then
        output_file%runoff_tmp(time,1)     = output_file%runoff_tmp(time,1) + real(melt_ice)
        output_file%rain_tmp(time,1)       = output_file%rain_tmp(time,1)   + real(rain_ice*dt_firn)
      end if
    end if

  end subroutine bessi_store_vars


! ################################################  
  subroutine dealloc_npz(output_file)
    implicit none
    class(npz_file), intent(inout) :: output_file

    if (allocated(output_file%snowmass_tmp))          deallocate(output_file%snowmass_tmp)
    if (allocated(output_file%lwmass_tmp))            deallocate(output_file%lwmass_tmp)
    if (allocated(output_file%rho_snow_tmp))          deallocate(output_file%rho_snow_tmp)
    if (allocated(output_file%snow_temp_tmp))         deallocate(output_file%snow_temp_tmp)
    if (allocated(output_file%albedo_tmp))            deallocate(output_file%albedo_tmp)
    if (allocated(output_file%vaporflux_tmp))         deallocate(output_file%vaporflux_tmp)
    if (allocated(output_file%accum_tmp))             deallocate(output_file%accum_tmp)
    if (allocated(output_file%snow_tmp))              deallocate(output_file%snow_tmp)
    if (allocated(output_file%rain_tmp))              deallocate(output_file%rain_tmp)
    if (allocated(output_file%melt_tmp))              deallocate(output_file%melt_tmp)
    if (allocated(output_file%refreezing_tmp))        deallocate(output_file%refreezing_tmp)
    if (allocated(output_file%runoff_tmp))            deallocate(output_file%runoff_tmp)
    if (allocated(output_file%melt_ice_tmp))          deallocate(output_file%melt_ice_tmp)
    if (allocated(output_file%snow_srftemp_tmp))      deallocate(output_file%snow_srftemp_tmp)
    if (allocated(output_file%real_mass_balance_tmp)) deallocate(output_file%real_mass_balance_tmp)
!     if (allocated(output_file%snow_mask_tmp))         deallocate(output_file%snow_mask_tmp)
    if (allocated(output_file%regridding_tmp))        deallocate(output_file%regridding_tmp)
    if (allocated(output_file%mask_tmp))              deallocate(output_file%mask_tmp)
    if (allocated(output_file%runcell_tmp))           deallocate(output_file%runcell_tmp)
  end subroutine dealloc_npz


! ################################################  
  subroutine bessi_write_vars(output_file, ix, iy)
    ! Write data to disk, at the end of the year.

    implicit none
    class(nc_file), intent(inout)     :: output_file
    integer,        intent(in)        :: ix
    integer,        intent(in)        :: iy

    ! local variables to calculate monthly averages:
    integer                           :: m
    integer, dimension(12), parameter :: month_start = (/  1, 32, 60,  91, 121, 152, 182, 213, 244, 274, 305, 335 /)
    integer, dimension(12), parameter :: month_end   = (/ 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 /)
    real                              :: snowmass_monthly(12,output_file%N_LAYER)
    real                              :: lwmass_monthly(12,  output_file%N_LAYER)
    real                              :: rho_snow_monthly(12,output_file%N_LAYER)
    real                              :: snowtemp_monthly(12,output_file%N_LAYER)
    real                              :: albedo_monthly(12)
    real                              :: vaporflux_monthly(12)
    real                              :: accum_monthly(12)
    real                              :: snow_monthly(12)
    real                              :: rain_monthly(12)
    real                              :: melt_monthly(12)
    real                              :: refreezing_monthly(12)
    real                              :: runoff_monthly(12)
    real                              :: melt_ice_monthly(12)
    real                              :: snow_srft_monthly(12)
    real                              :: rmb_monthly(12)
    real                              :: snow_mask_monthly(12)
    real                              :: regridding_monthly(12)

    ! If the output file has annual resolution (use N_TIME as proxy for that):
    if (output_file%N_TIME == 1) then
      !Write 3D values (snapshots at the end of the year)
      if (output_file%save_3D) then
        call writeNCDFSNOW3DValues(output_file%ncid, 1, output_file%snowmass_varid,           output_file%snowmass_tmp,             output_file%NLATS,output_file%NLONS,output_file%N_LAYER, output_file%N_TIME)
        call writeNCDFSNOW3DValues(output_file%ncid, 1, output_file%lwmass_varid,             output_file%lwmass_tmp,               output_file%NLATS,output_file%NLONS,output_file%N_LAYER, output_file%N_TIME)
        call writeNCDFSNOW3DValues(output_file%ncid, 1, output_file%snow_temp_varid,          output_file%snow_temp_tmp,            output_file%NLATS,output_file%NLONS,output_file%N_LAYER, output_file%N_TIME)
        call writeNCDFSNOW3DValues(output_file%ncid, 1, output_file%rho_snow_varid,           output_file%rho_snow_tmp,             output_file%NLATS,output_file%NLONS,output_file%N_LAYER, output_file%N_TIME)
      end if
      !Write 2D values per grid point (averages)
      call writeNCDFSNOW2DValues(output_file%ncid, 1, output_file%accum_varid,                output_file%accum_tmp,                output_file%NLATS,output_file%NLONS,1)
      call writeNCDFSNOW2DValues(output_file%ncid, 1, output_file%snow_varid,                 output_file%snow_tmp,                 output_file%NLATS,output_file%NLONS,1)
      call writeNCDFSNOW2DValues(output_file%ncid, 1, output_file%rain_varid,                 output_file%rain_tmp,                 output_file%NLATS,output_file%NLONS,1)
      call writeNCDFSNOW2DValues(output_file%ncid, 1, output_file%latent_heat_varid,          output_file%vaporflux_tmp,            output_file%NLATS,output_file%NLONS,1)
      call writeNCDFSNOW2DValues(output_file%ncid, 1, output_file%albedo_varid,               output_file%albedo_tmp,               output_file%NLATS,output_file%NLONS,1)
      call writeNCDFSNOW2DValues(output_file%ncid, 1, output_file%runoff_varid,               output_file%runoff_tmp,               output_file%NLATS,output_file%NLONS,1)
      call writeNCDFSNOW2DValues(output_file%ncid, 1, output_file%melt_varid,                 output_file%melt_tmp,                 output_file%NLATS,output_file%NLONS,1)
      call writeNCDFSNOW2DValues(output_file%ncid, 1, output_file%refreezing_varid,           output_file%refreezing_tmp,           output_file%NLATS,output_file%NLONS,1)
      call writeNCDFSNOW2DValues(output_file%ncid, 1, output_file%melt_ice_varid,             output_file%melt_ice_tmp,             output_file%NLATS,output_file%NLONS,1)
      call writeNCDFSNOW2DValues(output_file%ncid, 1, output_file%snow_mask_varid,            real(output_file%snow_mask_tmp),      output_file%NLATS,output_file%NLONS,1)
      call writeNCDFSNOW2DValues(output_file%ncid, 1, output_file%snow_srftemp_ave_varid,     output_file%snow_srftemp_tmp,         output_file%NLATS,output_file%NLONS,1)
      call writeNCDFSNOW2DValues(output_file%ncid, 1, output_file%real_mass_balance_varid,    output_file%real_mass_balance_tmp,    output_file%NLATS,output_file%NLONS,1)
      call writeNCDFSNOW2DValues(output_file%ncid, 1, output_file%regridding_varid,           real(output_file%regridding_tmp),     output_file%NLATS,output_file%NLONS,1)
      call closeNCDFFile(output_file%ncid)
      ! Deallocate temporary variables:
      call output_file%clear
      if (debug > 0) print *,'Writing output to ', output_file%filename
    end if

    ! If the output file has monthly resolution (use N_TIME as proxy for that):
    if (output_file%N_TIME == 12) then
      ! Re-shape the temporary daily data to monthly resolution:
      do m = 1, 12, 1
        if (output_file%save_3D) then
          snowmass_monthly(m,:) = sum(output_file%snowmass_tmp(month_start(m):month_end(m),:,1),        1)
          lwmass_monthly(m,:)   = sum(output_file%lwmass_tmp(month_start(m):month_end(m),:,1),          1)
          rho_snow_monthly(m,:) = sum(output_file%rho_snow_tmp(month_start(m):month_end(m),:,1),        1)
          snowtemp_monthly(m,:) = sum(output_file%snow_temp_tmp(month_start(m):month_end(m),:,1),       1)
        end if
        albedo_monthly(m)     = sum(output_file%albedo_tmp(month_start(m):month_end(m),1),              1)
        vaporflux_monthly(m)  = sum(output_file%vaporflux_tmp(month_start(m):month_end(m),1),           1)
        accum_monthly(m)      = sum(output_file%accum_tmp(month_start(m):month_end(m),1),               1)
        snow_monthly(m)       = sum(output_file%snow_tmp(month_start(m):month_end(m),1),                1)
        rain_monthly(m)       = sum(output_file%rain_tmp(month_start(m):month_end(m),1),                1)
        melt_monthly(m)       = sum(output_file%melt_tmp(month_start(m):month_end(m),1),                1)
        refreezing_monthly(m) = sum(output_file%refreezing_tmp(month_start(m):month_end(m),1),          1)
        runoff_monthly(m)     = sum(output_file%runoff_tmp(month_start(m):month_end(m),1),              1)
        melt_ice_monthly(m)   = sum(output_file%melt_ice_tmp(month_start(m):month_end(m),1),            1)
        snow_srft_monthly(m)  = sum(output_file%snow_srftemp_tmp(month_start(m):month_end(m),1),        1)
        rmb_monthly(m)        = sum(output_file%real_mass_balance_tmp(month_start(m):month_end(m),1),   1)
        snow_mask_monthly(m)  = real(sum(output_file%snow_mask_tmp(month_start(m):month_end(m),1),      1))
        regridding_monthly(m) = real(sum(output_file%regridding_tmp(month_start(m):month_end(m),1),     1))
      end do

      if (output_file%save_3D) then
        call writeNCDFSNOW3D_pointwise_t_zxy(output_file%ncid,1, output_file%snowmass_varid,  snowmass_monthly,      iy,ix,output_file%N_LAYER,12)
        call writeNCDFSNOW3D_pointwise_t_zxy(output_file%ncid,1, output_file%lwmass_varid,    lwmass_monthly,        iy,ix,output_file%N_LAYER,12)
        call writeNCDFSNOW3D_pointwise_t_zxy(output_file%ncid,1, output_file%rho_snow_varid,  rho_snow_monthly,      iy,ix,output_file%N_LAYER,12)
        call writeNCDFSNOW3D_pointwise_t_zxy(output_file%ncid,1, output_file%snow_temp_varid, snowtemp_monthly,      iy,ix,output_file%N_LAYER,12)
      end if
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%albedo_varid,      albedo_monthly,        iy,ix,                    12)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%latent_heat_varid, vaporflux_monthly,     iy,ix,                    12)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%accum_varid,       accum_monthly,         iy,ix,                    12)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%rain_varid,        rain_monthly,          iy,ix,                    12)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%refreezing_varid,  refreezing_monthly,    iy,ix,                    12)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%melt_varid,        melt_monthly,          iy,ix,                    12)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%snow_varid,        snow_monthly,          iy,ix,                    12)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%runoff_varid,      runoff_monthly,        iy,ix,                    12)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%snow_mask_varid,   snow_mask_monthly,     iy,ix,                    12)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%melt_ice_varid,    melt_ice_monthly,      iy,ix,                    12)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%real_mass_balance_varid, rmb_monthly,     iy,ix,                    12)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%snow_srftemp_ave_varid, snow_srft_monthly,iy,ix,                    12)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%regridding_varid,  regridding_monthly,    iy,ix,                    12)
      if ((ix .eq. output_file%NLONS) .and. (iy .eq. output_file%NLATS)) then
        call closeNCDFFile(output_file%ncid)
        ! Deallocate temporary variables:
        call output_file%clear
        if (debug > 0) print *,'Writing output to ', output_file%filename, '.'
      else
        call output_file%reset_data()
      end if
    end if

    ! If the output file has daily resolution (use N_TIME as proxy for that):
    if (output_file%N_TIME == ndays) then
      if (output_file%save_3D) then
        call writeNCDFSNOW3D_pointwise_t_zxy(output_file%ncid,1, output_file%snowmass_varid,  output_file%snowmass_tmp(:,:,1),      iy,ix,output_file%N_LAYER,ndays)
        call writeNCDFSNOW3D_pointwise_t_zxy(output_file%ncid,1, output_file%lwmass_varid,    output_file%lwmass_tmp(:,:,1),        iy,ix,output_file%N_LAYER,ndays)
        call writeNCDFSNOW3D_pointwise_t_zxy(output_file%ncid,1, output_file%rho_snow_varid,  output_file%rho_snow_tmp(:,:,1),      iy,ix,output_file%N_LAYER,ndays)
        call writeNCDFSNOW3D_pointwise_t_zxy(output_file%ncid,1, output_file%snow_temp_varid, output_file%snow_temp_tmp(:,:,1),     iy,ix,output_file%N_LAYER,ndays)
      end if
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%albedo_varid,      output_file%albedo_tmp(:,1),          iy,ix,                    ndays)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%latent_heat_varid, output_file%vaporflux_tmp(:,1),       iy,ix,                    ndays)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%accum_varid,       output_file%accum_tmp(:,1),           iy,ix,                    ndays)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%rain_varid,        output_file%rain_tmp(:,1),            iy,ix,                    ndays)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%refreezing_varid,  output_file%refreezing_tmp(:,1),      iy,ix,                    ndays)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%melt_varid,        output_file%melt_tmp(:,1),            iy,ix,                    ndays)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%snow_varid,        output_file%snow_tmp(:,1),            iy,ix,                    ndays)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%runoff_varid,      output_file%runoff_tmp(:,1),          iy,ix,                    ndays)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%snow_mask_varid,   real(output_file%snow_mask_tmp(:,1)), iy,ix,                    ndays)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%melt_ice_varid,    output_file%melt_ice_tmp(:,1),        iy,ix,                    ndays)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%real_mass_balance_varid, output_file%real_mass_balance_tmp(:,1),iy,ix,             ndays)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%snow_srftemp_ave_varid, output_file%snow_srftemp_tmp(:,1),iy,ix,                   ndays)
      call writeNCDFSNOW2D_pointwise_t_xy(output_file%ncid, 1, output_file%regridding_varid,  real(output_file%regridding_tmp(:,1)),iy,ix,                    ndays)
      if ((ix .eq. output_file%NLONS) .and. (iy .eq. output_file%NLATS)) then
        call closeNCDFFile(output_file%ncid)
        ! Deallocate temporary variables:
        call output_file%clear
        if (debug .ge. 2) print *,'Writing output to ', output_file%filename, '.'
      else
        call output_file%reset_data()
      end if
    end if

  end subroutine bessi_write_vars


! ################################################  
  subroutine init_npz(output_file, outp_freq, model_year, write_3D)
    ! Store annual or daily data
    ! Annual data is annual averages for all 2D fields and end-of-year snapshots for 3D fields

    use bessi_defs
    implicit none
    class(npz_file), intent(inout) :: output_file
    character(len=*), intent(in)   :: outp_freq
    integer, intent(in)            :: model_year
    logical, optional, intent(in)  :: write_3D
    
    output_file%outp_type = outp_freq
    write (output_file%filename, '( A6, ".", I7.7, ".npz" )') output_file%outp_type, model_year

    output_file%filepath = TRIM(adjustl(output_directory)) // output_file%filename

    if (present(write_3D)) then
      output_file%save_3D = write_3D
    else
      output_file%save_3D = .true.
    end if
    ! Allocate temporary storage variables (and reset to zero if necessary):
    if (output_file%outp_type == 'ANNUAL') then
      if (output_file%save_3D) then
        allocate(output_file%snowmass_tmp(nx,ny,n_snowlayer,1))
        allocate(output_file%lwmass_tmp(nx,ny,n_snowlayer,1))
        allocate(output_file%snow_temp_tmp(nx,ny,n_snowlayer,1))
        allocate(output_file%rho_snow_tmp(nx,ny,n_snowlayer,1))
      end if
      allocate(output_file%albedo_tmp(nx,ny,1))
      allocate(output_file%vaporflux_tmp(nx,ny,1))
      allocate(output_file%accum_tmp(nx,ny,1))
      allocate(output_file%snow_tmp(nx,ny,1))
      allocate(output_file%rain_tmp(nx,ny,1))
      allocate(output_file%melt_tmp(nx,ny,1))
      allocate(output_file%refreezing_tmp(nx,ny,1))
      allocate(output_file%runoff_tmp(nx,ny,1))
!       allocate(output_file%snow_mask_tmp(nx,ny,1))
      allocate(output_file%melt_ice_tmp(nx,ny,1))
      allocate(output_file%snow_srftemp_tmp(nx,ny,1))
      allocate(output_file%real_mass_balance_tmp(nx,ny,1))
      allocate(output_file%regridding_tmp(nx,ny,1))
      allocate(output_file%runcell_tmp(nx,ny))
      allocate(output_file%mask_tmp(nx,ny))
    elseif (output_file%outp_type == 'DAILY_') then
      if (output_file%save_3D) then
        allocate(output_file%snowmass_tmp(nx,ny,n_snowlayer,ndays))
        allocate(output_file%lwmass_tmp(nx,ny,n_snowlayer,ndays))
        allocate(output_file%snow_temp_tmp(nx,ny,n_snowlayer,ndays))
        allocate(output_file%rho_snow_tmp(nx,ny,n_snowlayer,ndays))
      end if
      allocate(output_file%albedo_tmp(nx,ny,ndays))
      allocate(output_file%vaporflux_tmp(nx,ny,ndays))
      allocate(output_file%accum_tmp(nx,ny,ndays))
      allocate(output_file%snow_tmp(nx,ny,ndays))
      allocate(output_file%rain_tmp(nx,ny,ndays))
      allocate(output_file%melt_tmp(nx,ny,ndays))
      allocate(output_file%refreezing_tmp(nx,ny,ndays))
      allocate(output_file%runoff_tmp(nx,ny,ndays))
!       allocate(output_file%snow_mask_tmp(nx,ny,ndays))
      allocate(output_file%melt_ice_tmp(nx,ny,ndays))
      allocate(output_file%snow_srftemp_tmp(nx,ny,ndays))
      allocate(output_file%real_mass_balance_tmp(nx,ny,ndays))
      allocate(output_file%regridding_tmp(nx,ny,ndays))
    end if

    ! Initialize all temporary variables with zeros:
    call output_file%reset_data()
  end subroutine init_npz


! ################################################
  subroutine reset_tmp_npz(output_file)
    implicit none
    class(npz_file), intent(inout) :: output_file

    if (output_file%save_3D) then
      output_file%snowmass_tmp          = 0e0
      output_file%lwmass_tmp            = 0e0
      output_file%rho_snow_tmp          = 0e0
      output_file%snow_temp_tmp         = 0e0
    end if
    output_file%albedo_tmp              = 0e0
    output_file%vaporflux_tmp           = 0e0
    output_file%accum_tmp               = 0e0
    output_file%snow_tmp                = 0e0
    output_file%rain_tmp                = 0e0
    output_file%melt_tmp                = 0e0
    output_file%refreezing_tmp          = 0e0
    output_file%runoff_tmp              = 0e0
    output_file%melt_ice_tmp            = 0e0
    output_file%snow_srftemp_tmp        = 0e0
    output_file%real_mass_balance_tmp   = 0e0
!     output_file%snow_mask_tmp           = 0
    output_file%regridding_tmp          = 0
    if (output_file%outp_type == 'ANNUAL') then
      output_file%runcell_tmp           = 0
      output_file%mask_tmp              = -1
    end if
!     if (output_file%outp_type == 'DAILY_') then
!       output_file%mask_tmp          = 0
!     end if
  end subroutine reset_tmp_npz


! ################################################
  subroutine store_vars_npz(output_file, ix, iy, time, accum, vaporflux, mass0, runoff, refreeze, melt, melt_ice, regrid, rain_ice, smb_ice, rain, albedo, snow_srftemp, mask, run_cell)
    ! Store annual or daily data
    ! Annual data is annual averages for all 2D fields and end-of-year snapshots for 3D fields

    use bessi_defs
    implicit none
    class(npz_file),intent(inout) :: output_file
    integer,        intent(in)    :: ix
    integer,        intent(in)    :: iy
    integer,        intent(in)    :: time
    real(kind=8),   intent(in)    :: accum
    real(kind=8),   intent(in)    :: vaporflux
    real(kind=8),   intent(in)    :: mass0
    real(kind=8),   intent(in)    :: runoff
    real(kind=8),   intent(in)    :: refreeze
    real(kind=8),   intent(in)    :: melt
    real(kind=8),   intent(in)    :: melt_ice
    real(kind=8),   intent(in)    :: rain_ice
    real(kind=8),   intent(in)    :: rain
    real(kind=8),   intent(in)    :: smb_ice
    real(kind=8),   intent(in)    :: albedo
    real(kind=8),   intent(in)    :: snow_srftemp
    integer,        intent(in)    :: regrid
    integer, intent(in), optional :: mask
    logical, intent(in), optional :: run_cell

    if (output_file%outp_type == 'ANNUAL') then
      output_file%snow_tmp(ix,iy,1)          = output_file%snow_tmp(ix,iy,1)         + real(accum*dt_firn)/ndays
      output_file%rain_tmp(ix,iy,1)          = output_file%rain_tmp(ix,iy,1)         + real(rain*dt_firn)/ndays   !rain on ice?
      output_file%vaporflux_tmp(ix,iy,1)     = output_file%vaporflux_tmp(ix,iy,1)    + real(vaporflux*dt_firn/(L_v + L_lh))/ndays
      output_file%runoff_tmp(ix,iy,1)        = output_file%runoff_tmp(ix,iy,1)       + real(runoff)/ndays
      output_file%refreezing_tmp(ix,iy,1)    = output_file%refreezing_tmp(ix,iy,1)   + real(refreeze)/ndays
      output_file%melt_tmp(ix,iy,1)          = output_file%melt_tmp(ix,iy,1)         + real(melt)/ndays !melt on ice
      output_file%melt_ice_tmp(ix,iy,1)      = output_file%melt_ice_tmp(ix,iy,1)     + real(melt_ice)/ndays
      output_file%albedo_tmp(ix,iy,1)        = output_file%albedo_tmp(ix,iy,1)       + real(albedo)/ndays
      output_file%accum_tmp(ix,iy,1)         = output_file%accum_tmp(ix,iy,1)        + real(smb_ice*rho_i*seconds_per_year)
      output_file%snow_srftemp_tmp(ix,iy,1)  = output_file%snow_srftemp_tmp(ix,iy,1) + real(snow_srftemp)/ndays
      output_file%regridding_tmp(ix,iy,1)    = output_file%regridding_tmp(ix,iy,1)   + regrid/ndays

      ! Only for the first time step:
      if (time == 1) then
        if (run_cell) then
          output_file%runcell_tmp(ix,iy) = 1
        else
          output_file%runcell_tmp(ix,iy) = 0
        end if
        output_file%mask_tmp(ix,iy) = mask
      end if

      ! Only for the last time step (snapshots):
      if (time == ndays) then
        if (output_file%save_3D) then
          output_file%snowmass_tmp(ix,iy,:,1)      = real(snowmass(ix,iy,:))
          output_file%lwmass_tmp(ix,iy,:,1)        = real(lwmass(ix,iy,:))
          output_file%rho_snow_tmp(ix,iy,:,1)      = real(rho_snow(ix,iy,:))
          output_file%snow_temp_tmp(ix,iy,:,1)     = real(snow_temp(ix,iy,:))
        end if
        output_file%real_mass_balance_tmp(ix,iy,1) = real(sum(snowmass(ix,iy,:)) - mass0 - output_file%melt_ice_tmp(ix,iy,1))
      end if

      if (include_values_over_ice) then
        output_file%runoff_tmp(ix,iy,1)     = output_file%runoff_tmp(ix,iy,1)       + real(melt_ice)/ndays
        output_file%rain_tmp(ix,iy,1)       = output_file%rain_tmp(ix,iy,1)         + real(rain_ice*dt_firn)/ndays
      end if

!       if (snowmass(ix,iy,1) .gt. 0.) output_file%snow_mask_tmp(ix,iy,1) = output_file%snow_mask_tmp(ix,iy,1) + 1 ! Unit: number of days with snow cover
    elseif (output_file%outp_type == 'DAILY_') then
      if (output_file%save_3D) then
        output_file%snowmass_tmp(ix,iy,:,time)  = real(snowmass(ix,iy,:))
        output_file%lwmass_tmp(ix,iy,:,time)    = real(lwmass(ix,iy,:))
        output_file%rho_snow_tmp(ix,iy,:,time)  = real(rho_snow(ix,iy,:))
        output_file%snow_temp_tmp(ix,iy,:,time) = real(snow_temp(ix,iy,:))
      end if
      output_file%albedo_tmp(ix,iy,time)        = real(albedo)
      output_file%vaporflux_tmp(ix,iy,time)     = real(vaporflux)
      output_file%accum_tmp(ix,iy,time)         = real(smb_ice*rho_i*seconds_per_year) !ABo: seconds_per_year for daily data?
      output_file%snow_tmp(ix,iy,time)          = real(accum*dt_firn)
      output_file%rain_tmp(ix,iy,time)          = real(rain*dt_firn)
      output_file%refreezing_tmp(ix,iy,time)    = real(refreeze)
      output_file%melt_tmp(ix,iy,time)          = real(melt)
      output_file%runoff_tmp(ix,iy,time)        = real(runoff)
      output_file%melt_ice_tmp(ix,iy,time)      = real(melt_ice)
!       output_file%mask_tmp(ix,iy,time)          = mask
      ! Extra for wet snow else useless as snowmass, in general not very useful
!       if (snowmass(ix, iy, 1) .gt. 0d0) output_file%snow_mask_tmp(ix,iy,time) = 1

      ! If there is no snow at the current location, set all temperatures to the freezing point. ABo: Maybe better to use more obvious value, e.g., -999?
!       if (snow_temp(ix,iy,:) == 0d0) output_file%snow_temp_tmp(ix,iy,1,time) = 273.15
      ! Extra for wet snow else useless as snowmass, in general not very useful:
!       if (snowmass(ix, iy, 1) .gt. 0.) output_file%snow_mask_tmp(ix,iy,time) = 1

      if (time .eq. 1) then
        output_file%real_mass_balance_tmp(ix,iy,time) = real(sum(snowmass(ix, iy, :))) - mass0 - real(melt_ice)
      else
        ! ABo: This will not work if output_file%save_3D = .true. because snowmass_tmp is not allocated. Needs work.
        if (output_file%save_3D) then
          output_file%real_mass_balance_tmp(ix,iy,time) = real(sum(snowmass(ix, iy, :))) - sum(output_file%snowmass_tmp(ix,iy,:,time-1)) - real(melt_ice)
        end if
      end if

      output_file%regridding_tmp(ix,iy,time)   = regrid
      output_file%snow_srftemp_tmp(ix,iy,time) = real(snow_srftemp)
            
      if (include_values_over_ice) then
        output_file%runoff_tmp(ix,iy,time)     = output_file%runoff_tmp(ix,iy,time) + real(melt_ice)
        output_file%rain_tmp(ix,iy,time)       = output_file%rain_tmp(ix,iy,time)   + real(rain_ice*dt_firn)
      end if
    end if

  end subroutine store_vars_npz


! ################################################
  subroutine add_smbice_end_of_year(output_file, ix, iy, smb_ice)
    ! Add smb_ice from smb_point(), because it's outside the time loop and not included in store_vars_npz.

    implicit none
    class(npz_file),intent(inout) :: output_file
    integer,        intent(in)    :: ix
    integer,        intent(in)    :: iy
    real(kind=8),   intent(in)    :: smb_ice

    if (output_file%outp_type == 'ANNUAL') then
      output_file%accum_tmp(ix,iy,1)     = output_file%accum_tmp(ix,iy,1)     + real(smb_ice*rho_i*seconds_per_year)
    elseif (output_file%outp_type == 'DAILY_') then
      output_file%accum_tmp(ix,iy,ndays) = output_file%accum_tmp(ix,iy,ndays) + real(smb_ice*rho_i*seconds_per_year) !ABo: seconds_per_year for daily data?
    end if

  end subroutine add_smbice_end_of_year


! ################################################
  subroutine write_npz(output_file)
    ! Write data to disk, at the end of the year.

    ! TODO: Variable "accumulation" (accum_tmp, smb_ice) is incorrect in output, because it does not take
    ! into consideration the mass change by smb_point outside the time loop in smb_emb.
    implicit none
    class(npz_file), intent(inout)     :: output_file
    character(len=14) :: timestamp_string
    character(len=128) :: directory_name
    integer,dimension(8) :: values

    if (output_file%outp_type == 'ANNUAL') then
      if (output_file%save_3D) then
        call add_npz(output_file%filepath, 'snow_mass',           output_file%snowmass_tmp(:,:,:,1))
        call add_npz(output_file%filepath, 'liquid_water_mass',   output_file%lwmass_tmp(:,:,:,1))
        call add_npz(output_file%filepath, 'snow_density',        output_file%rho_snow_tmp(:,:,:,1))
        call add_npz(output_file%filepath, 'snow_temperature',    output_file%snow_temp_tmp(:,:,:,1))
      end if
      call add_npz(output_file%filepath, 'albedo',              output_file%albedo_tmp(:,:,1))
      call add_npz(output_file%filepath, 'vapor_flux_?',        output_file%vaporflux_tmp(:,:,1))
      call add_npz(output_file%filepath, 'accumulation',        output_file%accum_tmp(:,:,1))
      call add_npz(output_file%filepath, 'rain',                output_file%rain_tmp(:,:,1))
      call add_npz(output_file%filepath, 'snowfall',            output_file%snow_tmp(:,:,1))
      call add_npz(output_file%filepath, 'melt',                output_file%melt_tmp(:,:,1))
      call add_npz(output_file%filepath, 'refreezing',          output_file%refreezing_tmp(:,:,1))
      call add_npz(output_file%filepath, 'runoff',              output_file%runoff_tmp(:,:,1))
      call add_npz(output_file%filepath, 'melt_ice',            output_file%melt_ice_tmp(:,:,1))
      call add_npz(output_file%filepath, 'surface_temperature', output_file%snow_srftemp_tmp(:,:,1))
      call add_npz(output_file%filepath, 'real_mass_balance_?', output_file%real_mass_balance_tmp(:,:,1))
!       call add_npz(output_file%filepath, 'snow_mask_?',         output_file%snow_mask_tmp(:,:,1))
      call add_npz(output_file%filepath, 'regridding_count',    output_file%regridding_tmp(:,:,1))
      call add_npz(output_file%filepath, 'run_cell_this_year',  output_file%runcell_tmp)
      call add_npz(output_file%filepath, 'landmask',            output_file%mask_tmp)

    elseif (output_file%outp_type == 'DAILY_') then
      ! This is a two-stage process. First, all fields are stored as .npy files. A temporary directory is created for this
      ! purpose. After that, all .npy files are zipped, with compression, into a .npz file. This is done by a bash script
      ! so that it can run in the background while BESSI continues. A custom version of the script is created on the fly.
      ! The temporary directory is removed at the end.
      call date_and_time(VALUES=values)
      write(timestamp_string, '(I4.4, I2.2, I2.2, I2.2, I2.2, I2.2)') values(1), values(2), values(3), values(5), values(6), values(7)
      directory_name = TRIM(adjustl(TRIM(adjustl(output_directory)) // 'tmp_' // timestamp_string // '/'))
      call system('mkdir ' // directory_name)
      call chdir(directory_name)

      ! Inject a bash script into tmp directory to generate .npz file.
      ! This can be decoupled from the run itself and run in the background to save time.
      open (unit=10,file="bessi_npz.sh",action="write")
      write (10,'(A8,A14,A10)') 'zip -q3 ', timestamp_string, '.npz *.npy'
      write (10,'(A3,A14,A8,A)') 'mv ', timestamp_string, '.npz ../', output_file%filename
      write (10,'(A)') 'cd ../..'
      write (10,'(A,A)') 'rm -r ', TRIM(adjustl(directory_name))
      close (10)

      if (output_file%save_3D) then
        call save_npy('snow_mass.npy',           output_file%snowmass_tmp)
        call save_npy('liquid_water_mass.npy',   output_file%lwmass_tmp)
        call save_npy('snow_density.npy',        output_file%rho_snow_tmp)
        call save_npy('snow_temperature.npy',    output_file%snow_temp_tmp)
      end if
      call save_npy('albedo.npy',              output_file%albedo_tmp)
      call save_npy('vapor_flux_?.npy',        output_file%vaporflux_tmp)
      call save_npy('acccumulation.npy',       output_file%accum_tmp)
      call save_npy('rain.npy',                output_file%rain_tmp)
      call save_npy('snowfall.npy',            output_file%snow_tmp)
      call save_npy('melt.npy',                output_file%melt_tmp)
      call save_npy('refreezing.npy',          output_file%refreezing_tmp)
      call save_npy('runoff.npy',              output_file%runoff_tmp)
      call save_npy('melt_ice.npy',            output_file%melt_ice_tmp)
      call save_npy('surface_temperature.npy', output_file%snow_srftemp_tmp)
      call save_npy('real_mass_balance_?.npy', output_file%real_mass_balance_tmp)
!       call save_npy('snow_mask_?.npy',         output_file%snow_mask_tmp)
      call save_npy('regridding_count.npy',    output_file%regridding_tmp)
!       call save_npy('landmask.npy',            output_file%mask_tmp)
      ! Run bash script:
      call execute_command_line('chmod 755 bessi_npz.sh', wait=.True.)
      call execute_command_line('./bessi_npz.sh', wait=.False.)
      call chdir('../..')

    end if

    if (debug .ge. 2) write(*,'("Output written to file: ", A)') output_file%filename
    call output_file%clear

  end subroutine write_npz

!     subroutine write_check_mass(year)
!         ! Create a txt file with all mass fluxes
! 
!         use bessi_defs
!         use conservation
!         implicit none
! 
!         integer, intent(in) :: year
!         integer :: ii
! 
!         if(mass_checking) then
!             if (year == 1) then
!                 open(42, file=trim(adjustl(output_directory)) // 'diagnosed_massflux_Snowman3D.txt', &
!                          status="new", action="write")
!                 write(42,*) 'This file contains the total mass of water and snow as well as massfluxes of snow and water '
!                 write(42,*) 'inside the model Snoman3D. Total masses are in kg and the fluxes are in kg per timestep.'
!                 write(42,*) 'initial mass snow', tab, 'end mass snow', tab,'initial mass water', tab, &
!                             'end mass water', tab, 'accumulation snow', tab, 'accumulation water', tab, 'snow melt', &
!                             tab, 'water runoff', tab, 'refreeze', tab, 'snow to ice model'
!             else
!                 open(42, file=trim(adjustl(output_directory)) // 'diagnosed_massflux_Snowman3D.txt',&
!                          status="old", position="append", action="write")
!             end if
! 
!             check_init_mass_snow = check_init_mass_snow * dx * dy
!             check_end_mass_snow = check_end_mass_snow * dx * dy
! 
!             check_init_mass_water = check_init_mass_water * dx * dy
!             check_end_mass_water = check_end_mass_water * dx * dy
! 
!             check_accum_snow  = check_accum_snow * dx * dy
!             check_accum_water = check_accum_water * dx * dy
! 
!             check_melted_snow = check_melted_snow * dx * dy
!             check_runoff_water = check_runoff_water * dx * dy
!             check_refreeze_water = check_refreeze_water * dx * dy
!             check_ice2ice = check_ice2ice * dx * dy
! 
!             ! write fluxes to txt file
!             do ii = 1, ndays, 1
!                 write(42,*) check_init_mass_snow(ii), tab, check_end_mass_snow(ii), tab, check_init_mass_water(ii), tab, &
!                      check_end_mass_water(ii), tab, check_accum_snow(ii), tab, check_accum_water(ii), tab, &
!                     check_melted_snow(ii), tab, check_runoff_water(ii), tab, check_refreeze_water(ii), tab, check_ice2ice(ii)
!             end do
! 
!             close(42)
!         end if
!     end subroutine write_check_mass
! 
!     subroutine write_check_energy(year)
!         ! Creating a txt file with all energy fluxes
!         
!         use bessi_defs
!         use conservation
!         implicit none
! 
!         integer, intent(in) :: year
!         integer :: ii
! 
!         if(energy_checking) then
!             if (year == 1) then
!                 open(420, file=trim(adjustl(output_directory)) // 'diagnosed_energyflux_Snowman3D.txt',&
!                          status="new", action="write")
!                 write(420,*) 'This file contains the total energy stored in water and snow as well as energyfluxes into and out '
!                 write(420,*) 'of the model Snoman3D. Total energies are in J and the fluxes are in J per timestep.'
!                 write(420,*) 'initial energy snow', tab, 'end energy snow', tab, 'initial energy water', tab, 'end energy water', &
!                                 tab, 'obs. surface energy flux', tab, 'diag. surface energy flux', tab, 'accum tot energy flux', &
!                                 tab, 'energy runoff by percolation', tab, 'energy passed to ice', tab, 'freezing energy diag', &
!                                 tab, 'freezing energy heat', tab, 'melting energi diag', tab, 'melting energy heat', tab, &
!                                 'Energy entering snow for melt', tab, 'energy runoff by melt-up'
!             else
!                 open(420, file=trim(adjustl(output_directory)) // 'diagnosed_energyflux_Snowman3D.txt',&
!                          status="old", position="append", action="write")
!             end if
! 
!             check_init_energy_snow = check_init_energy_snow * dx * dy 
!             check_init_energy_water = check_init_energy_water * dx * dy 
!             check_end_energy_snow = check_end_energy_snow * dx * dy 
!             check_end_energy_water = check_end_energy_water * dx * dy 
!             check_surface_e_flux_obs = check_surface_e_flux_obs * dx * dy 
!             check_surface_e_flux_diag = check_surface_e_flux_diag * dx * dy 
!             check_e_accum_tot = check_e_accum_tot * dx * dy 
! 
!             check_e_freeze_tot  = check_e_freeze_tot * dx * dy 
!             check_e_freeze_heat = check_e_freeze_heat * dx * dy 
! 
!             check_e_perc_runoff = check_e_perc_runoff * dx * dy 
!             check_e_ice2ice = check_e_ice2ice * dx * dy
! 
!             check_e_melt_tot = check_e_melt_tot * dx * dy 
!             check_e_melt_heat = check_e_melt_heat * dx * dy 
!             check_e_melt_qq = check_e_melt_qq * dx * dy 
!             check_e_melt_runoff = check_e_melt_runoff * dx * dy 
! 
!             ! write fluxes to txt file
!             do ii=1,ndays,1
!                 write(420,*) check_init_energy_snow(ii), tab, check_end_energy_snow(ii), tab, check_init_energy_water(ii), tab, &
!                                 check_end_energy_water(ii), tab,check_surface_e_flux_obs(ii), tab, check_surface_e_flux_diag(ii), &
!                                 tab, check_e_accum_tot(ii), tab, check_e_perc_runoff(ii), tab, check_e_ice2ice(ii), tab, &
!                                 check_e_freeze_tot(ii), tab, check_e_freeze_heat(ii), tab, check_e_melt_tot(ii), tab, &
!                                 check_e_melt_heat(ii), tab, check_e_melt_qq(ii), tab, check_e_melt_runoff(ii)
!             end do
! 
!             close(420)
!         end if
!     end subroutine write_check_energy

end module bessi_data
