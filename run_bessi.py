import subprocess
import os
import sys
import shutil
from datetime import datetime


def get_from_defs(filename, parameter):
  def_file = open(filename, 'r')
  lines = def_file.readlines()
  for row in lines:
    if (row.find(parameter) != -1) and (row.find(' = ') != -1):
      path = row.split()[-1]
      return path
  # Routine only gets to the following code if none of the lines in the loop returned a positive match:
  print('### ERROR: Did not find output directory.')
  sys.exit()


#######################################################
source_dir           = 'src' # best defined with relative path
compile_only         = False
hide_compiler_output = False
hide_runtime_output  = False
niceness_level       = 10
######################################################


# Create a timestamp for the output directory:
now         = datetime.now()
timestamp   = now.strftime("%Y%m%d_%H%M%S")                                                                                                                                                                                                                                                                                                                

# Read output directory from bessi_defs:
output_path = get_from_defs(source_dir+'/bessi_defs.f90','output_directory')
run_name    = get_from_defs(source_dir+'/bessi_defs.f90','run_name')
output_dir  = output_path[1:-1]+run_name[1:-1]+'_'+timestamp

print('New output directory: ' + output_dir)
os.mkdir(output_dir)
shutil.copytree(source_dir,output_dir+'/'+source_dir)
log_file    = open(output_dir+'/BESSI_out.log', 'w')

# Compile:
if hide_compiler_output:
  process = subprocess.run(['make', 'bessi-single'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
else:
  print('##############################################')
  print('### COMPILER OUTPUT ##########################')
  print('\u2B07 '*23)
  print(' ')
  process = subprocess.run(['make', 'bessi-single'])
  print(' ')
  print('\u2B06 '*23)
  print('### COMPILER OUTPUT ##########################')
  print('##############################################')

# Check for errors before next step:
if (process.returncode != 0):
  print('### ERROR: Did not compile.')
  sys.exit()

shutil.copy('bin/bessi.x',output_dir+'/.')

if compile_only:
  print('Compile only, done.')
else:
  os.chdir(output_dir)
  if hide_runtime_output:
    subprocess.run('./bessi.x', preexec_fn=lambda : os.nice(niceness_level), stdout=log_file, stderr=log_file, text=True)
  else:
    subprocess.run('./bessi.x', preexec_fn=lambda : os.nice(niceness_level), text=True)
