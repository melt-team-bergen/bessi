import numpy as np
import matplotlib as ml
from scipy import interpolate
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib import colors as mcolors
from netCDF4 import Dataset
import os
import glob

def get_smb_colormap(cmap_name='RdBu',vmin=None,vmax=None,segments=10):
  # Define SMB colormap:
  midpoint = 0
  # asymmtric ranges
  neg_range  = np.linspace(vmin, midpoint, segments // 2 + 1)
  pos_range  = np.linspace(midpoint, vmax, segments // 2 + 1)
  boundaries = np.concatenate((neg_range, pos_range[1:]))
 
  smb_cmap = plt.get_cmap(cmap_name, segments)
  smb_norm = mcolors.BoundaryNorm(boundaries, smb_cmap.N)
 
  ## Mask zero values to display them in white if needed
  #if mask_zeros:
    #data = np.ma.masked_where(data == 0, data)
    #smb_cmap.set_bad(color='white')

  return smb_cmap, smb_norm


def gr_plot(array_in,cb_min=np.nan,cb_max=np.nan):
  plt.clf()
  if cb_min==np.nan or cb_max==np.nan:
    # If color bar limits are not defined, use default normalization:
    plt.imshow(array_in.T,origin='lower')
  else:
    plt.imshow(array_in.T,origin='lower',vmin=cb_min,vmax=cb_max)

  plt.colorbar()
  return


class bessi_run():
  """
  This class represents a set of operations and visualizations for analyzing and plotting
  simulation output data from the BESSI model.

  Args:
      output_dir (str): The directory where the simulation output files are located.
      run_name (str): The name of the simulation run.

  Attributes:
      run_name (str): The name of the simulation run.
      path (str): The path to the directory containing simulation output files.

  Methods:
      list_vars(outp_freq, year):
          Lists the variables available in a specific output file.
      load_nc(outp_freq, year, varname):
          Loads a specific variable from a NetCDF file.
      quick_check(year):
          Generates a quick visual check of snow mass at the end of a specific year.
      overview(year):
          Generates an overview plot of various snow-related variables for a specific year.
  """
# ===========================================
  def __init__(self,output_dir,run_name):
    """
      Initializes a bessi_run instance.

      Args:
          output_dir (str): The directory where the simulation output files are located.
          run_name (str): The name of the simulation run.
    """
    self.run_name   = run_name
    self.path       = glob.glob(output_dir + '/' + run_name+'*')[0] + '/output/'
    if not os.path.isdir(self.path):
      print(self.path + ' does not exist.')

    return

# ===========================================
  def list_vars(self,outp_freq,year):        
    """
    Lists the variables available in a specific output file.

    Args:
        outp_freq (str): Output frequency, e.g., 'ANNUAL', 'DAILY'.
        year (int): The year for which to list variables.
    """
    filename = self.path + str(outp_freq).ljust(7,'_') + '.' + str(year).rjust(7,'0') + '.nc'
    ncfile   = Dataset(filename, 'r')
    print('Variables in ' + filename + ':')
    print(list(ncfile.variables.keys()))
    ncfile.close()
    return

# ===========================================
  def load_nc(self,outp_freq,year,varname):
    """
    Loads a specific variable from a NetCDF file.

    Args:
        outp_freq (str): Output frequency, e.g., 'ANNUAL', 'DAILY'.
        year (int): The year for which to load the variable.
        varname (str): The name of the variable to load.

    Returns:
        numpy.ndarray: Loaded variable data.
    """   
    filename = self.path + str(outp_freq).ljust(7,'_') + '.' + str(year).rjust(7,'0') + '.nc'
    ncfile   = Dataset(filename, 'r')
    var      = np.squeeze(ncfile.variables[varname][:].copy())
    ncfile.close()
    return var

# ===========================================
  def load_npz(self,outp_freq,year):
    filename = self.path + str(outp_freq).ljust(6,'_') + '.' + str(year).rjust(7,'0') + '.npz'
    npzfile  = np.load(filename)
    return npzfile

# ===========================================
  def quick_check(self,year):
    """
    Generates a quick visual check of snow mass at the end of a specific year.

    Args:
        year (int): The year for which to generate the quick check.
    """   
    snowmass_a = self.load_nc('ANNUAL',year,'snowmass')
    snowmass_d = self.load_nc('DAILY',year,'snowmass')

    plt.figure(1)
    plt.clf()
    plt.subplot(1,2,1)
    plot_var(np.sum(snowmass_a,0),[6e3,8e3])
    plt.title('snow mass, end of year')
    plt.subplot(1,2,2)
    plt.plot(snowmass_d[80,150,0,:],'k')
    plt.title('snow mass at [80,150]')
    return

# ===========================================
  def overview(self,year):
    """
    Generates an overview plot of various snow-related variables for a specific year.

    Args:
        year (int): The year for which to generate the overview plot.
    """

    snowmass_a = self.load_nc('ANNUAL',year,'snowmass')
    snowtemp_a = self.load_nc('ANNUAL',year,'snowtemp')-273.15
    snowdens_a = self.load_nc('ANNUAL',year,'snowdensity')
    lwmass_a   = self.load_nc('ANNUAL',year,'lwmass')
    albedo_a   = self.load_nc('ANNUAL',year,'albedo')
    snow_a     = self.load_nc('ANNUAL',year,'snow')
    rain_a     = self.load_nc('ANNUAL',year,'rain')
    melt_a     = self.load_nc('ANNUAL',year,'melt')
    runoff_a   = self.load_nc('ANNUAL',year,'runoff')
    smb_a      = self.load_nc('ANNUAL',year,'mass_balance')
    snowmass_d = self.load_nc('DAILY', year,'snowmass')
    snowtemp_d = self.load_nc('DAILY', year,'snowtemp')-273.15
    snowdens_d = self.load_nc('DAILY', year,'snowdensity')
    lwmass_d   = self.load_nc('DAILY', year,'lwmass')
    albedo_d   = self.load_nc('DAILY', year,'albedo')
    snow_d     = self.load_nc('DAILY', year,'snow')
    rain_d     = self.load_nc('DAILY', year,'rain')
    melt_d     = self.load_nc('DAILY', year,'melt')
    runoff_d   = self.load_nc('DAILY', year,'runoff')
    smb_d      = self.load_nc('DAILY', year,'mass_balance')

    plt.figure(1)
    plt.clf()
    plt.subplot(2,5,1)
    plot_var(snowmass_a[0,:,:],[0,350])
    plt.title('snowmass')
    plt.subplot(2,5,2)
    plot_var(snowtemp_a[0,:,:],[-50,0])
    plt.title('snow temperature')
    plt.subplot(2,5,3)
    plot_var(snowdens_a[0,:,:],[300,450])
    plt.title('snow density')
    plt.subplot(2,5,4)
    plot_var(lwmass_a[0,:,:],[0,50])
    plt.title('liquid water mass')
    plt.subplot(2,5,5)
    plot_var(albedo_a,[0.3,1])
    plt.title('albedo')
    plt.subplot(2,5,6)
    plot_var(snow_a,[0,5])
    plt.title('snow')
    plt.subplot(2,5,7)
    plot_var(runoff_a,[0,5])
    plt.title('runoff')
    plt.subplot(2,5,8)
    plot_var(rain_a,[0,.5])
    plt.title('rain')
    plt.subplot(2,5,9)
    plot_var(melt_a,[0,1])
    plt.title('melt')
    plt.subplot(2,5,10)
    plot_var(smb_a,[-500,500],colmap='bwr')
    plt.title('mass balance')

    plt.figure(2)
    plt.clf()
    plt.subplot(2,5,1)
    plot_var(snowmass_d[:,:,0,0],[0,350])
    plt.title('snowmass')
    plt.subplot(2,5,2)
    plot_var(snowtemp_d[:,:,0,0],[-50,0])
    plt.title('snow temperature')
    plt.subplot(2,5,3)
    plot_var(snowdens_d[:,:,0,0],[300,450])
    plt.title('snow density')
    plt.subplot(2,5,4)
    plot_var(lwmass_d[:,:,0,0],[0,100])
    plt.title('liquid water mass')
    plt.subplot(2,5,5)
    plot_var(albedo_d[:,:,0],[0.3,1])
    plt.title('albedo')
    plt.subplot(2,5,6)
    plot_var(snow_d[:,:,0],[0,5])
    plt.title('snow')
    plt.subplot(2,5,7)
    plot_var(runoff_d[:,:,0],[0,5])
    plt.title('runoff')
    plt.subplot(2,5,8)
    plot_var(rain_d[:,:,0],[0,.5])
    plt.title('rain')
    plt.subplot(2,5,9)
    plot_var(melt_d[:,:,0],[0,1])
    plt.title('melt')
    plt.subplot(2,5,10)
    plot_var(smb_d[:,:,0],[-50,50],colmap='bwr')
    plt.title('mass balance')
    return

# ===========================================
# ===========================================
class input_data():
  """
  This class provides methods to access and load input data variables from NetCDF files.

  Args:
      filepath (str): The directory where the input data files are located.
      name (str): The name of the input data file.

  Attributes:
      path (str): The path to the input data file.

  Methods:
      list_vars():
          Lists the variables available in the input data file.
      load_nc(varname):
          Loads a specific variable from the input data NetCDF file.
  """
# ===========================================
  def __init__(self,filepath,name):
    """
    Initializes an input_data instance.

    Args:
         filepath (str): The directory where the input data files are located.
         name (str): The name of the input data file.
    """
    self.path  = glob.glob(filepath + '/' + name+'*')[0]
    if not os.path.isfile(self.path):
      print(self.path + ' does not exist.')
    else:
      ncfile   = Dataset(self.path, 'r')
      print('Variables in ' + self.path + ':')
      print(list(ncfile.variables.keys()))
      ncfile.close()
    return

# ===========================================
  def list_vars(self):        
    """
    Lists the variables available in the input data file.
    """
    ncfile   = Dataset(self.path, 'r')
    print('Variables in ' + self.path + ':')
    print(list(ncfile.variables.keys()))
    ncfile.close()
    return

# ===========================================
  def load_nc(self,varname):
    """
    Loads a specific variable from the input data NetCDF file.

    Args:
      varname (str): The name of the variable to load.

    Returns:
        numpy.ndarray: Loaded variable data.
    """
    ncfile   = Dataset(self.path, 'r')
    var      = np.squeeze(ncfile.variables[varname][:].copy())
    ncfile.close()
    return var

# ===========================================
# ===========================================
def plot_var(var,bounds,colmap='inferno'):    
  """
  Plots a 2D variable using imshow.

  Args:
      var (numpy.ndarray): 2D variable data.
      bounds (list): A list containing [min_value, max_value] for colormap scaling.
      colmap (str, optional): Colormap for visualization. Defaults to 'inferno'.
  """
  plt.imshow(var.T,origin='lower',vmin=bounds[0],vmax=bounds[1],cmap=colmap)
  plt.colorbar()
  return
