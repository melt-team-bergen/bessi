# nc stuff: 
# rearranging variables to a new output file and 
#cd modeloutput/
for run in 221
do 
run_zeros=$(printf "%05d" $run)
#cd ensemble_m_${run_zeros}*
type=MONTHLY
pwd
type=DAILY
for number in {1..2}
do
leading_zeros=$(printf "%07d" $number)

inputname=${type}_$leading_zeros.nc
outputname=${type}_${run_zeros}_$leading_zeros.nc
echo "starting $leading_zeros.nc"
ncpdq -a time,layer,y,x $inputname itm${type}.nc
ncks --mk_rec_dmn time itm${type}.nc $outputname
rm itm${type}.nc
echo "finished $leading_zeros.nc"
rm $inputname
done

#cd ..
done


#potentially concentenating them 
ncrcat -F -n 4,7,1 out_0000011.nc test.nc


for year in {1979..2017}
do
size=3500
mv snowtemp_${size}_000${year}_ANNUAL.nc snowtemp_${size}_000${year}.nc
mv snowmass_${size}_000${year}_ANNUAL.nc snowmass_${size}_000${year}.nc
mv snowdensity_${size}_000${year}_ANNUAL.nc snowdensity_${size}_000${year}.nc
mv snow_${size}_000${year}_ANNUAL.nc snow_${size}_000${year}.nc
mv runoff_${size}_000${year}_ANNUAL.nc runoff_${size}_000${year}.nc
mv melt_${size}_000${year}_ANNUAL.nc melt_${size}_000${year}.nc
mv mass_balance_${size}_000${year}_ANNUAL.nc mass_balance_${size}_000${year}.nc
mv averaged_surface_temp_${size}_000${year}_ANNUAL.nc averaged_surface_temp_${size}_000${year}.nc
mv melt_of_ice_${size}_000${year}_ANNUAL.nc melt_of_ice_${size}_000${year}.nc

done

for year in {1979..2017}
do
mv albedo_${size}_000${year}_ANNUAL.nc albedo_${size}_000${year}.nc
done

for year in {1979..2017}
do
ncecat -O snowtemp_${size}_000${year}.nc ${year}.nc
done
ncrcat -F -n 38,4,1 1979.nc snowtemp_${size}.nc

for year in {1979..2017}
do
ncecat -O snowmass_${size}_000${year}.nc ${year}.nc
done
ncrcat -F -n 38,4,1 1979.nc snowmass_${size}.nc

for year in {1979..2017}
do
ncecat -O snow_${size}_000${year}.nc ${year}.nc
done
ncrcat -F -n 38,4,1 1979.nc snow_${size}.nc

for year in {1979..2017}
do
ncecat -O snowdensity_${size}_000${year}.nc ${year}.nc
done
ncrcat -F -n 38,4,1 1979.nc snowdensity_${size}.nc

for year in {1979..2017}
do
ncecat -O melt_${size}_000${year}.nc ${year}.nc
done
ncrcat -F -n 38,4,1 1979.nc melt_${size}.nc

for year in {1979..2017}
do
ncecat -O runoff_${size}_000${year}.nc ${year}.nc
done
ncrcat -F -n 38,4,1 1979.nc runoff_${size}.nc

for year in {1979..2017}
do
ncecat -O mass_balance_${size}_000${year}.nc ${year}.nc
done
ncrcat -F -n 38,4,1 1979.nc mass_balance_${size}.nc

for year in {1979..2017}
do
ncecat -O albedo_${size}_000${year}.nc ${year}.nc
done
ncrcat -F -n 38,4,1 1979.nc albedo_${size}.nc

for year in {1979..2017}
do
ncecat -O averaged_surface_temp_${size}_000${year}.nc ${year}.nc
done
ncrcat -F -n 38,4,1 1979.nc averaged_surface_temp_${size}.nc

for year in {1979..2017}
do
ncecat -O melt_of_ice_${size}_000${year}.nc ${year}.nc
done
ncrcat -F -n 38,4,1 1979.nc melt_of_ice_${size}.nc

ncra averaged_surface_temp_${size}.nc averaged_surface_temp.nc
ncra mass_balance_${size}.nc mass_balance.nc
ncra albedo_${size}.nc albedo.nc
ncra snowtemp_${size}.nc snowtemp.nc
ncra snow_${size}.nc snow.nc
ncra snowmass_${size}.nc snowmass.nc
ncra snowdensity_${size}.nc snowdensity.nc
ncra melt_of_ice_${size}.nc melt_of_ice.nc
ncra runoff_${size}.nc runoff.nc


for year in {1979..2017}
do
size=3500
mv latent_heat_flux_${size}_000${year}_ANNUAL.nc latent_heat_flux_${size}_000${year}.nc
done

for year in {1979..2017}
do
ncecat -O latent_heat_flux_${size}_000${year}.nc ${year}.nc
done
ncrcat -F -n 38,4,1 1979.nc latent_heat_flux_${size}.nc
ncra latent_heat_flux_${size}.nc latent_heat_flux.nc
