# BESSI
This is the Bergen Snow Simulator (BESSI), a mass and energy balance model. The firn model uses a mass based grid designed for long-timescale modelling in particular of the Greenland ice sheet (Born et al., 2019, Zolles and Born 2021). The code is under development to also be useful for modeling of smaller ice caps and glaciers. This repository contains the files necessary to run BESSI V.2. 

BESSI consists of:
* run_* files: Compilers
* src/bessi_defs.F90: Customization to domain, settings and toggles not set in run_bessi
* src/Wrapper.F90: Wrapper
* src/bessi_* files: Reading climate, writing, terminal output
* src/physics/*: Contains all physics


The version added here contains an additional subroutine that calculates the albedo and the net solar heat flux with different chosen albedo parametrizations. The energy flux calculation now calculates the turbulent latent heat flux, based on the residual method. The vapor flux has significant impact on the SMB on the local scale, for PD conditions.
    

There are three different switches for specific subroutines concerning the energy balance calculation. The turbulent latent heat flux can be switched on and off, there are 4 different albedo parametrizations to choose from. Incoming longwave radiation can either be calculated from the atmospheric temperature or direct meteorological input can be taken. Based on previous results we recommend including the turbulent latent heat flux, using albedo parametrizations 4 or 5 and taking the incoming long-wave from climate data if it is available. 

Parameters for albedo, turbulent exchange coefficients etc.  have been optimized relative to GRACE and 10m firn core temperature data.The published version now contains an additional switch for long-wave radiation, either taking it from the climate model input or using a parametrization 

Let us know if you plan to use BESSI, we would love to hear about it! (andreas.born@uib.no or rebekka.froystad@uib.no)  
For technical difficulties, contact rebekka.froystad@uib.no or open an issue. 

# RUNNING BESSI #
Currently, BESSI is customized to run for the Greenland ice sheet on a 10 km resolution.

1. Compile: ./run_bessi.sh
2. Then execute: ./bin/bessi.x

## Customizing to your data and grid ##
In bessi_run.sh file:
* Change file paths in bessi_run.sh file that are defined as
	* BESSI_OUTPUT_DIR: output directory
	* BESSI_INPUT_DOMAIN: elevation used in domain (to use lapse rate correctly)
	* BESSI_INPUT_CLIMATE_DIR: directory with climate files (file names defined later)
	* BESSI_INPUT_CLIMATE_ELEVATION: path to netcdf file with elevation data used by climate model (variable name defined later)
* Change what years you want to run over (these should be contained in the climate files, explained in the following)

In src/bessi_defs.F90, make sure these are correct for your data
* Info on grid: resolution and grid size (dx, dy, L, W, nx, ny) around lines 255-285 in code
* Grid variable names
	* bedrock: netcdf_input_bedrock_variable (ca line 335)
	* elevation: netcdf_input_icesurf_variable (ca line 340)
* Climate variable names, starting with "netcdf_input_eraiterim_"
	* temperature: "temp_variable"
	* precipitation: "precip_variable"
	* short wave radiation: "swradboa_variable"
	* dew point: "dewpT_variable"
	* wind variable: "wind_variable" not in use yet, keep as empty string
	* long wave radiation: "lwrd_variable"
	* reference elevation: "netcdf_input_eraiterim_initial_climate_elevation_variable"



## Code quirks ##

### Input units ###
BESSI requires input with daily temporal resolution
* Precipitation: mm/day (negative precip not accepted)
* Temperature: C
* Short wave radiation: W/m2
* Long wave radiation: W/m2 or J/day, check "lwrd_unit" in src/bessi_defs.F90
* Humidity: 2m dew point, C


### Annual/daily output ###
BESSI is writes annual outputs as default in the parallellized version of the code ("make bessi"). 
Daily and monthly outputs are also available for the non-parallellized version ("make bessi-single").
Type of temporal output and frequency can be specified in src/bessi_defs.F90 (annual_data, monthly_data, daily_data). A "focus" period were all the data is written can be chosen additionally. 

Daily and monthly output data is written in an unconventional way for netcdf files. This is due to how BESSI is structured. Dimensions are (y,x,time) instead of (time,y,x). This means data looks all jumbled up in ncview unless "axes" are specified and can still be read by e.g. python. It is written this way due to computational efficiency and how BESSI is set up.



## Publications using BESSI ##

Born, A., Imhof, M. A., and Stocker, T. F.: An efficient surface energy–mass balance model for snow and ice, The Cryosphere, 13, 1529–1546, https://doi.org/10.5194/tc-13-1529-2019, 2019. 

Fettweis, X. et al.: GrSMBMIP: intercomparison of the modelled 1980–2012 surface mass balance over the Greenland Ice Sheet, The Cryosphere, 14, 3935–3958, https://doi.org/10.5194/tc-14-3935-2020, 2020. 

Zolles, T. and Born, A.: Sensitivity of the Greenland surface mass and energy balance to uncertainties in key model parameters, The Cryosphere, 15, 2917–2938, https://doi.org/10.5194/tc-15-2917-2021, 2021. 

Holube, K. M., Zolles, T., and Born, A.: Sources of uncertainty in Greenland surface mass balance in the 21st century, The Cryosphere, 16, 315–331, https://doi.org/10.5194/tc-16-315-2022, 2022.


